#ifndef ___main_hpp___
#define ___main_hpp___

#include <trng/yarn3.hpp>			 /* Support for TRNG's yarn random number generator */
#include <vector>
#include <array>


void sampleprior(double * par_ptr,  trng::yarn3 &gen);

void importData(Array2D<double>& output, std::vector<int> &validInd, char* csvName);

void save_fwrite(const char *file, Array4D *par_chain,Array3D *energy_chain,Array2D<int> *step_accept,Array2D<int> *swap_accept,Array2D<double> *ptr_beta, int lastswap, double* autocorr);

// void energy(double &energy, double *par, Array2D<double> &data, std::vector<int>& validInd, Array2D<double> &data2, std::vector<int>& validInd2);
void energy(double &energy, double *par, const std::array<   Array2D<double>, ___nExperiments___> data, const std::array<  std::vector<int>, ___nExperiments___> validInd);

void g_PDF(std::vector<double> &F, double a, trng::yarn3 &gen);

void sample_ensemble(std::vector<int> &F, trng::yarn3 &gen);

void shuffle(int * shuffle_ptr, int len, trng::yarn3 &gen);
double calc_rate(int ID, int * ptr );
void adapt_beta( double* swaprate, double* beta_curr);
void adapt_step(double * steprate, double* step_curr);

static const double betaarray[100] = {25.2741, 7., 4.47502, 3.5236, 3.0232,
                      2.71225, 2.49879, 2.34226, 2.22198, 2.12628,
                      2.04807, 1.98276, 1.92728, 1.87946, 1.83774,
                      1.80096, 1.76826, 1.73895, 1.7125, 1.68849,
                      1.66657, 1.64647, 1.62795, 1.61083, 1.59494,
                      1.58014, 1.56632, 1.55338, 1.54123, 1.5298,
                      1.51901, 1.50881, 1.49916, 1.49, 1.4813,
                      1.47302, 1.46512, 1.45759, 1.45039, 1.4435,
                      1.4369, 1.43056, 1.42448, 1.41864, 1.41302,
                      1.40761, 1.40239, 1.39736, 1.3925, 1.38781,
                      1.38327, 1.37888, 1.37463, 1.37051, 1.36652,
                      1.36265, 1.35889, 1.35524, 1.3517, 1.34825,
                      1.3449, 1.34164, 1.33847, 1.33538, 1.33236,
                      1.32943, 1.32656, 1.32377, 1.32104, 1.31838,
                      1.31578, 1.31325, 1.31076, 1.30834, 1.30596,
                      1.30364, 1.30137, 1.29915, 1.29697, 1.29484,
                      1.29275, 1.29071, 1.2887, 1.28673, 1.2848,
                      1.28291, 1.28106, 1.27923, 1.27745, 1.27569,
                      1.27397, 1.27227, 1.27061, 1.26898, 1.26737,
                      1.26579, 1.26424, 1.26271, 1.26121,
                      1.25973};

#endif