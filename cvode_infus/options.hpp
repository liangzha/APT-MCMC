#ifndef ___OPTIONS___ // include header guard
#define ___OPTIONS___

/*=================================================================================================*
 *======================================OPTIONS BEGIN HERE=========================================*
 *=================================================================================================*/
#define ___MaxThreads___ 							8	// Maximum number of threads to open
#define ___nChains___								4	// Number of temperature chains
#define ___nSteps___								25	// Total number of steps to attempt before a swap attempt
#define ___nWalkers___								100	// Total number of steps to attempt before a swap attempt
#define ___nSwaps___								100	// Total number of swaps between chains to attempt
#define ___nPars___									2	// Total number of parameters

/* Status interval Options */
#define ___dispInterval___ 							100	// Display status updates after n swaps have been attempted
#define ___saveInterval___							1000	// Save partially completed results in case of crashes
#define ___statusBar___ 							1	// Turns on or off the dynamic status bar between disp Intervals


/* General Adaption Options */
#define ___adapt_last___							0	// Last swap event that adaption of relstep or beta can occur
#define ___min_adaption_factor___					8.0e-01	// Lowest multiplier to modify relstep or beta
#define ___max_adaption_factor___					1.2e+00	// Max multiplier to modify relstep or beta

/* Adapt Beta Options */
#define ___optimal_swap_accept___					2.4e-01 	// Set optimum swap acceptance % that program should aim for
#define ___adapt_beta_interval___					1000	// How often to change temperatures among chains
#define ___adapt_beta_rate___						4.0e-02	// Exponential to change beta

/* Adapt Step Size Options */	
#define ___optimal_step_accept___					3.0e-01	// Set optimum step acceptance % that program should aim for
#define ___adapt_step_interval___					1000	// How often to change step size among chains
#define ___adapt_step_size___						1.5e-01	// Exponential to change step size
#define ___gzStep___                				3.0e+00	// Starting Step size

/* Initialization Options */
#define ___max_init_steps___						10		// Max number of parameters to test before running algorithm

/* Energy Options */
#define ___big_energy___							1.0e+29	// Penalty energy

/* Data Options */
#define ___nExperiments___							2	// Number of exeriments/data files
#define ___ExperimentFiles___						{"aptdata_Experiment1.csv","aptdata_Experiment2.csv"}	// Array of filenames
#define ___log_data___								false	// true to tell the code to take the ln of your data. false if it is already in ln space
#define ___log_min___								1.0e-14	// Define a minimum value to use if log encounters a 0 

/* Save Options */
#define ___matFilename___							"simpleKinetics"

/* cvODE Settings */
#define ___CVODE_NEQ___   							2	// number of equations 
#define ___CVODE_RTOL___  							RCONST(1e-08) 	// scalar relative tolerance           
#define ___CVODE_MAX_NUM_STEPS___					1000	// Set max number of steps for solver to take before reaching next time

/* Optional:  instead of providing cvode_atol, you can provide a vector of absolute tolerance levels, one for each ODE/state (CVODE_NEQ total states) that you have specified
example: 3 states:
#define ___CVODE_VECTOR_ATOL___ 		{1.0e-8, 1.0e-14, 1.0e-6}  
 */
#define ___CVODE_ATOL___ 							RCONST(0.0001)	// Absolute tolerance for cvode, constant value for each ODE you have
// #define ___CVODE_DispWarnings___        /* UnComment this line if you want to see CVODE step size warnings! */

/* Some useful constants, if you need it */
#define ___pi___	3.14159265358979323846264338327950288419716939937510

/*=================================================================================================*
 *======================================OPTIONS END HERE===========================================*
 *=================================================================================================*/
#endif
