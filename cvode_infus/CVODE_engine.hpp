#ifndef ___CVODE_engineHPP___
#define ___CVODE_engineHPP___ 



/* Vector and matrix accessor macros: Ith, IJth */

/* These macros are defined in order to write code which exactly matches
   the mathematical problem description given above.

   Ith(v,i) references the ith component of the vector v, where i is in
   the range [1..NEQ] and NEQ is defined below. The Ith macro is defined
   using the N_VIth macro in nvector.h. N_VIth numbers the components of
   a vector starting from 0.

   IJth(A,i,j) references the (i,j)th element of the dense matrix A, where
   i and j are in the range [1..NEQ]. The IJth macro is defined using the
   DENSE_ELEM macro in dense.h. DENSE_ELEM numbers rows and columns of a
   dense matrix starting from 0. */

#define Ith(v,i)    NV_Ith_S(v,i-1)       /* Ith numbers components 1..NEQ */
//#define IJth(A,i,j) DENSE_ELEM(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */


#include <sundials/sundials_types.h> /*Support for realtypes */


/* Private functions to output results */

static void PrintOutput(realtype t, realtype y1, realtype y2, realtype y3);

/* Private function to print final statistics */

static void PrintFinalStats(void *cvode_mem);

/* Private function to check function return values */

static int check_flag(void *flagvalue, char *funcname, int opt);

/* Public function to invoke CVODE_Engine for integration of CVODE_equations */
int integrate(void *par, Array2D<realtype> *model, const std::vector<double> &Y0, const std::vector<double> &tsim, Array2D<realtype> infus_ramp, Array2D<realtype> infus_zoh, Array2D<realtype> infus_bol);

void myErrFcn( int error_code, const char *module, const char *function, char *msg, void *eh_data);

#endif