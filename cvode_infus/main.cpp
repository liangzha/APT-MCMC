#include "options.hpp"				 /* Options for MCMC */
#include "classes.hpp"				 /* Support for custom Array classes */
#include <sundials/sundials_types.h> /* Definition of type realtype */
#include "main.hpp"					 /* Support for functions used throughout this code */
#include <omp.h>					 /* Support for parallelization */
#include <iostream>					 /* Support for printing stuff */
#include <stdio.h>					 /* Support for printing stuff */
#include <math.h>					 /* Support for Math */
#include <trng/uniform_dist.hpp> 	 /* Support for uniform distribution for use with generating z */
#include <trng/uniform01_dist.hpp>   /* Support for uniform distribution (0 to 1) for probability acceptances */
#include "CVODE_engine.hpp"			/* Definitions for the CVODE integrator */
#include <cmath>					/* Include math libraries */
#include <nvector/nvector_serial.h>	/* Support for CVODE vectors, types, functions, macros */

#include <fstream>

#define Ca		 NV_Ith_S(y,0)
#define Ca_dot	 NV_Ith_S(ydot,0)
#define Cb		 NV_Ith_S(y,1)
#define Cb_dot	 NV_Ith_S(ydot,1)

//Define parameters
#define k1 params[0]
#define k2 params[1]
#define infus u[1] // start at 1 because u[0] is a time
// #define moomoo u[2]
/* ========================================
 * =END DEFINE PARAMETERS &  STATE MACROS==
 * ========================================
 */

// Compute function rhs(t,y), a dy/dt slope function that is invoked by CVODE
int rhs(realtype t, N_Vector y, N_Vector ydot, realtype* params, realtype* u)
{
	/* ==============================================
	 * =============DEFINE ODES HERE=================
	 * ==============================================
	 * Based on your macro definitions earlier, you can use them to "pretty" up your ODEs
	 */

	 Ca = Ca * (Ca > 0);
	 Cb = Cb * (Cb > 0);
	//ODE for state _Constants_
	// printf("Params = [%2.1f, %2.1f]\n",k1,k2);
	// printf("u = [%2.1f]\n",infus);

	//ODE for state Ca
	// Ca_dot =  infus;
	 Ca_dot = - k1 * Ca + infus;

	//ODE for state Cb
	Cb_dot = k1*Ca-k2*Cb;
	// printf("Cadot = %f, Cbdot = %f\n",Ca_dot, Cb_dot);
	/* ==============================================
	 * ==========END ODE DEFINITIONS HERE============
	 * ==============================================
	 */

	return(0);
}
int rhs2(realtype t, N_Vector y, N_Vector ydot, void* userdata)
{
	/* ==============================================
	 * =============DEFINE ODES HERE=================
	 * ==============================================
	 * Based on your macro definitions earlier, you can use them to "pretty" up your ODEs
	 */
	realtype* params = (realtype*) userdata;
	//ODE for state _Constants_
	// printf("Params = [%2.1f, %2.1f]\n",k1,k2);
	// printf("u = [%2.1f]\n",params[2]);

	//ODE for state Ca
	Ca_dot = 5 - k1 * Ca;

	//ODE for state Cb
	Cb_dot = k1*Ca-k2*Cb + params[2]/4;
	printf("Cadot = %f, Cbdot = %f\n",Ca_dot, Cb_dot);
	/* ==============================================
	 * ==========END ODE DEFINITIONS HERE============
	 * ==============================================
	 */

	return(0);
}

// void moofoo(void* hi[])
// {
// 	printf("hi[0] = {%i, %i, %i}\n", ((int*) hi[0])[0],((int*) hi[0])[1],((int*) hi[0])[2]);
// 	printf("hi[1] = {%i, %i, %i}\n", ((int*) hi[1])[0],((int*) hi[1])[1],((int*) hi[1])[2]);
// }

// void moofoo2(void* hi)
// {
// 	// how to convert void* into void**????
// 	int* momo = *((int**) hi);
// 	printf("{%i, %i, %i}\n", momo[0],momo[1],momo[2]);

// 	double* momo1 = *((double**) hi+1);
// 	printf("{%f, %f, %f}\n", momo1[0],momo1[1],momo1[2]);
	
// 	double* momo2 =  (double*)   *((void**) hi+1) ;
// 	printf("{%f, %f, %f}\n", momo1[0],momo1[1],momo1[2]);
	
// }


int main() 
{

	/*=================================================================
	 *======================Initialization=============================
	 *=================================================================*/
    // double runtime = omp_get_wtime(); 					/* Timing the run */
	
	double par[] = {0.7, 0.3};
	// double par[] = {0.23, 0.46, 0.0};
	
	const std::vector<double>  Y0   = { 3.0,2.000000 };
	// const std::vector<double>  tsim = { 0,2,4,6,8,12,24 };
	const std::vector<double>  tsim = { 0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4.0,4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,5.0,5.1,5.2,5.3,5.4,5.5,5.6,5.7,5.8,5.9,6.0,6.1,6.2,6.3,6.4,6.5,6.6,6.7,6.8,6.9,7.0,7.1,7.2,7.3,7.4,7.5,7.6,7.7,7.8,7.9,8.0,8.1,8.2,8.3,8.4,8.5,8.6,8.7,8.8,8.9,9.0,9.1,9.2,9.3,9.4,9.5,9.6,9.7,9.8,9.9,10.0,10.1,10.2,10.3,10.4,10.5,10.6,10.7,10.8,10.9,11.0,11.1,11.2,11.3,11.4,11.5,11.6,11.7,11.8,11.9,12.0,12.1,12.2,12.3,12.4,12.5,12.6,12.7,12.8,12.9,13.0,13.1,13.2,13.3,13.4,13.5,13.6,13.7,13.8,13.9,14.0,14.1,14.2,14.3,14.4,14.5,14.6,14.7,14.8,14.9,15.0,15.1,15.2,15.3,15.4,15.5,15.6,15.7,15.8,15.9,16.0,16.1,16.2,16.3,16.4,16.5,16.6,16.7,16.8,16.9,17.0,17.1,17.2,17.3,17.4,17.5,17.6,17.7,17.8,17.9,18.0,18.1,18.2,18.3,18.4,18.5,18.6,18.7,18.8,18.9,19.0,19.1,19.2,19.3,19.4,19.5,19.6,19.7,19.8,19.9,20.0,20.1,20.2,20.3,20.4,20.5,20.6,20.7,20.8,20.9,21.0,21.1,21.2,21.3,21.4,21.5,21.6,21.7,21.8,21.9,22.0,22.1,22.2,22.3,22.4,22.5,22.6,22.7,22.8,22.9,23.0,23.1,23.2,23.3,23.4,23.5,23.6,23.7,23.8,23.9,24  };

	Array2D<realtype> model(tsim.size(), Y0.size());
	
	Array2D<realtype> infus_zoh(3,2); // zero by default
	infus_zoh(1,0) = 6;
	infus_zoh(1,1) = -40;
	infus_zoh(2,0) = 24;

	// Array2D<realtype> infus_zoh(2,2); // zero by default
	// infus_zoh(0,0) = 0;
	// infus_zoh(0,1) = 0;
	// infus_zoh(1,0) = 24;
	// infus_zoh(1,1) = 0;

	Array2D<realtype> infus_ramp(3,2); // zero by default
	infus_ramp(1,0) = 6;
	infus_ramp(1,1) = 40;
	infus_ramp(2,0) = 24;
	infus_ramp(2,1) = 40;

	// Array2D<realtype> infus_ramp(2,2); // zero by default
	// infus_ramp(1,0) = 24;

	Array2D<realtype> infus_bol(1,3);
	infus_bol(0,0) = 14;
	infus_bol(0,1) = 10;

	// printf("Main: Width of infuse zoh  is %i \n",infus_zoh.GetWd());
	printf("====zoh====\n");
	for (int i = 0; i < infus_zoh.GetHt(); ++i)
	{
		for (int j = 0;  j < infus_zoh.GetWd(); ++j)
			printf("%2.1f  ",infus_zoh(i,j));
		printf("\n");
	}

	printf("====ramp====\n");
	for (int i = 0; i < infus_ramp.GetHt(); ++i)
	{
		for (int j = 0;  j < infus_ramp.GetWd(); ++j)
			printf("%2.1f  ",infus_ramp(i,j));
		printf("\n");
	}

	printf("===bol===\n");
	for (int i = 0; i < infus_bol.GetHt(); ++i)
	{
		for (int j = 0; j < infus_bol.GetWd(); ++j)
			printf("%2.1f  ", infus_bol(i,j));
		printf("\n");
	}

	// printf("===========\n Col extractor: \n");

	// realtype moo[infus_zoh.GetHt()]; 
	// infus_zoh.extract_col(moo,0);
	// for (int i =0; i < 4; ++i)
	// 	printf("%f\n",moo[i]);


	int errflag = integrate(par, &model, Y0, tsim, infus_ramp, infus_zoh, infus_bol);
	printf("finished integrating\n");
	if (errflag)	 //some cvode integration failure encountered
	{
		printf("Something fucked up\n");
		return -1;
	}
	else
	{
		// /* Calculate the ln likelihood */
		// for (auto e: validInd[1])      // For loop over valid indicies only
		// {
		// 	printf('%f\n',e);
		// }
		printf("I can integrate successfully!!!\n");
	}

	// printf("====results====\nt\tCa\t\tCb\n");
	// for (int i = 0; i < model.GetHt(); ++i)
	// {
	// 	printf("%2.1f\t",tsim[i]);
	// 	for (int j = 0;  j < model.GetWd(); ++j)
	// 		printf("%2.3e \t",model(i,j));
	// 	printf("\n");
	// }

	std::ofstream myfile;
	myfile.open("results.csv");
	myfile << "t,Ca,Cb\n";
	for (int i = 0; i < model.GetHt(); ++i)
	{
		myfile << tsim[i];
		for (int j = 0;  j < model.GetWd(); ++j)
			myfile << "," << model(i,j);
		myfile << "\n";
	}
	myfile.close();
	// printf("Finished. I took %f s.\n", (omp_get_wtime()-runtime));
	return 0;
}
