"""
Generate an APT-MCMC simulation to fit the VDW reaction

Assume Isothermal CSTR, where only A flows in:
* CaI = 10 mol/L
* F/V = 4/7 min^-1

A --kAB--> B --kBC--> C
2A --kAD--> D

Initial Conditions:
Cao = CaI
Everything else is 0

Fit kAB, kBC, kAD

The data was generated using:
datat = [0:0.5:10]
kAB, kBC, kAD = [5/6, 5/3, 1/6]
With noise of  N(0, 0.1)

"""

import sys
sys.path.append("../") 
import aptmodel as apt

import importlib
importlib.reload(apt)


# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# Create parameters
model.add_par('mumax',0,20)     # define a parameter, kAB, whose searchspace lies between (0,5)
model.add_par('Ks',0,20) 	  # define a parameter, kBC, whose searchspace lies between (0,10)
model.add_par('Ysc',0,20)     # define a parameter, kAD, whose searchspace lies between (0,10)
model.add_par('kd',0,20)     # define a parameter, kAD, whose searchspace lies between (0,10)
model.add_par('m',0,20)     # define a parameter, kAD, whose searchspace lies between (0,10)
model.add_par('Ypc',0,20)     # define a parameter, kAD, whose searchspace lies between (0,10)
#model.add_par('Cp_s',0,200)     # define a parameter, kAD, whose searchspace lies between (0,10)
model.showpar()		# See the parameters and verify them before moving on 

# Add states
# 	This also sets up a dictionary of ODEs for you to define later on
model.add_states('Cc','Cs','Cp')	 # 'Cc', 'Cd', 'Ce'.... etc
model.showstates()	# See the states and verify them before moving on

# Define ODEs as a string.
# The contents within the string must be C++, but is pretty generic.
# Remember to use pow instead of ** or ^
# Supported custom functions:
# 	hill(a,b) = a/(b + a)
# 	hilln(a,n,b) = a^n/(b^n + a^n)
# 	heavy(z) = 1/10^(-10*z)

# Our system is described by
# dCa/dt = 5 - k1 * Ca
# dCb/dt = k1*Ca - k2*Cb
model.ODE['Constants'].add_helper('Cp_s = 93')
# model.ODE['Constants'].add_helper('Ysc = 12.5')
# model.ODE['Constants'].add_helper('Ypc = 5.6')
model.ODE['Constants'].add_helper('rg = mumax * pow(1-Cp/Cp_s,0.52) * Cc * Cs/(Ks+Cs)')
model.ODE['Cc'].set('rg - kd * Cc') 
model.ODE['Cs'].set('-Ysc * rg - m * Cs')
model.ODE['Cp'].set('Ypc * rg')

# Load data from a matlab file
import numpy as np
loaded = np.load('bioRXN_data3.npz')
dataT = loaded['arr_0']
Cc_data, Cs_data, Cp_data = (loaded['arr_1']).T


# Put this object into the APTexperiment object
experiment1 = apt.experiment('Experiment1')

# Create a APTdata object to put data into this Experiment
aptdata = apt.data(Cc_data,states=['Cc'], time=dataT, isLog=False)	# make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)
# we can also add more data into this experiment
aptdata = apt.data(Cs_data,states=['Cs'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)

aptdata = apt.data(Cp_data,states=['Cp'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)


experiment1.show(showall=True)			# Check if information was added correctly

model.add_experiment(experiment1)


# loaded = np.load('bioRXN_data2.npz')
# dataT = loaded['arr_0']
# Cc_data, Cs_data, Cp_data = (loaded['arr_1']).T


# # Put this object into the APTexperiment object
# experiment2 = apt.experiment('Experiment2')

# # Create a APTdata object to put data into this Experiment
# aptdata = apt.data(Cc_data,states=['Cc'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
# experiment2.add_data(aptdata)
# # we can also add more data into this experiment
# aptdata = apt.data(Cs_data,states=['Cs'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
# experiment2.add_data(aptdata)

# aptdata = apt.data(Cp_data,states=['Cp'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
# experiment2.add_data(aptdata)
# model.add_experiment(experiment2)

# Create an options variable for model
options = apt.aptoptions(maxThr=8)
options.nSwap= 1000
options.filename = "bioRXN"
options.show()	# Should run this and double check things are okay before generating

model.output('bioRXN')
model.generate(options)
