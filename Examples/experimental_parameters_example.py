import sys
sys.path.append("../") 

import aptmodel as apt

import importlib
importlib.reload(apt)


# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# Create parameters
model.add_par('k1',0,5)  # define a parameter, k1, whose searchspace lies between (0,5)
model.add_par('k2_exp1',0,3)	 # define a parameter, k2, whose searchspace lies between (0,7)
model.add_par('k2_exp2',0,3)	 # define a parameter, k2, whose searchspace lies between (0,7)
model.showpar()		# See the parameters and verify them before moving on 

# Add states
# 	This also sets up a dictionary of ODEs for you to define later on
model.add_states('Ca')	 # 'Cc', 'Cd', 'Ce'.... etc
model.showstates()	# See the states and verify them before moving on

# Define ODEs as a string.
'''
Our system is described by
dCa/dt = k1 - k2 * Ca
'''
model.ODE['Ca'].set('k1  - k2* Ca') 

from scipy.io import loadmat
import numpy as np

importeddata = loadmat('experimentally_dependent_infusions.mat')['data']

# Imported data is:
#  t   Ca_exp1   Ca_exp2
#  0     ..        ..  
#  ..    ..        ..

T = importeddata[:,0]
Ca_exp1 = importeddata[:,1]
Ca_exp2 = importeddata[:,2]

# Create a APTdata object to store dataA in
aptdata = apt.data(Ca_exp1.T,states=['Ca'], time=T, isLog=False)	# make sure that dataA is nStates x nTime before passing it in!

# Generate an APT.experiment object to store all this information
experiment1 = apt.experiment('Experiment1')
experiment1.add_data(aptdata)

# Attach this experiment into the APT.model
model.add_experiment(experiment1)

aptdata = apt.data(Ca_exp2.T, states=['Ca'], time=T, isLog=False)
experiment2 = apt.experiment('Experiment2')
experiment2.add_data(aptdata)
model.add_experiment(experiment2)


model.add_per_experiment_par('k2', {'Experiment1':'k2_exp1', 'Experiment2':'k2_exp2'})

# Create an options variable for model
options = apt.aptoptions(maxThr=4)
options.nSwap= 1e3
options.filename = "experimental_parameters"
options.show()	# Should run this and double check things are okay before generating

model.output('experimental_parameters_example')
model.generate(options)

"""
Now go to the output directory, which is by default ./Output, but we set
it as ./experimental_parameters here.
In a terminal, run
$ make
$ ./experimental_parameters
And you should see APT-MCMC running!

Ex:
	output = apt.openresults('experimental_parameters.mcmc')

Once it is done, it will save a .mcmc file, which can be loaded into your
Python workspace via apt.openresults(filename) OR converted to a .mat file
using MCMC_convert.m

The parameters used to generate this example were:
k1 = .3
k2 = .4
k3 = .7
"""

