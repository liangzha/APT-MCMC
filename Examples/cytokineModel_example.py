import aptmodel as apt

# Initialize model
model = apt.aptmodel()

model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

model.add_states('TNF', 'N', 'IL6', 'M', 'P', 'IL10')
# model.showStates()

model.ODE['TNF'].add_helper('Vapp_tnf = Vmt/(1.0 + IL6/K6t)')
model.ODE['TNF'].set('Vapp_tnf * hill(M, Kmt*(1+IL10/K10t) ) - mut * TNF')

model.ODE['N'].add_helper('Kapp_N = Kn * (1 + M/Kmn + IL6/K6n + TNF/Ktn2)')
model.ODE['N'].set('Vtn * hill(TNF, Ktn1) - Vn * hill(N,Kapp_N)')

model.ODE['IL6'].add_helper('Kapp_IL6 = Km6 * (1+ IL10/K106)')
model.ODE['IL6'].set('Vm6 * hill(M, Kapp_IL6) - mu6 * IL6')

model.ODE[
    'M'].set('Kpm * P/(1+ exp(-(t-8))) + V6m * hilln(IL6,3, K6m) - mum * M')

model.ODE[
    'P'].set('infus*(t>=0)*(t<1)+Kp1*P*(1-P/Pinf)* (P-Peps)/(P+Peps) - Knp * N * hill(P,Kp2) * (1+  Vni*hill(IL6, Kni) )')

model.ODE['IL10'].set('Km10*M - mu10*IL10')

model.ODE['Constants'].add_helper('Pinf = 10000')
model.ODE['Constants'].add_helper('Peps = 50')


model.load_params_from_csv('test.csv',verbose=False)


# Load data from a matlab file
from scipy.io import loadmat
import numpy as np

# sod_times = [-3,-2,-1,2,4,6,8,10,12]
sod_times = [-2,2,4,6,8,10,12]
sod_states = ['TNF','IL6','IL10','N','M']

# pig 1
piggy = loadmat('SOD_1')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod1 = apt.aptdata('sod1',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod1.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod1)

# pig 2
piggy = loadmat('SOD_2')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod2 = apt.aptdata('sod2',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod2.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod2)

# pig 4
piggy = loadmat('SOD_4')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod4 = apt.aptdata('sod4',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod4.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod4)

# pig 7
piggy = loadmat('SOD_7')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod7 = apt.aptdata('sod7',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod7.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod7)

# pig 9
piggy = loadmat('SOD_9')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod9 = apt.aptdata('sod9',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod9.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod9)

# pig 10
piggy = loadmat('SOD_10')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod10 = apt.aptdata('sod10',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod10.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod10)

# pig 12
piggy = loadmat('SOD_12')['pig']
piggy = np.delete(piggy,2,axis=1) # remove IL8 from data
piggy = np.delete(piggy,[0,2],axis=0)
apt_sod12 = apt.aptdata('sod12',piggy.T,states=sod_states,lnFlag=False,time=sod_times)
apt_sod12.missingInitialCondition([0,0,0,0,0,0],{})
model.add_data(apt_sod12)

options = apt.aptoptions(maxThr=20)
options.nSwap= 1e6
options.saveInterval = 1e10
options.filename = "pig_sepsis"
options.saveFile = "pig_sepsis"
options.show()	# Should run this and double check things are okay before generating

model.generate(options)

