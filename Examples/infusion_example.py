import sys
sys.path.append("../") 
import aptmodel as apt

import importlib
importlib.reload(apt)


# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# Create parameters
model.add_par('k1',0,5)  # define a parameter, k1, whose searchspace lies between (0,5)
model.add_par('k2',0,3)	 # define a parameter, k2, whose searchspace lies between (0,7)
model.showpar()		# See the parameters and verify them before moving on 

# Add states
# 	This also sets up a dictionary of ODEs for you to define later on
model.add_states('Ca','Cb')	 # 'Cc', 'Cd', 'Ce'.... etc
model.showstates()	# See the states and verify them before moving on

# Define ODEs as a string.
'''
Our system is described by
dCa/dt = 5 - k1 * Ca
dCb/dt = k1*Ca - k2*Cb
'''
model.ODE['Ca'].set(' - k1 * Ca + infus1') 
model.ODE['Cb'].set('k1*Ca-k2*Cb')

model.enable_infusions({0:'infus1'})


# Load data from a matlab file
'''
infusion_sample_data:  
t_data = np.arange(0,24.1,.1)
col1: Ca
col2: Cb
Generated with aforementioned ODEs at infusions:
====zoh====
 t    infus1
0.0   0.0
6.0  -40.0
24.0  0.0
====ramp====
 t    infus1
0.0   0.0
6.0   40.0
24.0  40.0
===bol===
 t     Ca    Cb
14.0  10.0  0.0
'''

from scipy.io import loadmat
import numpy as np

importeddata = loadmat('infusion_sample_data.mat')['data']

# Create a APTdata object to store dataA in
aptdata = apt.data(importeddata.T,states=['Ca','Cb'], time=np.arange(0,24.1,.1), isLog=False)	# make sure that dataA is nStates x nTime before passing it in!

# Define infusion matrices
infus_zoh = np.zeros([3,2])
infus_zoh[1] = [6, -40]
infus_zoh[2] = [24, 0]

infus_ramp = np.zeros([3,2])
infus_ramp[1] = [6, 40]
infus_ramp[2] = [24, 40]

infus_bol = np.zeros([1,3])
infus_bol[0] = [14, 10, 0]

# Generate an APT.experiment object to store all this information
experiment1 = apt.experiment('Experiment1')
experiment1.add_data(aptdata)

# Add the infusions into the experiment
experiment1.add_infusion(zoh=infus_zoh, ramp=infus_ramp, bolus=infus_bol) 

experiment1.show(showall=True)			# Check if information was added correctly

# Attach this experiment into the APT.model
model.add_experiment(experiment1)

# Create an options variable for model
options = apt.aptoptions(maxThr=4)
options.nSwap= 1e3
options.filename = "infusion_example"
options.show()	# Should run this and double check things are okay before generating

model.output('infusion_example')
model.generate(options)

"""
Now go to the output directory, which is by default ./Output, but we set
it as ./infusion_example here.
In a terminal, run
$ make
$ ./infusion_example
And you should see APT-MCMC running!

Once it is done, it will save a .mcmc file, which can be loaded into your
Python workspace via apt.openresults(filename) OR converted to a .mat file
using MCMC_convert.m

Ex:
   output = apt.openresults('infusion_example.mcmc')

The parameters used to generate this example were:
k1 = 0.7
k2 = 0.3
"""
