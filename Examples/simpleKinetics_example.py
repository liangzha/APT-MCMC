import sys
sys.path.append("../") 
import aptmodel as apt

import importlib
importlib.reload(apt)


# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# Create parameters
model.add_par('k1',0,5)  # define a parameter, k1, whose searchspace lies between (0,5)
model.add_par('k2',0,7)	 # define a parameter, k2, whose searchspace lies between (0,7)
model.showpar()		# See the parameters and verify them before moving on 

# Add states
# 	This also sets up a dictionary of ODEs for you to define later on
model.add_states('Ca','Cb')	 # 'Cc', 'Cd', 'Ce'.... etc
model.showstates()	# See the states and verify them before moving on

# Define ODEs as a string.
# The contents within the string must be C++, but is pretty generic.
# Remember to use pow instead of ** or ^
# Supported custom functions:
# 	hill(a,b) = a/(b + a)
# 	hilln(a,n,b) = a^n/(b^n + a^n)
# 	heavy(z) = 1/10^(-10*z)

# Our system is described by
# dCa/dt = 5 - k1 * Ca
# dCb/dt = k1*Ca - k2*Cb
model.ODE['Ca'].set('5 - k1 * Ca') 
model.ODE['Cb'].set('k1*Ca-k2*Cb')

# Load data from a matlab file
from scipy.io import loadmat
import numpy as np
dataA = loadmat('simpleKinetics_data.mat')['dataA']
dataB = loadmat('simpleKinetics_data.mat')['dataB']

# Create a APTdata object to store dataA in
aptdata = apt.data(dataA,states=['Ca','Cb'], time=[0,2,4,6,8,12,24], isLog=False)	# make sure that dataA is nStates x nTime before passing it in!

# Put this object into the APTexperiment object
experiment1 = apt.experiment('Experiment1')
experiment1.add_data(aptdata)
# we can also add more data into this experiment if we wanted to


# Create another APTdata object to put dataB in. 
aptdata = apt.data(dataB,states=['Ca','Cb'],time=[0, 2, 4, 6, 8, 12, 24],isLog=False)

experiment2 = apt.experiment('Experiment2')
experiment2.add_data(aptdata) 			
experiment2.show(showall=True)			# Check if information was added correctly

model.add_experiment(experiment1)
model.add_experiment(experiment2)

# Create an options variable for model
options = apt.aptoptions(maxThr=8)
options.nSwap= 1e2
options.filename = "simpleKinetics"
options.show()	# Should run this and double check things are okay before generating

model.output('simpleKinetics_example')
model.generate(options)

"""
Now go to the output directory, which is by default ./Output, but we set
it as ./simpleKinetics_example here.
In a terminal, run
$ make
$ ./simpleKinetics
And you should see APT-MCMC running!

Once it is done, it will save a .mcmc file, which can be loaded into your
Python workspace via apt.openresults(filename) OR converted to a .mat file
using MCMC_convert.m

The parameters used to generate this example were:
k1 = .1789
k2 =  .91
"""
# output = apt.openresults('simpleKinetics.mcmc')
