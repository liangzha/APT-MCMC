# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#from assimulo.solvers import CVode
#from assimulo.problem import Explicit_Problem
import parmodel
import matplotlib.pyplot as plt
import numpy as np

def rhs(t,y,p):
    k1, k2= p
    
    ydot = k1 - k2 * y
    
    return np.array(ydot)
    
model = parmodel.parmodel()
t,y1 = model.sim(rhs, [0, 20], 3, [.3, .4])
plt.figure()
plt.plot(t,y1, label = 'k2=.4')


t2,y2 = model.sim(rhs, t, 3, [.3, .7])
plt.plot(t2,y2,'r',label = 'k2=.7')

plt.legend()

data = np.zeros([len(t),5])
data[:,0] = np.array(t)
data[:,1] = np.array(y1).T
data[:,2] = np.array(y2).T




def rhs(t,y,p):
    k1, k2, const = p
    
    ydot = const + k1 - k2 * y
    
    return np.array(ydot)
    
_,y1 = model.sim(rhs, t, 3, [.3, .4, 4])

plt.figure()
plt.plot(t,y1, label = 'k2=.4, const=4')

_,y2 = model.sim(rhs, t, 3, [.3, .7, 5])
plt.plot(t2,y2,'r',label = 'k2=.7, const = 5')

plt.legend()
#
#data = np.zeros([len(t),3])
#data[:,0] = np.array(t)
data[:,3] = np.array(y1).T
data[:,4] = np.array(y2).T



from scipy.io import savemat
savemat('experimentally_dependent_infusions.mat',{'data':data})