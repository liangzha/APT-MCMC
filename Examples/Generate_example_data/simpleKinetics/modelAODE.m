function [ dy ] = modelAODE( t, y,par )
%MODELAODE Summary of this function goes here
%   Detailed explanation goes here
k1 = par(1);
k2 = par(2);
dy(1) = 5 - k1 * y(1);
dy(2) = k1*y(1) - k2*y(2);
dy = dy';
end

