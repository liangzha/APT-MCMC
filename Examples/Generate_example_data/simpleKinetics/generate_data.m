tdata = [0, 2, 4, 6, 8, 12, 24];

init = [3, 1];
par= [.1789, .91];
tsmooth = [0 24];
[tsmoothOut,dataA] = ode45(@modelAODE,tsmooth,init,[],par);
figure
plot(tsmoothOut,dataA)
legend('Ca','Cb');

[~,dataA] = ode45(@modelAODE,tdata,init,[],par);
hold on;
plot(tdata,dataA,'o')
hold off;
dataA = dataA';

% save dataA dataA

init2 = [ 0, 5];
[tsmoothOut,dataB] = ode45(@modelAODE,tsmooth,init2,[],par);
figure;
plot(tsmoothOut,dataB)
hold on;
[~,dataB] = ode45(@modelAODE,tdata,init2,[],par);
plot(tdata,dataB,'o');
dataB = dataB';

save simpleKineticsModel_data dataA dataB