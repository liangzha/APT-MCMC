tdata = [0, 1, 2, 3, 4, 6, 10, 24,  30, 36, 40];
tsmooth = linspace(0,40,1000);

init1 = [30, 20, 20];
par = [.8, 50,.56, .17, .38];

[~,traj] = ode15s(@modelB, tsmooth, init1, [], par);
[~,dataA] = ode15s(@modelB, tdata, init1, [], par);
close all;
figure;
for i = 1:3
    subplot(3,1,i);
    plot(tsmooth, traj(:,i)); hold on;
    plot(tdata,dataA(:,i),'o'); hold off;
end


init2 = [100, 0, 0];
[~,traj] = ode15s(@modelB, tsmooth, init2, [], par);
[~,dataB] = ode15s(@modelB, tdata, init2, [], par);
figure;
for i = 1:3
    subplot(3,1,i);
    plot(tsmooth, traj(:,i)); hold on;
    plot(tdata,dataB(:,i),'o'); hold off;
end

dataA = dataA(3:end,:)';
dataB = dataB(5:end,:)';

timeA = tdata(3:end)-tdata(3);
timeB = tdata(5:end)-tdata(5);

save noinitcond_data dataA dataB timeA timeB
