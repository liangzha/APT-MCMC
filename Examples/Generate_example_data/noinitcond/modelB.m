function [ dy ] = modelB( t,y,par )
%MODELB Summary of this function goes here
%   Detailed explanation goes here

y1 = y(1);
y2 = y(2);
y3 = y(3);

Vmax = par(1);
Km = par(2);
mu1 = par(3);
mu2 = par(4);
mu3 = par(5);

dy(1) = Vmax/(Km + y1) * y1 - mu1*y1;
dy(2) = mu1*y1 - mu2*y2;
dy(3) = mu2*y2 - mu3*y3;

dy = dy';

end

