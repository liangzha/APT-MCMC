# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 11:34:54 2016

@author: aznef
"""

from lazytools import *
import matplotlib.pyplot as plt
import seaborn as sns
from assimulo.solvers import CVode
from assimulo.problem import Explicit_Problem
import numpy as np

sns.set(style='whitegrid')


#par = [4.19895e+00 ,6.01994e-01 ]
par = [.7, .3]


def calcramp(x, x1,x2,y1,y2):
    m = (y2-y1)/(x2-x1)
    return y1 + m * (x-x1)

def rhs(t,y):
    k1,k2 = par
    
    Ca, Cb = y
    
    dCa = - k1*Ca + calcramp(t, 0, 6, 0, 40) * (t<=6)
    dCb = k1*Ca - k2*Cb
    
    return np.array([dCa, dCb])

def gen_sim(model):
    sim = CVode(model)
    sim.atol = 0.001
    sim.rtol = 1.0e-8
    sim.maxsteps = 1000
    sim.discr = 'BDF'
    sim.iter='Newton'
    return sim
    
y0 = [3,2]
t0 = 0

model = Explicit_Problem(rhs,y0,t0, name="ExampleInfusion")
sim = gen_sim(model)

_, model_part1 = sim.simulate(14, ncp_list = np.arange(0,14.1,0.1))
model_part1[-1,0] += 10

y0 = model_part1[-1]
t0 = 14
model = Explicit_Problem(rhs,y0,t0, name="ExampleInfusion")
sim = gen_sim(model)
_, model_part2 = sim.simulate(24, ncp_list = np.arange(14, 24, 0.1))
model_part2 = np.delete(model_part2, 0, axis=0)

t = np.arange(0,24.1,0.1)
model = np.concatenate( (model_part1, model_part2) , axis=0)

fig = plt.figure(figsize=(12,10))

plt.subplot(1,2,1)
plt.plot(t, model[:,0],'r')

plt.subplot(1,2,2)
plt.plot(t, model[:,1],'b')