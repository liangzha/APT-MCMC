import sys
sys.path.append("../") 
import aptmodel as apt

# Lines 5 and 6 are optional.
# They are kept for bug testing aptmodel by forcing python to refresh its import of aptmodel.py
import importlib
importlib.reload(apt)


# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# List available benchmarks

options = apt.aptoptions(maxThr=8)

options.saveInterval = 10000000 # very high, turns off saveInterval
options.StatusBar = 0
options.nStep = 1
options.nSwap = 10000

options.filename = "ackley"
model.genbenchmark(1,options,3)

options.filename = "adjiman"
model.genbenchmark(2,options)

options.filename = "alpine"
model.genbenchmark(3,options,5)

options.filename = "bard"
model.genbenchmark(4,options)

options.filename = "beale"
model.genbenchmark(5,options)

options.filename = "bird"
model.genbenchmark(6,options)

options.filename = "bohachevskey"
model.genbenchmark(7,options)

options.filename = "booth"
model.genbenchmark(8,options)

options.filename = "bukin"
model.genbenchmark(9,options)

options.filename = "corana"
model.genbenchmark(10,options)

options.filename = "davamaldi"
model.genbenchmark(11,options)

options.filename = "devilliers"
model.genbenchmark(12,options)

options.filename = "eggholder"
model.genbenchmark(13,options,2)

options.filename = "griewank"
model.genbenchmark(14,options, 2)
