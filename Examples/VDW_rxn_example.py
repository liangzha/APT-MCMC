"""
Generate an APT-MCMC simulation to fit the VDW reaction

Assume Isothermal CSTR, where only A flows in:
* CaI = 10 mol/L
* F/V = 4/7 min^-1

A --kAB--> B --kBC--> C
2A --kAD--> D

Initial Conditions:
Cao = CaI
Everything else is 0

Fit kAB, kBC, kAD

The data was generated using:
datat = [0:0.5:10]
kAB, kBC, kAD = [5/6, 5/3, 1/6]
With noise of  N(0, 0.1)

"""

import sys
sys.path.append("../") 
import aptmodel as apt

import importlib
importlib.reload(apt)


# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# Create parameters
model.add_par('kAB',0,10)     # define a parameter, kAB, whose searchspace lies between (0,5)
model.add_par('kBC',0,10) 	  # define a parameter, kBC, whose searchspace lies between (0,10)
model.add_par('kAD',0,10)     # define a parameter, kAD, whose searchspace lies between (0,10)
model.showpar()		# See the parameters and verify them before moving on 

# Add states
# 	This also sets up a dictionary of ODEs for you to define later on
model.add_states('Ca','Cb','Cc','Cd')	 # 'Cc', 'Cd', 'Ce'.... etc
model.showstates()	# See the states and verify them before moving on

# Define ODEs as a string.
# The contents within the string must be C++, but is pretty generic.
# Remember to use pow instead of ** or ^
# Supported custom functions:
# 	hill(a,b) = a/(b + a)
# 	hilln(a,n,b) = a^n/(b^n + a^n)
# 	heavy(z) = 1/10^(-10*z)

# Our system is described by
# dCa/dt = 5 - k1 * Ca
# dCb/dt = k1*Ca - k2*Cb
model.ODE['Constants'].add_helper('F_over_V = 4.0/7.0')
model.ODE['Constants'].add_helper('CaI = 10.0')
model.ODE['Ca'].set('F_over_V * (CaI - Ca) - kAB * Ca - kAD * Ca * Ca') 
model.ODE['Cb'].set('- F_over_V * Cb - kBC * Cb + kAB * Ca')
model.ODE['Cc'].set('- F_over_V * Cc + kBC * Cb')
model.ODE['Cd'].set('-F_over_V * Cd + 0.5 * kAD * Ca * Ca')

# Load data from a matlab file
import numpy as np
loaded = np.load('VandeVusse_data.npz')
dataT = loaded['arr_0']
Ca_data, Cb_data, Cc_data, Cd_data = (loaded['arr_1']).T


# Put this object into the APTexperiment object
experiment1 = apt.experiment('Experiment1')

# Create a APTdata object to put data into this Experiment
aptdata = apt.data(Ca_data,states=['Ca'], time=dataT, isLog=False)	# make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)
# we can also add more data into this experiment
aptdata = apt.data(Cb_data,states=['Cb'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)

aptdata = apt.data(Cc_data,states=['Cc'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)

aptdata = apt.data(Cd_data,states=['Cd'], time=dataT, isLog=False)  # make sure that dataA is nStates x nTime before passing it in!
experiment1.add_data(aptdata)


experiment1.show(showall=True)			# Check if information was added correctly

model.add_experiment(experiment1)

# Create an options variable for model
options = apt.aptoptions(maxThr=8)
options.nSwap= 1000
options.filename = "Van_de_Vusse"
options.show()	# Should run this and double check things are okay before generating

model.output('Van_de_Vusse')
model.generate(options)
