"""
Note this is outdated!
"""
import sys
sys.path.append("../") 
import aptmodel as apt

# Initialize model
model = apt.aptmodel()

# Set lib,include dir of CVODE and TRNG libraries
model.set_include_path('/usr/local/include')
model.set_lib_path('/usr/local/lib')

# Create parameters
model.add_par('Vmax',0,2)  
model.add_par('Km',0,500)  
model.add_par('mu1',0,2)  
model.add_par('mu2',0,2)  
model.add_par('mu3',0,2)  


# Add states
# 	This also sets up a dictionary of ODEs for you to define later on
model.add_states('y1','y2','y3')

# Define ODEs as a string.
# The contents within the string must be C++, but is pretty generic.
# Remember to use pow instead of ** or ^
# Supported custom functions:
# 	hill(a,b) = a/(b + a)
# 	hilln(a,n,b) = a^n/(b^n + a^n)
# 	heavy(z) = 1/10^(-10*z)

# Our system is described by
# dy1/dt = Vmax*y1/(Km + y1)  mu1*y1
# dy2/dt = mu1*y1 - mu2*y2
# dy3/dt = mu2*y2 - mu3*y3
model.ODE['y1'].set('Vmax*hill(y1,Km) - mu1*y1') 
model.ODE['y2'].set('mu1*y1 - mu2*y2')
model.ODE['y3'].set('mu2*y2 - mu3*y3')

# Load data from a matlab file
from scipy.io import loadmat
import numpy as np
matobject 	= loadmat('noinitcond_data.mat')
dataA 		= matobject['dataA']
dataB 		= matobject['dataB']
timeA 		= matobject['timeA'][0]  # convert from a 1,9 ndarray to a (9,) ndarray
timeB 		= matobject['timeB'][0]  # convert from a 1,9 ndarray to a (9,) ndarray
# this data is 3 x 9

# Create a APTdata object to store dataA in
experiment1 = apt.aptdata('dataA',dataA,states=['y1','y2','y3'],time=timeA,lnFlag=False)

"""
 	Since we do not have data at inital conditions, we must let the model know where to integrate
 	from. The user must pass this initial condition list, in the order dictated by model.showStates().
	This is also the same order that the user set in the first place.
	This list can contains values (ex. 0) and can contain variables enclosed between ''.
	The user then needs to provide a dictionary of these variables and their associated parameter index
	as returned by model.showPar()
"""
model.add_par('y1InitialA',0, 1000)
"""
State 1, y1, is completely unmeasured, so we define a parameter, y1InitialA.
We use the aptdata.fitInitialCondition(dict) command, where dict is a dictionary of
   (key)   :     (value)
State Name : parameterName

Note that a unique parameter needs to be defined for each experiment (unless you WANT the initial
contitions between each experiment to be the same).
"""
experiment1.fitInitialCondition({'y1':'y1InitialA'})

"""
	Additionally, we do not know how long ago that the initial condition occured. We must define a
	timeshift parameter to fit!
"""
model.add_par('unknownTimeA',0, 10) 		# running model.showPar() informs us that this is at index 7
print('Adding parameter, unknownTimeA at parameter index 7\n\n')
experiment1.set_tshift_par(7)

# Put this object into the APTModel object
model.add_data(experiment1)

# Repeat this for dataB
experiment2 = apt.aptdata('dataB',dataB,states=['y1','y2','y3'],time=timeB,lnFlag=False)
model.add_par('y1InitialB',0, 1000)
experiment2.fitInitialCondition({'y1':'y1InitialB'})
model.add_par('unknownTimeB',0,10)
experiment2.set_tshift_par(9)
model.add_data(experiment2)

model.showpar()

# Create an options variable for model
options = apt.aptoptions(maxThr=8)
options.nSwap= 1e3
options.filename = "noInitialCondition"
# options.show()	# Should run this and double check things are okay before generating

model.generate(options)