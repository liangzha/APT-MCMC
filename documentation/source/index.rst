.. APT-MCMC documentation master file, created by
   sphinx-quickstart on Thu Mar 31 09:34:09 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to APT-MCMC's documentation!
====================================

In order to get started, going through the Requirements is recommended to ensure your system has the correct libraries to run APT-MCMC simulations. After that, consult the `Tutorial`_ for a Quick-Start guide on parameter fitting, which should covers parameter fitting to a 2-state, 2-parameter ODE system. This should cover the majority of use-cases.

For advanced fitting requirements, consult documentation regarding the APT-MCMC object that best pertains to your requirements. In a worst case scenario, APT-MCMC can be used to generate the bulk (and very repetative) aspects of C++ simulation code, which may then be edited directly.


Supports:

* Automatic `C++`` generation if the user can specify a model within the confines of APTmodel
* Unknown initial conditions are supported and can be parameters.
* A data timeshift can be a parameter. This is in the event of data being collected at some time after the true beginning of a dynamical system. A typical use case would be hospital data, where data is relative to enrollment rather than disease time zero.
* Infusions (ramp, bolus, zero-order-hold, or any combination thereof) are fully supported, but magnitudes, times, and durations **MUST** be known. They cannot be parameters.

.. note:: Advanced use case: Infusions can be combined with a timeshift as long as it is known when the infusion occured, relative to the start of data collection.



.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   Requirements <requirements.rst>
   Tutorial <tutorial.rst>
   APTmodel <aptmodel.rst>
   APTexperiment <aptexperiment.rst>
   APTdata <aptdata.rst>
   APToptions and Generating Simulations <aptoptions.rst>
   Troubleshooting <trouble.rst>





.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`


.. _`Tutorial`: ./tutorial.html
