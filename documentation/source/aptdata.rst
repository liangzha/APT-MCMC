APTdata
=======

This class stores a piece of data. Data is associated with timepoints and a state.

.. note:: Initial conditions and timepoints for your experiment will defined using this!

Initializing an experiment
--------------------------

**apt.data(values,states, isLog=False, numerical_min=1e-14)**

  Initialize an APTdata object. This will be added to an experiment later. **Required.**

  Args:

  * values (array-like): An matrix of values that can be an array or vector. NumPy arrays are highly recommended. These values must be entered as follows: Rows pertain to states. Columns pertain to times.
  * states (list of str): A list of strings containing the state names that each row in **values** pertains to.
  * time (array-like): A list or numpy array of monotonically increasing values that each column in **values** pertains to.
  * isLog (bool): Flag to notify APT if **values** is in log-space(True) or in linear-space(False). If **values** is not in log-space, it will log it for you.
  * numerical_min (float): A number determining the minimum numerical-threshold when taking the natural log of your data (if and only if **isLog** is False)

  Returns:

  * APTdata object. This object will contain all information about this piece of data. You will add it to an APTexperiment object later on.

  Example::

    aptdata = apt.data(values, states=['C_a','C_b','C_c'], time=[0,1,2,5,10,15,20], isLog=False, numerical_min=0.1)

.. _in-initcond:

Initial Conditions
------------------

Often times, data for a state is unavailable, but an initial condition is known. To enter initial conditions::

  initcond = apt.data([0, 0, 0.1, 3.4], states=['C_a','C_b','C_c', 'C_d'], time=[0], isLog=False, numerical_min=0.1)


Recommendations
---------------

.. note:: Simulation times will be automatically determined by your data. The earliest timepoint specified will automatically be the times that you need to specify initial conditions.

.. note:: Data for every single state does not need to be specified! However, initial condtitions **MUST** be specified: either via data (see :ref:`in-initcond`.) or fitting initial conditions (see `Fitting Initial Conditions in APTexperiment`_).

.. note:: If some of your data is 0 (including initial conditions), it is recommended setting **numerical_min** to be something large such as 0.1. The reason is that in log-space, your data value might be converted to -32 and the fitting routine will prioritize that data point over all else.


.. _`Fitting Initial Conditions in APTexperiment`: ./aptexperiment.html#in-fitinitialcond
