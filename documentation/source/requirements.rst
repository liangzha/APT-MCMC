Required Libraries
==================

APT-MCMC requires the following libraries to be installed: 

* Python 3 (any version)
* :ref:`in_sundials`
* :ref:`in_trng`

Keep track of their installation locations because you will need to set their paths.

.. _in_sundials:

Sundials v2.6.2
---------------
.. highlight:: console

.. note:: This guide pertains to version ``2.6.2`` only!

Download the `sundials-2.6.2.tar.gz`_ package.

Unzip and create build directories::

	$ tar -xvf sundials-2.6.2.tar.gz
	$ mkdir builddir
	$ cd builddir

Now build with the following commands, assuming the installation directory is ``/usr/local`` (replace that with the directory of your choice). Remember that your installation directory must not have a trailing slash: ``/``.

::

	$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local ../
	$ make
	$ make install

Windows-Specific Notes
~~~~~~~~~~~~~~~~~~~~~~

Cygwin is recommended. Ensure the following packages are installed:

* ``wget``
* ``gcc (gnu)``
* ``g++ (gnu)``
* ``gcc-fortran (gnu)``
* ``make (gnu)``
* ``ccmake``
* ``lapack``
* ``patch``

These packages are available in the category ``Dlevel`` or ``Utils`` when you select them via the Cygwin installer.

You can try your luck with the installation procedures above, but chances are that your verison of Cygwin fails during the make stage due to an error due to ``CLOCK_MONOTONIC_RAW``.

In this case, try this (in the builddir directory)::

	$ make clean
	$ ccmake ../

You will be in an interactive screen. Match the settings:

* Build ARKODE ON
* Build CVODE ON
* Build CVODES ON
* Build IDA ON
* Build IDAS ON
* Build KINSOL ON
* Build SHARED LIBS OFF
* Build STATIC LIBS ON
* ....
* EXAMPLES ENABLE OFF
* FCMIX ENABLE OFF
* LAPACK ENABLE ON
* MPI ENABLE OFF

Press ``c`` to configure and then ``e`` to ignore the warning about **CMAKE_LEGACY**. Ensure no other errors are encountered. If so, press ``g`` to generate make files and exit out of the interactive screen.

Now try::

	$ make 
	$ make install


.. _in_trng:

TRNG v4.19
----------

TRNG is a random number library designed for use with MCMC simulations. `TRNG`_ may be obtained here.

Compilation instructions will be added soon.

.. _TRNG: http://numbercrunch.de/trng/
.. _sundials-2.6.2.tar.gz: https://computation.llnl.gov/casc/sundials/main.html