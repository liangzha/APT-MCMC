APToptions and Generating Simulations
=====================================

.. role:: pyth(code)
  :language: python

This class stores all of the relevant simulation and hyperparameter options for your run. Once an options object is created with the desired settings, call APTmodel's :pyth:`generate()` function to finish creating the simulation.

.. note:: You can save an APToptions object (or several) and apply them to an APTmodel object to easily experiment with different sets of hyperparameters for a run.

Quick Start
-----------

.. code-block:: python

	**Assumes an APTmodel "model" is previously defined**
	>> options = apt.aptoptions(maxThr=4) # Default options with 4 parallel threads allocated for simulation
	>> options.filename = "name_of_complied_executable"
	>> options.show() # Double check the options
	===aptoptions object===
	Core Settings
	|nTemp        =        4, number of parallel ensembles to run
	|nStep        =       25, number of MCMC steps to take before attemping location swaps between temperatures
	|nEnsemble    =      100, number of samplers exploring each temperature simultaneously
	|nSwap        =    1e+03, number of swaps to attempt before ending simulation
	|nInit        =    1e+03, number of initial MC samples to take before starting simulation

	Display Settings
	|dispInterval =    1e+02, display info after every _interval_ swaps
	|StatusBar    =    On

	Save Settings
	|filename     =    experimental_par_const, name of compiled executable
	|saveFile     =    mcmcOUT, name of resultsfile (.mcmc will be appended to this)
	|saveInterval =    1e+03, partial run saving at _interval_

	Auto-adaptation (of step-size, &temperatures) Settings
	|adapt_last   =        0, stop adapting values after this swap attempt
	|min_adapt_factor = 0.8, min multiplier when adapting values
	|max_adapt_factor = 1.25, max multiplier when adapting values
	Beta (to help swap acceptance %)
	 | optimal_swap_accept %   = 0.24
	 | adapt_beta_interval     = 1000, adapt beta after every _interval_ swaps
	 | adapt_beta_rate         = 0.04, fine-tune how beta gets modified
	 Stepsize (to help step acceptance %)
	 | optimal_step_accept %   = 0.3
	 | adapt_step_interval     = 1000, adapt stepsize after every _interval_ swaps
	 | adapt_step_rate         = 0.15, fine-tune how stepsize gets modified

	step_size   =        3, initial step size value (2-3.5) recommended

	Other Options
	|bigEnergy    =    1e+29, in case of integration failure or prior violation, use this value instead of infinity
	|numerical_min=    1e-14, logspace is used in objective function so set a minimum value to avoid log(0) issues
	|cvode_rtol   =    1e-08, rel tol for cvode (lower values increase simulation time)
	|cvode_atol   =   0.0001, abs tol for cvode (lower values increase simulation time)
	|cvode_max_steps = 1000, max steps to take between points

	>> options.nSwap = 1e6 # Last minute change
	>> model.output("name_of_simulation_folder")
	>> model.generate(options)

The codeblock above initiates a default APToptions object with default settings. Using the :pyth:`show()` function of the object displays all of the names of modifiable objects within the class(e.g. **step_size**: :pyth:`options.step_size = 3.5`) Once the options are set, call the APTmodel function :pyth:`generate(APToptions object)` to finish auto-generating the simulation.


Defining Options
----------------

**apt.aptoptions(maxThr, nTemp, nStep, nEnsemble, nSwap, nInit, dispInterval, statusBar, saveInterval, saveFile, filename, bigEnergy)**

  Initialize an APToptions object. **Required.**

  Args:

  * maxThr (int, 8 default): Maximum number of CPU threads to use for this simulation. **This number will be automatically lowered to the maximum number of threads available on your machine if you input too high of a number**
  * nTemp (int, 4 default): Number of tempered simulations.
  * nStep (int, 25 default): Number of simulation steps before a parameter swap between temperatures is attempted.
  * nEnsemble (int, 100 default): Size of ensemble (number of samplers) at each temperature.
  * nSwap (int, 10000 default): Number of inter-temperature swap attempts. Simulation will end after the last swap attempt.
  * nInit (int, 1000 default): Number of random parameters to test to distribute the locations of each sampler prior to starting the simulation. **Set to 1 to turn off**
  * dispInterval (int, 100 default): Displays simulation status after every *dispInterval* swap attempts. To turn off, set this to a number higher than *nSwap*.
  * statusBar (int, 1 default): Set to 1 (on) or 0 (off) to display a progress bar between each *dispInterval* number of swaps. 
  * saveInterval (int, 1000 default): Save partial MCMC results after every *saveInterval* swap attempts. To turn off, set this to a number higher than *nSwap*.
  * saveFile (str, "mcmcOUT" default): Filename of results object (.mcmc extension).
  * filename (str, "apt_mcmc" default): Filename of simulation executable.
  * bigEnergy (large int, 1e29 default): Large number to penalize parameters in certain scenarios. Increase this only if your objective function values exceed 1e29 (but you would probably be having poor performance anyway with such large values and you should scale them down considerably).


  Returns:

  * APToptions. This object will contain all hyperparameters and simulation settings and is independent of your simulation. Additional settings can be changed by directly modifying the APToptions object.

  Example::

    options_A = apt.aptoptions(maxThr=20, nTemp=10, nEnsemble=200)


Generating APT simulations
--------------------------

Once your APTmodel and APToptions are defined, it is time to generate the APT simulation and convert everything into a compile-able C++ executable. The conversion results in quite a number of files so it will automatically output the contents into a subdirectory of the current folder. 

**model.output(folderName)**

  Set the subdirectory where you want the simulation files to be placed into.

  Args:

  * folderName (str, "Output" default): Name of a folder (if it does not exist, it will be created for you) that you want the auto-generated simulation files to go. 

  Returns:

  Nothing


**model.generate(options)**

  Generate APT simulation files in accordance to the options that you set.

  Args:

  * options (APToptions object)

  Returns:

  Nothing. Check your output directory for the files and compile them with `make`.


Compiling APT simulations
-------------------------

Enter your directory containing the newly generated APT simulation source code files. Make the program and then begin running your simulation.

.. code-block:: console
	
	$ cd [simulation directory]
	$ make
	$ ./[executable name]

Once this is running, you can monitor the statistics for the first couple of swap attempts to ensure the simulation is healthy and doing what is intended. 

.. note:: One sure-fire way to tell that your run is not going as intended is when you see the log likelihood of all samplers to be 1e29 (or whatever you set **bigenergy** to be in the options). Stop it and double check to see if your code is doing what you intended.

Once it is running smoothly, continue monitoring the simulation by analyzing the partial save results every now and then.

.. note:: If the MCMC statistics are healthy and suggests convergence, then you can stop the run prematurely and use the partial results. For this reason, I would recommend you set **nSwaps** in the options to be larger than you think is necessary (subject to memory requirements of course).