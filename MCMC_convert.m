function ReadMCMC(fileName)

myFile = java.io.File(fileName);
% len = length(myFile)/8; % Get size of file in bytes, divide by size of double to get number of elements
fileOut = fileName(1:end-5);

fileID = fopen(fileName);
cfg.nChains     = fread(fileID, 1, 'double');
cfg.nSteps      = fread(fileID, 1, 'double');
cfg.nWalkers    = fread(fileID, 1, 'double');
cfg.nSwaps      = fread(fileID, 1, 'double');
cfg.nPars       = fread(fileID, 1, 'double');
cfg.lastswap        = fread(fileID, 1, 'double');
cfg.init_steps      = fread(fileID, 1, 'double');
cfg.adapt_beta_interval   = fread(fileID, 1, 'double');

energy_chain = fread(fileID, cfg.nChains*cfg.nWalkers*cfg.nSwaps ,'double');
par_chain    = fread(fileID, cfg.nChains*cfg.nWalkers*cfg.nSwaps*cfg.nPars, 'double');
step_accept  = fread(fileID, cfg.nChains*cfg.nSwaps,'int');
swap_accept  = fread(fileID, cfg.nChains*cfg.nSwaps,'int');
beta_hist    = fread(fileID, cfg.nChains*cfg.nSwaps,'double');
acor         = fread(fileID, cfg.nPars, 'double');

fclose(fileID);

% Post processing
% energy_chain is nChain x nWalkers x nSwap
energy_chain = reshape(energy_chain,cfg.nSwaps, cfg.nWalkers, cfg.nChains);
energy_chain = permute(energy_chain,[3 2 1]);
% energy_chain = energy_chain(:,:);

% par_chain is nChain x nWalkers x nSwap x nPars
par_chain = reshape(par_chain,cfg.nPars, cfg.nSwaps, cfg.nWalkers, cfg.nChains);
par_chain = permute(par_chain,[4 3 2 1]);

% step_accept is nChain x nSwap
step_accept = reshape(step_accept, cfg.nSwaps, cfg.nChains)';

% swap_accept is nChain x nSwap
swap_accept = reshape(swap_accept, cfg.nSwaps, cfg.nChains)';

% beta_hist is nChain x nSwap
beta_hist = reshape(beta_hist, cfg.nSwaps, cfg.nChains)'; %#ok<*NASGU>


save(fileOut,'cfg','energy_chain','par_chain','step_accept','swap_accept','beta_hist','acor','-v7.3');

samples = par_chain(1,:,:,:);
samples = squeeze(samples);

samples = permute(samples,[3 1 2]);
samples = samples(:,:);
diag(cov(samples'))

end
