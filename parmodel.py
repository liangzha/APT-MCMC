# -*- coding: utf-8 -*-
"""
Goal is to call

cvode(rhs, tspan, y0, par)

and rhs is
def rhs(t,y, par)
   Ca,Cb,... = y
   dCa = f(t,y,par)
   dCb = ..
   
   return np.array([dCa, dCb, ...])
   

"""

from assimulo.solvers import CVode
from assimulo.problem import Explicit_Problem

class parmodel:
    def __init__(self):
        self.par = None
        
    def rhswrapper(self,t,y):
        return rhs(t,y,self.par)
        
    def sim(self, rhs, tspan, y0, par):
        self.par = par
        model= Explicit_Problem(self.rhswrapper, y0, tspan[0])
        sim = CVode(model)
        
        if len(tspan) == 2:  # let CVode pick integration times
            t,y = sim.simulate(tspan[1])
        else:                # Specified integration times
            t,y = sim.simulate(tspan[-1],ncp=0, ncp_list=tspan)
        
        return t,y
    
    

#import numpy as np
#import matplotlib.pyplot as plt
#
#p =np.array([[0,1],[-2,-1]])
#y0=np.array([1.0,1.0])
#t0 = 0.0
#tfinal = 10.0        #Specify the final time
#def rhs(t,y,p):
#    A = p
#    yd=np.dot(A,y)
#    
#    return yd
#
#
#model = parmodel()
#t,y = model.sim(rhs, [t0, tfinal], y0, p)
#    
#plt.plot(t,y)