# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 21:48:20 2015

@author: aznef
"""
from collections import OrderedDict
import numpy as np
import copy
import csv
import os
import subprocess


class aptoptions:

    """
    Contains APT-MCMC Settings
    """

    def __init__(self, maxThr=8, nTemp=4, nStep=25, nEnsemble=100, nSwap=10000, nInit=1000, dispInterval=100, statusBar=1,
        saveInterval=1000, saveFile="mcmcOUT", filename="apt_mcmc", bigEnergy=1e29, adapt_last=0, min_adapt_factor=0.8,
        max_adapt_factor=1.25, optimal_swap_accept=0.24, adapt_beta_interval=1000, adapt_beta_rate=0.04, optimal_step_accept=0.30,
        adapt_step_interval=1000, adapt_step_rate=0.15, step_size=3.0, numerical_min=1.0e-14, cvode_atol=1e-4, cvode_rtol=1e-8, cvode_max_steps=1000):
        # Core settings
        self.maxThr = int(maxThr)
        self.nTemp = int(nTemp)
        self.nStep = int(nStep)
        self.nEnsemble = int(nEnsemble)
        self.nSwap = int(nSwap)
        self.nInit = int(nInit)

        # Display settings for results
        self.dispInterval = int(dispInterval)
        self.StatusBar = int(statusBar > 0)

        # Save settings
        self.saveInterval = int(saveInterval)
        self.saveFile = saveFile
        self.filename = filename

        # Master Hyperparameter Auto-adaptation settings
        self.adapt_last = int(adapt_last)
        self.min_adapt_factor = min_adapt_factor
        self.max_adapt_factor = max_adapt_factor

        # Adaptation settings to improave swapping rates between parallel
        # tempered ensembles
        self.optimal_swap_accept = optimal_swap_accept
        self.adapt_beta_interval = int(adapt_beta_interval)
        self.adapt_beta_rate = adapt_beta_rate

        # Adaptation settings to improve step acceptance rates for samplers
        self.optimal_step_accept = optimal_step_accept
        self.adapt_step_interval = int(adapt_step_interval)
        self.adapt_step_rate = adapt_step_rate

        # Initial step size for samplers
        self.step_size = step_size

        # Other options
        self.bigEnergy = bigEnergy
        self.numerical_min = numerical_min

        # CVode integration options
        self.cvode_rtol = cvode_rtol
        self.cvode_atol = cvode_atol
        self.cvode_max_steps = int(cvode_max_steps)

    def show(self):
        print('===aptoptions object===')
        print('Core Settings')
        print('|nTemp        = {0:8d}, number of parallel ensembles to run'.format(
            self.nTemp))
        print('|nStep        = {0:8d}, number of MCMC steps to take before attemping location swaps between temperatures'.format(
            self.nStep))
        print('|nEnsemble    = {0:8g}, number of samplers exploring each temperature simultaneously'.format(
            self.nEnsemble))
        print('|nSwap        = {0:8.2g}, number of swaps to attempt before ending simulation'.format(
            self.nSwap))
        print('|nInit        = {0:8.2g}, number of initial MC samples to take before starting simulation'.format(
            self.nInit))
        print('\nDisplay Settings')
        print('|dispInterval = {0:8.2g}, display info after every _interval_ swaps'.format(
            self.dispInterval))
        print('|StatusBar    =    ', end="")
        if self.StatusBar:
            print('On')
        else:
            print('Off')
        print('\nSave Settings')
        print('|filename     =    {:s}, name of compiled executable'.format(self.filename))
        print('|saveFile     =    {:s}, name of resultsfile (.mcmc will be appended to this)'.format(self.saveFile))
        print('|saveInterval = {0:8.2g}, partial run saving at _interval_'.format(
            self.saveInterval))
        print('\nAuto-adaptation (of step-size, &temperatures) Settings')
        print('|adapt_last   = {0:8d}, stop adapting values after this swap attempt'.format(self.adapt_last))
        print('|min_adapt_factor = {:g}, min multiplier when adapting values'.format(self.min_adapt_factor))
        print('|max_adapt_factor = {:g}, max multiplier when adapting values'.format(self.max_adapt_factor))
        print('Beta (to help swap acceptance %)')
        print(' | optimal_swap_accept %   = {:2g}'.format(self.optimal_swap_accept))
        print(' | adapt_beta_interval     = {:d}, adapt beta after every _interval_ swaps'.format(self.adapt_beta_interval))
        print(' | adapt_beta_rate         = {:g}, fine-tune how beta gets modified'.format(self.adapt_beta_rate))
        print(' Stepsize (to help step acceptance %)')
        print(' | optimal_step_accept %   = {:2g}'.format(self.optimal_step_accept))
        print(' | adapt_step_interval     = {:d}, adapt stepsize after every _interval_ swaps'.format(self.adapt_step_interval))
        print(' | adapt_step_rate         = {:g}, fine-tune how stepsize gets modified'.format(self.adapt_step_rate))
        print('\nstep_size   = {0:8g}, initial step size value (2-3.5) recommended'.format(self.step_size))
        print('\nOther Options')
        print('|bigEnergy    = {:8g}, in case of integration failure or prior violation, use this value instead of infinity'.format(self.bigEnergy))
        print('|numerical_min= {:8g}, logspace is used in objective function so set a minimum value to avoid log(0) issues'.format(self.numerical_min))
        print('|cvode_rtol   = {:8g}, rel tol for cvode (lower values increase simulation time)'.format(self.cvode_rtol))
        print('|cvode_atol   = {:8g}, abs tol for cvode (lower values increase simulation time)'.format(self.cvode_atol))
        print('|cvode_max_steps = {:d}, max steps to take between points'.format(self.cvode_max_steps))

class par:

    def __init__(self, name=[""], LB=[], UB=[], experiment_dependent = False):
        self.name = name
        self.LB = LB
        self.UB = UB
        self.__experiment_dependent = False


class ode_equation:

    def __init__(self, name):
        self.name = name
        self.equation = ''
        self.helper = []

    def set(self, expr):
        self.equation = ''.join([self.name, '_dot = ', expr, ';'])

    def add_helper(self, expr):
        self.helper.append(''.join(['realtype ', expr, ';']))

    def show(self):
        print('===ode_equation object: {:s}==='.format(self.name))
        if self.helper:  # if __helper contains something
            for expr in self.helper:
                print(expr)
        print(self.equation)

class data: 
    """
    Stores data for APT-MCMC. Data must be nStates x nTime
    """
    def __init__(self, values, states, time, isLog=False, numerical_min=1e-14, weights=[]):
        if np.ndim(time)==0:
            time = [time]
        self.time = np.array(time)

        values = np.array(values, dtype=np.float)
        if np.ndim(values)==0:
            values = values[np.newaxis][np.newaxis]
            self.nStates = values.shape[0]
            self.nTimes =  values.shape[1]

        elif np.ndim(values)==1:
            values = values[np.newaxis]
            self.nStates = values.shape[0]
            self.nTimes =  values.shape[1]
            if len(states) != self.nStates:
                values = values.T
                self.nStates = values.shape[0]
                self.nTimes =  values.shape[1]
            assert len(states) == self.nStates, "Dimension 0 of data matrix must be equivalent to number of states!\nDimension 1 of data matrix must be equivalent to number of timepoints!"

        elif np.ndim(values)==2:
            self.nStates = values.shape[0]
            self.nTimes =  values.shape[1]
            assert len(states) == self.nStates, "Dimension 0 of data matrix must be equivalent to number of states!"
            assert len(time) == self.nTimes, "Dimension 1 of data matrix must be equivalent to number of timepoints!"

        if len(weights)==0:
            self.weights = np.ones([self.nTimes, self.nStates])  # Set default weights to 1
        else:
            self.set_weights(weights)

        """
        Take the log of data if it isn't already.
        Add 1 to the data regardless in order to prevent squaring small errors and to prevent negative values.
        """
        if isLog is False:
            self.raw_values = np.copy(values)
            # values[values < numerical_min] = values[values < numerical_min] + 1
            self.values = np.log(self.raw_values + 1)
        else:
            self.values = np.copy(values)
            self.raw_values = np.exp(self.values)


        self.states = OrderedDict()
        counter = 0
        for state in states:
            self.states[state] = counter
            counter += 1

        self.noInitCondition = False
        self.tshift = False

    def set_weights(self, wts):
        # Process data weights
        if self.nTimes>1:
                assert wts.shape[0] == self.nTimes,  "Data weights are mismatched in nTimes with Data values!"
        if self.nStates >1:
            assert wts.shape[1] == self.nStates, "Data weights are mismatched in nStates with Data values!"
        self.weights = np.resize(wts, [self.nTimes,self.nStates])

class experiment:

    def __init__(self, name):
        if not name.replace("_","").isalnum(): # throw error if name contains char other than alphanumerics, '_' is allowed.
            raise NameError("Experiment names *MUST* be alphanumerical and _ characters only!")

        self.time = []
        self.name = name

        self.nStates = 0
        self.nTimes = 0

        self.values = np.array([])

        self.states = OrderedDict()
        self.noInitCondition = False
        self.tshift = False

        self.infus_zoh = np.nan
        self.infus_ramp = np.nan
        self.infus_bolus = np.nan

        self.initcond = {} # dictionary of initial conditions
    
    def add_data(self, data):
        # If experiment has no data at all,
        if self.nTimes == 0:
            self.values = np.copy(data.values)
            self.time = np.copy(data.time)
            self.nTimes = data.nTimes
            self.nStates = data.nStates
            self.states = copy.deepcopy(data.states)
            self.rawvalues = np.copy(data.raw_values)   # Save raw values from data due to the building of initial conditions
            self.initcond = {}
            for state in data.states:
                self.initcond[state] = data.raw_values[data.states[state],0]
            self.init_cond_time = data.time[0]
            self.weights = data.weights

        else:
            # Parse through the states in the data and add them to the experiment
            for state in data.states.keys():
                # If state is already defined in experiment:
                if state in self.states:
                    # Check if any of the experiment's times are the same as the data's
                    #   get indices of overlap
                    Toverlap_data = np.in1d(self.time,data.time).nonzero()[0]
                    Toverlap_self = np.in1d(data.time,self.time).nonzero()[0]
                    if len(Toverlap_data) != 0:
                        # check if data is equal, otherwise warn user
                        flag = np.all(data.values[data.states[state],Toverlap_self] == self.values[self.states[state],Toverlap_data])
                        if flag == False:
                            print("Warning: Values in state \"%s\" are being redefined! Check if this was intended."%state)
                        else:
                            print("All data for state \"%s\" already exists."%state)
                            return

                # If state is not already defined, add it
                else:
                    # NOTE, FOR SOME REASON, I am stupid
                    # I made values and rawvalues to be nState x nTime
                    # But.. its supposed to be nTime x nState. Too lazy to change because 
                    # it currently works...

                    # Add a new dict entry for the state and copy its data into it
                    self.states[state] = self.nStates

                    # Copy data values (raw and logged) into the experiment. 
                    # Insert nans to preallocate the space.
                    self.values        = np.insert(self.values, self.nStates, np.nan, axis=0)
                    self.rawvalues     = np.insert(self.rawvalues, self.nStates, np.nan, axis=0)
                    self.weights       = np.insert(self.weights, self.nStates, 1, axis=1)
                    self.nStates += 1
                    # print("Inserting new state, {:s}.".format(state))  # Commented out because it is too verbose


                # Save the state's data into the experiment
                newtimes = np.setdiff1d(data.time, self.time) # find values of new times that are to be inserted
                # print("Inserting new time(s): ",end="")
                # for t in newtimes:
                #     print("{:g} ".format(t),end=" ")
                # print("\n")
                self.time = np.union1d(self.time,data.time) # update experiment times
                inserted_indices = np.in1d(self.time,newtimes).nonzero()[0] # insertion idx of new timepoints 
                inserted_indices[inserted_indices>self.values.shape[1]] = self.values.shape[1] # Often, we want to insert indices after the current length of self.values, which freaks np.insert out. This prevents it from inserting at an index larger than the current length
                self.values = np.insert(self.values, inserted_indices, np.nan, axis=1) # insert nans into rows of new times into experimental data
                self.rawvalues = np.insert(self.rawvalues, inserted_indices, np.nan, axis=1) # insert nans into rows of new times into raw experimental data
                self.weights = np.insert(self.weights, inserted_indices, 1, axis=0) # insert 1 into rows of new times into data weights

                inserted_indices = np.in1d(self.time,data.time).nonzero()# index of data times in new experiment times
                self.values[self.states[state], inserted_indices] = data.values[data.states[state],:] # Copy data into experimental data
                self.rawvalues[self.states[state], inserted_indices] = data.raw_values[data.states[state],:] # Copy rawdata into raw experimental data
                self.weights[inserted_indices, self.states[state]] = data.weights[:,data.states[state]] # Copy data weights into experimental weights

                self.nTimes = len(self.time)

                # If the new data contains timepoints earlier than previously encountered, remove all previous initial conditions and use available data for new inits
                if np.min(data.time) < self.init_cond_time:
                    self.init_cond_time = np.min(data.time)
                    self.initcond = {}
                    self.initcond[state] = data.raw_values[data.states[state],0]
                elif np.min(data.time) == self.init_cond_time:
                    self.initcond[state] = data.raw_values[data.states[state],0]

    def add_infusion(self, zoh=np.nan, ramp=np.nan, bolus=np.nan):
        # User provides array-like structures for zero-order-hold, ramp, and bolus infusions
        if not np.all(np.isnan(zoh)):
            zoh = np.array(zoh)
            assert (zoh.shape[0] > 0 and zoh.shape[1] >= 2), "Zero-order-hold infusion array is ill-conditioned! Array size must be nonzero; it must contain a time column and least 1 infusion column. If no infusion available, pass a np.nan argument."
            # Check for monotonicity
            for i in range(1,zoh.shape[0]):
                assert zoh[i,0] > zoh[i-1,0], "Zero-order-hold infusion array must contain monotonically increasing timepoints."
            self.infus_zoh = zoh

        if not np.all(np.isnan(ramp)):
            ramp = np.array(ramp)
            assert (ramp.shape[0] > 0 and ramp.shape[1] >= 2), "Ramp infusion array is ill-conditioned! Array size must be nonzero; it must contain a time column and least 1 infusion column. If no infusion available, pass a np.nan argument."
            # Check for monotonicity
            for i in range(1,ramp.shape[0]):
                assert ramp[i,0] > ramp[i-1,0], "Ramp infusion array must contain monotonically increasing timepoints."
            self.infus_ramp = ramp

        if not np.all(np.isnan(bolus)):
            bolus = np.array(bolus)
            assert (bolus.shape[0] > 0 and bolus.shape[1] >= 2), "Bolus infusion array is ill-conditioned! Array size must be nonzero; it must contain a time column and least 1 infusion column. If no infusion available, pass a np.nan argument."
            # Check for monotonicity
            for i in range(1,bolus.shape[0]):
                assert bolus[i,0] > bolus[i-1,0], "Bolus infusion array must contain monotonically increasing timepoints."
            self.infus_bolus = bolus

    def get_time(self):
        return self.time

    def fitInitialCondition(self, dict_input):
        # User provides a dictionary of {'states': 'parameter'} if they want initial conditions fitted
        self.noInitCondition = True
        # self.init_dict = dict_input
        for state in dict_input:
            self.initcond[state] = dict_input[state]

    def set_tshift_par(self,tshift_name):
        self.tshift = True
        self.tshift_name = tshift_name

    # Print all the data in this experiment in linear space. 
    def show(self, showall=False): 
        print('===apt.experiment object: {:s}==='.format(self.name))
        print('nState x nTime aptdata matrix ({:d} x {:d})'.format(
            self.nStates, self.nTimes))
        print('Preview (in linear space): ')

        print("          ",end="")
        for state in self.states:
            print('{:9s}'.format(state), end="")
        
        if self.nTimes <= 5 or showall== True:
            for t in range(0, self.nTimes):
                print("\n t={:<3}".format(self.time[t]),end="")
                for state in self.states:
                    print('{:5.2e}'.format(self.rawvalues[self.states[state],t],13 ),end=" ")
        else:
            for t in range(0, np.min([3,self.nTimes]) ):
                print("\n t={:<3}".format(self.time[t]),end="")
                for state in self.states:
                    print('{:5.2e}'.format(self.rawvalues[self.states[state],t],13 ),end=" ")
            print("\n ... ")

            for t in range(self.nTimes-2,self.nTimes):
                print("\n t={:<3}".format(self.time[t]),end="")
                for state in self.states:
                    print('{:5.2e}'.format(self.rawvalues[self.states[state],t],13 ),end=" ")
                
        if not np.all(np.isnan(self.infus_zoh)):
            print("\n==Infusion: Zero Order Hold==")
            for i in self.infus_zoh:
                print(i)

        if not np.all(np.isnan(self.infus_ramp)):
            print("\n==Infusion: Ramp==")
            for i in self.infus_ramp:
                print(i)

        if not np.all(np.isnan(self.infus_bolus)):
            print("\n==Infusion: Bolus==")
            for i in self.infus_bolus:
                print(i)

        print("\n==========================================================\n")

class aptmodel:
    """
    A series of ODEs containing states, parameters, etc
    """

    def __init__(self, name=""):
        self.name = name
        # self.annotation = ""
        self.include_path = ''
        self.lib_path = ''
        
        self.__reset()

    def __reset(self):
        self.listOfParameters = OrderedDict()
        self.listOfStates = OrderedDict()
        self.ODE = OrderedDict()
        self.ODE['Constants'] = ode_equation('_Constants_')
        self.Parameters = []
        self.dependent_pars = []

        self.logMode = True

        self.dependent_const = []
        self.__dependent_const_flag = False

        self.nPar = 0
        self.nStates = 0
        self.nExp = 0
        self.Exp = OrderedDict()
        self.nExperimentalPars = 0

        self.__infusions = False
        self.listOfInfusions = {}

        self.initcond_raw = []

        self.outputDir = 'Output/'

        self.benchmark_flag = False
        self.__benchmark_preventGenerate = True

    def use_log_mode(self, value):
        assert type(value) is bool, "Must provide a boolean!"
        self.logMode = value

    def output(self,dirstr):
        temp = len(dirstr)
        if dirstr[temp-1] != '/':
            dirstr += '/'
        self.outputDir = dirstr

    def add_experiment(self, *args):
        for arg in args:
            assert type(arg) is experiment, "Input is not of apt.experiment type: %r" % arg
            assert np.all([ state in arg.initcond for state in self.listOfStates]), "Not all initial conditions are provided for experiment %s! Each state must have either data for the initial point OR be marked for fit using the data.fitInitialCondition function."%arg.name
            # print('Initial Conditions for Experiment %s'%arg.name)  # commented out because too verbose
            # for state in arg.initcond:
            #     if type(arg.initcond[state]) is str:
            #         print('   %s: fit (%s)'%(state, arg.initcond[state]))
            #     else:    
            #         print('   %s: %f'%(state, arg.initcond[state]))
            if self.__infusions:
                assert not (np.any(np.isnan(arg.infus_bolus)) and np.any(np.isnan(arg.infus_ramp)) and np.any(np.isnan(arg.infus_zoh))), "APT Infusion mode is set to true. Some kind of infusion array must be entered into experiment %s."%arg.name
                if not np.any(np.isnan(arg.infus_ramp)) and not np.any(np.isnan(arg.infus_zoh)):
                    assert arg.infus_ramp.shape[1] == arg.infus_zoh.shape[1], "Experiment %s has incompatible number of inputs in the ZOH and ramp infusion arrays. Ensure their column lengths are equal."%arg.name
            arg.weights[arg.weights<=0] = 1
            self.Exp[arg.name] = arg
            self.nExp += 1

    def enable_infusions(self, infusion_dict):
        self.__infusions = True
        counter = 1
        assert len(infusion_dict)>0, "List of infusion names must be nonzero if you enable infusions!"
        keylist = [i for i in infusion_dict.keys()]
        keylist = np.unique(keylist)
        if not all(x==(y-1) for x, y in zip(keylist, keylist[1:])): # elmeents strictly increasing by 1
            raise NameError("Infusion dictionary missing a key. Did you skip a number?")
        elif keylist[0] != 0:
            raise NameError("Infusion dictionary must have a key starting at 0.")
        self.listOfInfusions = infusion_dict
        

    def set_include_path(self, *args):
        self.include_path = ''
        for arg in args:
            if arg[0:2] == '-I':
                self.include_path = ''.join([self.include_path, ' ', arg])
            else:
                self.include_path = ''.join([self.include_path, ' -I', arg])

    def set_lib_path(self, *args):
        self.lib_path = ''
        for arg in args:
            if arg[0:2] == '-L':
                self.lib_path = ''.join([self.lib_path, ' ', arg])
            else:
                self.lib_path = ''.join([self.lib_path, ' -L', arg])

    def add_states(self, *args):
        """ Adds states into the APTmodel.

        Add the names of your model states into the model.

        Args:
            *args (str): Variable length argument of strings, one for each state the user wishes to add.
        """
        for i, state in enumerate(args):
            assert type(state) is str, "%r is not a string!" % state
            self.listOfStates[state] = self.nStates+1
            self.ODE[state] = ode_equation(state)
            self.nStates += 1

    def add_par(self, name, LB, UB):
        """ Adds parameters into APTmodel.

        Add a parameter along with its lower bound and upper bound. A uniform distribution bounded at [LB, UB] will be established for this parameter's prior. 

        Args:
            name (str): Name of the parameter.
            LB (float): Lower bound of the parameter.
            UB (float): Upper bound of the parameter.
        """ 
        if LB > UB:
            print(
                'User input of LB is greater than UB! Flipping bounds for you.')
            tmp = LB
            LB = UB
            UB = tmp
        self.Parameters.append(par(name, LB, UB))
        self.listOfParameters[name] = self.nPar
        self.nPar += 1

    def add_per_experiment_par(self, name, exp_dict):
        """
        Args:
            name (str): Name of the parameter.
            exp_dict  (dict):  { 'experiment_name','parname' }
        """
        self.dependent_pars.append( (name, exp_dict))
        self.nExperimentalPars += 1

    def add_per_experiment_constant(self, name, const_dict):
        """
        Args:
            name (str): Name of the constant.
            const_dict  (dict):  { 'experiment_name','constvalue' }
        """
        self.dependent_const.append( (name, const_dict) )
        self.__dependent_const_flag = True

    def load_params_from_csv(self, filename, verbose=True):
        # Provide the name of a csv file (supports w/ header or not)
        with open(filename) as csvfile:
            readLine = csv.reader(csvfile)
            for i, row in enumerate(readLine):
                try:
                    parname = row[0]
                    LB = float(row[1])
                    UB = float(row[2])
                    if LB > UB:
                        print(
                            '   Note: User input of par: {:s} LB is greater than UB! Flipping bounds for you.'.format(parname))
                        tmp = LB
                        LB = UB
                        UB = tmp
                    if verbose:
                        print(
                            'Par {:3d}: {:15s}, LB={:5.3g}, UB={:5.3g}'.format(i, parname, LB, UB))
                    self.Parameters.append(par(parname, LB, UB))
                    self.listOfParameters[parname] = self.nPar
                    self.nPar += 1
                except ValueError:
                    if i == 0:  # User probably has a header
                        pass
                    else:       # User probably has non#s in the csv file
                        raise NameError(
                            'Error encountered in {:s}.\nDouble check row {:d}'.format(filename, i))

    def showpar(self):
        print('===aptmodel object, showing Parameters===')
        print('The following parameters and bounds are loaded:')
        for i in range(0, self.nPar):
            print(
                'Par {:3d}: {:15s}, LB={:5.3g}, UB={:5.3g}'.format(i+1, self.Parameters[i].name, self.Parameters[i].LB, self.Parameters[i].UB))
        print('\n')

    def showstates(self):
        print('===aptmodel object, showing States===')
        print('The following states are set:')
        for state in self.listOfStates:
            print('State {:3d}: {:15s}'.format(
                self.listOfStates[state], state))
        print('\n')

    def listbenchmarks(self):
        print('===aptmodel Supports the following Benchmarks:===')
        print('1. Ackley Function (Scalable)\n2. Adjiman Function\n3. Alpine Function (Scalable)\n4. Bard Function\n5. Beale Function\n6. Bird Function \n7. Bohachevskey\n8.Booth\n9. Bukin \n10. Corana Function\n11. Davamaldi Function\n12. Devilliers Glasser Function\n13. EggHolder Function (Scalable)\n14. Griewank Function\n')

    def genbenchmark(self,func_no, options=aptoptions(), npar = 0):
        # Provite a function number (listed by using the self.listbenchmarks() above)
        # and generate the necessary aptfiles 

        # Erase and reset all aptmodel values, just to be safe
        self.__reset()
        self.benchmark_flag= True

        npar = int(npar)
        
        # Select Ackley Function
        if func_no==1:
            if npar <= 0:
                raise NameError('Ackley Function is scalable. Please specify a number of variables to test\n Example: 3 dimensions: aptmodel.runbenchmark(1, 3)')
            for i in range(1,npar+1):
                self.add_par('x_%i'%i, -35, 35)
            self.benchmark_energy = "\t// Ackley Function\n\tdouble sum1=0,sum2=0;\n\tfor (int i = 0; i < ___nPars___; ++i)\n\t{\n\t\tsum1 += par[i]*par[i];\n\t\tsum2 += cos(2*___pi___*par[i]);\n\t}\n\tenergy -= -20* exp(-.02*pow(sum1/___nPars___,0.5) ) - exp(sum2/___nPars___) + 20 + exp(1);"
            self.outputDir = 'Benchmark_Ackley/'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Alpine')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Adjiman Function
        elif func_no==2:    
            self.add_par('x_1',-1,2)
            self.add_par('x_2',-1,1)
            self.benchmark_energy = "\t// Adjiman Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy -= cos(x1)*sin(x2) - x1/(x2*x2+1) + 10;   // Function evaluation can be negative. Add a value to prevent it from being negative. */"
            self.outputDir = 'Benchmark_Adjiman'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Adjiman')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Alpine Function
        elif func_no ==3:
            if npar <= 0:
                raise NameError('Alpine Function is scalable. Please specify a number of variables to test\n Example: 3 dimensions: aptmodel.runbenchmark(3, 3)')
            for i in range(1,npar+1):
                self.add_par('x_%i'%i, -10, 10)
            self.benchmark_energy = "\t// Alpine Function\n\tfor (int i = 0; i < ___nPars___; ++i)\n\t{\n\t\tenergy -= std::abs(par[i] * sin(par[i]) + 0.1*par[i]);\n\t}"
            self.outputDir = 'Benchmark_Alpine'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Alpine')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Bard Function
        elif func_no ==4:
            self.add_par('x_1',-0.25,25)
            self.add_par('x_2',0.01,2.5)
            self.add_par('x_3',0.01,2.5)
            self.benchmark_energy = "\t// Bard Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tdouble x3 = par[2];\n\tdouble y[] = {0, 0.14, 0.18, 0.22, 0.25, 0.29, 0.32, 0.35, 0.39, 0.37, 0.58, 0.73, 0.96, 1.34, 2.10, 4.39}; // y[0] is 0 to padd the array such that y[i] 'starts' at index of 1\n\tint u,v,w;\n\tfor (int i = 1; i <=15; ++i)\n\t{\n\t\tu= i;\n\t\tv= 16-i;\n\t\tif (u < v)\n\t\t\tw = u;\n\t\telse\n\t\t\tw = v;\n\t\tenergy -= pow( y[i] - x1 - u/ (x2*v + x3*w) , 2);\n\t}"
            self.outputDir = 'Benchmark_Bard'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Bard')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Beale Function
        elif func_no ==5:
            self.add_par('x_1',-4.5,4.5)
            self.add_par('x_2',-4.5,4.5)
            self.benchmark_energy = "\t// Beale Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy -= pow ( (1.5 - x1 + x1*x2) , 2) + pow(2.25 - x1 + x1*x2*x2 ,2) + pow( 2.625 -x1 + x1 *x2*x2*x2,2);"
            self.outputDir = 'Benchmark_Beale'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Beale')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Bird Function
        elif func_no ==6:
            self.add_par('x_1', -2*np.pi, 2*np.pi)
            self.add_par('x_2', -2*np.pi, 2*np.pi)
            self.benchmark_energy = "\t// Bird Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy -= sin(x1) * exp( pow(1- cos(x2), 2)) + cos(x2) * exp( pow(1- sin(x1), 2)) + pow(x1-x2,2) + 1000;"
            self.outputDir = 'Benchmark_Bird'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Bird')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Bohachevskey Function
        elif func_no == 7:
            self.add_par('x_1', -100, 100)
            self.add_par('x_2', -100, 100)
            self.benchmark_energy = '\t// Bohachevskey Function \n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy -= x1*x1 + 2 * x2*x2 - 0.3 * cos(3*___pi___*x1 + 4 * ___pi___*x2) + 0.3;'
            self.outputDir = 'Benchmark_Bohachevskey'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Bohachevskey')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Booth Function
        elif func_no == 8:
            self.add_par('x_1', -10, 10)
            self.add_par('x_2', -10, 10)
            self.benchmark_energy = '\t// Booth Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy -= pow(x1+2*x2 - 7,2) + pow(2*x1+x2-5,2);'
            self.outputDir = 'Benchmark_Booth'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Booth')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Bukin Function
        elif func_no ==9:
            self.add_par('x_1', -15, -5)
            self.add_par('x_2', -3, 3)
            self.benchmark_energy = '\t// Bukin Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy -= 100 * pow( abs(x2-0.01*x1*x1) , 0.5) + 0.01 * abs(x1+10);'
            self.outputDir = 'Benchmark_Bukin'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Bukin')
            self.generate(options)
            self.__benchmark_preventGenerate = True            

        # Corana Function
        elif func_no == 10:
            for i in range(1,4+1):
                self.add_par('x_%i'%i, -500, 500)
            self.benchmark_energy = '\t// Corana Function\n\tconst double d[] = {1,1000,10,100};\n\tconst double A = 0.05;\n\tdouble z,v, sgn;\n\tfor (int i = 0; i < 4; ++i)\n\t{\n\t\tdouble x = par[i];\n\t\tsgn = (x > 0) - (x < 0);\n\t\t\n\t\tz = 0.2 * floor(std::abs(x/0.2) + 0.49999 ) *sgn;\n\t\tv = std::abs(x-z);\n\t\t\n\t\tif (std::abs(v) < A)\n\t\t{\t\n\t\t\tsgn = (z > 0) - (z < 0);\n\t\t\t\n\t\t\tenergy -= 0.15 * pow(z - 0.05*sgn,2) *d[i];\n\t\t\n\t\t}\n\t\telse\n\t\t\tenergy -= d[i] * x * x;\n\t}'
            self.outputDir = 'Benchmark_Corana'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Corana')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Davamaldi Function
        elif func_no == 11:
            self.add_par('x_1',0,14)
            self.add_par('x_2',0,14)
            self.benchmark_energy = '\t// Davamaldi Function\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tenergy-=(1 - pow(std::abs( sin(___pi___*(x1-2) )*sin(___pi___*(x2-2))/ (___pi___*___pi___*(x1-2)*(x2-2))  ),5)) * (2 + pow(x1-7,2)+ 2*pow(x2-7,2)  );'
            self.outputDir = 'Benchmark_Davamaldi'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Davamaldi')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Devilliers Glasser Function
        elif func_no == 12:
            for i in range(1,5+1):
                self.add_par('x_%i'%i, -500, 500)
            self.benchmark_energy = '\t// Devilliers Glasser\n\tdouble t,y;\n\tdouble e507 = exp(0.507);\n\tdouble x1 = par[0];\n\tdouble x2 = par[1];\n\tdouble x3 = par[2];\n\tdouble x4 = par[3];\n\tdouble x5 = par[4];\n\tfor (int i=0; i < 16; ++i)\n\t{\n\t\tt = 0.1*i;\n\t\t      y = 53.81 * pow(1.27,t) * tanh(3.012*t + sin(2.13*t) ) * cos(e507*t);\n\t\t\n\t\tenergy -= pow( x1*pow(x2,t)*tanh( x3*t + sin(x4*t) ) * cos( exp(x5) * t ) -y ,2);\n\t\n\t}'
            self.outputDir = 'Benchmark_DevilliersGlasser'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: DevilliersGlasser')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # EggHolder Function
        elif func_no == 13:
            if npar <= 0:
                raise NameError('EggHolder Function is scalable. Please specify a number of variables to test\n Example: 3 dimensions: aptmodel.runbenchmark(13, 3)')
            for i in range(1, npar+1):
                self.add_par('x_%i'%i, -512, 512)
            self.benchmark_energy = '\t// EggHolder Function\n\tdouble sum = 0;\n\tdouble xi,xip1;\n\tfor (int i = 0; i < ___nPars___ - 1; ++i)\n\t{\n\t\txi   = par[i];\n\t\txip1 = par[i+1];\n\t\tsum += - (xip1+47)*sin( pow(abs( xip1 + xi/2 + 47),0.5) ) - xi * sin ( pow( abs(xi - (xip1 + 47)), 0.5) );\n\t}\n\tsum += 1000; // We shift it up because we want to avoid neg numbers when converting to logspace\n\tenergy -= sum;'
            self.outputDir = 'Benchmark_EggHolder'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: EggHolder')
            self.generate(options)
            self.__benchmark_preventGenerate = True

        # Griewank Function
        elif func_no == 14:
            if npar <= 0:
                raise NameError('Griewank Function is scalable. Please specify a number of variables to test\n Example: 3 dimensions: aptmodel.runbenchmark(14, 3)')
            for i in range(1, npar+1):
                self.add_par('x_%i'%i, -100, 100)
            self.benchmark_energy = '\t// Griewank Function\n\tdouble sum1 = 0, sum2 = 1;\n\tdouble xi;\n\tfor (int i = 0; i < ___nPars___; ++i)\n\t{\n\t\txi = par[i];\n\t\tsum1 += xi * xi / 4000;\n\t\t\n\t\tsum2 = sum2 * cos(xi/pow(i+1, 0.5));\n\t}\n\tenergy -= sum1 - sum2 + 1;\n'
            self.outputDir = 'Benchmark_Griewank'
            self.__benchmark_preventGenerate = False
            print('Generating benchmark: Griewank')
            self.generate(options)
            self.__benchmark_preventGenerate = True
        else:
            raise NameError('Invalid option selected. Please use aptmodel.listbenchmarks() for a list of valid selections.')

    def __preprocess(self, options):
        # process some options here
        """
        Create a long list from the provided options. Code will later replaces a
        '|' each time it is encountered in template_options.hpp with the next object
        in this list and copy it over to options.hpp

        Also handle the uniqueness of LB, UB of parameters.
        Also handle log_e of data if not already
        Also handle creating output dir
        """
        self.__optlist = []
        self.__optlist.append(str(int(options.maxThr)))
        self.__optlist.append(str(int(options.nTemp)))
        self.__optlist.append(str(int(options.nStep)))
        self.__optlist.append(str(int(options.nEnsemble)))
        self.__optlist.append(str(int(options.nSwap)))
        self.__optlist.append(str(int(self.nPar)))
        self.__optlist.append(str(int(options.dispInterval)))
        self.__optlist.append(str(int(options.saveInterval)))
        self.__optlist.append(str(int(options.StatusBar)))
        self.__optlist.append(str(int(options.adapt_last)))
        self.__optlist.append(options.min_adapt_factor)
        self.__optlist.append(options.max_adapt_factor)
        self.__optlist.append(options.optimal_swap_accept)
        self.__optlist.append(str(int(options.adapt_beta_interval)))
        self.__optlist.append(options.adapt_beta_rate)
        self.__optlist.append(options.optimal_step_accept)
        self.__optlist.append(str(int(options.adapt_step_interval)))
        self.__optlist.append(options.adapt_step_rate)
        self.__optlist.append(options.step_size)
        self.__optlist.append(str(int(options.nInit)))
        self.__optlist.append(options.bigEnergy)
        self.__optlist.append(str(len(self.Exp)))
        if self.benchmark_flag:
            self.__optlist.append('{}')
        else:
            temp = ['{\"aptdata_']
            for i, dataname in enumerate(self.Exp.keys()):
                temp.append(dataname)
                if i == self.nExp-1:
                    temp.append('.csv\"}')
                else:
                    temp.append('.csv\",\"aptdata_')
            self.__optlist.append(''.join(temp))
        self.__optlist.append('false')
        self.__optlist.append(options.numerical_min)
        self.__optlist.append(''.join(['\"', options.filename, '\"']))
        self.__optlist.append(str(self.nStates))
        self.__optlist.append(''.join(['RCONST(', '{}'.format(options.cvode_rtol), ')']))
        self.__optlist.append(str(int(options.cvode_max_steps)))
        self.__optlist.append(''.join(['RCONST(', '{}'.format(options.cvode_atol), ')']))

        # convert any remaining numerical values in options into strings, displaying at least
        # 1 decimal point. The str() command has been done on options we've already converted to ints
        for i,val in enumerate(self.__optlist):
            if type(val) is not str:
                first = '{}'.format(val)
                second = '{:.1e}'.format(val)
                val = max((first, second), key=len)
                self.__optlist[i] = val

        # self.__optlist[27] = ''.join(['RCONST(', self.__optlist[27], ')'])
        # self.__optlist[29] = ''.join(['RCONST(', self.__optlist[29], ')'])

        # Put parameter LB and UB into nPar x 2 array
        LBUBarray = np.zeros([self.nPar,2])
        LBUBarray[:,0] = [par.LB for par in self.Parameters]
        LBUBarray[:,1] = [par.UB for par in self.Parameters]
        # sort them into unique LB,UB pairs
        tmp = np.ascontiguousarray(LBUBarray).view(np.dtype((np.void, LBUBarray.dtype.itemsize * LBUBarray.shape[1])))
        _, self.__uniqueLB_UB,self.__prior_idx = np.unique(tmp, return_index=True, return_inverse=True)
        self.__uniqueLB_UB = LBUBarray[self.__uniqueLB_UB]

        # check for uniform distribution
        self.__unif01 = any( (self.__uniqueLB_UB[:,0]==0) & (self.__uniqueLB_UB[:,1]==1)  )
        if self.__unif01:
            # if it exists, put it at the top of the unique LB UB array
            unifInd = np.where( (self.__uniqueLB_UB[:,0]==0) & (self.__uniqueLB_UB[:,1]==1) )[0]
            self.__uniqueLB_UB[ [0, unifInd],: ] = self.__uniqueLB_UB[ [unifInd, 0],: ]
            # update the indices that assign each par to its LB UB
            tmp = np.where(self.__prior_idx==unifInd)
            self.__prior_idx[np.where(self.__prior_idx==0)]=unifInd
            self.__prior_idx[tmp]=0

        # check size of bolus infusions
        for apt_data in self.Exp.values():
            if not np.any(np.isnan(apt_data.infus_bolus)):
                assert apt_data.infus_bolus.shape[1] == (self.nStates+1), "Bolus infusion in experiment %s needs (nStates+1) number of columns!"%apt_data.name 

        # Check if outputdirectory exists, if not, create it
        if not os.path.exists(self.outputDir):
            os.makedirs(self.outputDir)

    def generate(self, options=aptoptions()):
        # First, we need checks
        #   If we ARE benchmarking and safety is ON, throw error
        if self.benchmark_flag and self.__benchmark_preventGenerate:
            raise NameError('generate() is invalid if you are using aptmcmc benchmarks!')
        #   If self.Exp is NOT defined and we are NOT running a benchmark, throw error
        if (not self.Exp) and (not self.benchmark_flag):
            raise NameError('Some fitting data (APTexperiment object) must be specified and added to the model!')
        #   If self.ODE is NOT defined and we are NOT running a benchmark, throw error
        if (not self.ODE) and (not self.benchmark_flag):
            raise NameError('ODEs for your states must be specified!')
        #   If per experiment parameters do not have a par defined for each experiment, throw error
        if self.nExperimentalPars > 0:
            for par in self.dependent_pars:
                for exp in par[1].keys():
                    assert exp in self.Exp.keys(), "Experimentally dependent parameter %s is missing a dictionary entry for experiment %s! Ensure a parameter is assigned to each experiment."%(par[0],exp)
        if self.__dependent_const_flag:
            for const in self.dependent_const:
                for exp in const[1].keys():
                    assert exp in self.Exp.keys(), "Experimentally dependent constant %s is missing a dictionary entry for experiment %s! Ensure a constant is assigned to each experiment."%(const[0],exp)

        self.__preprocess(options)

        # Second, we write the make file
        currpath = os.path.join(self.outputDir, 'makefile')
        print('Writing {:s} ...'.format(currpath), end="")
        genFile = open(currpath, 'w')
        genFile.write(
            'CC=         g++\nCFLAGS=     -std=c++0x -w -fopenmp -O3\nLDFLAGS=    -lsundials_nvecserial -lsundials_cvode -ltrng4 -llapack\nLDLIBS=     ')
        genFile.write(self.lib_path)
        genFile.write(self.include_path)
        aptpath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'aptmcmc_src')
        aptpath = aptpath.replace('\\','/') # Force Unix Path slash direction, ensures cygwin compatability in Windows
        genFile.write(' -I./ -I%s\nSRCDIR=     %s'%(aptpath,aptpath) )
        if self.__infusions:
            genFile.write(
            '\nSOURCES=    $(SRCDIR)/stretch_move_helper.cpp $(SRCDIR)/CVODE_infusion_engine.cpp $(SRCDIR)/functions.cpp $(SRCDIR)/importData.cpp $(SRCDIR)/swap_helper.cpp $(SRCDIR)/acc.cpp $(SRCDIR)/save_fwrite.cpp $(SRCDIR)/main.cpp UserModel.cpp')
        else:
            genFile.write(
            '\nSOURCES=    $(SRCDIR)/stretch_move_helper.cpp $(SRCDIR)/CVODE_engine.cpp $(SRCDIR)/functions.cpp $(SRCDIR)/importData.cpp $(SRCDIR)/swap_helper.cpp $(SRCDIR)/acc.cpp $(SRCDIR)/save_fwrite.cpp $(SRCDIR)/main.cpp UserModel.cpp')
        genFile.write('\nOBJECTS=    $(SOURCES:.cpp=.o)\nEXECUTABLE= ')
        genFile.write(options.filename)
        genFile.write(
            '\n\n\nall: $(SOURCES) $(EXECUTABLE) clean_o\n\n$(EXECUTABLE):  $(OBJECTS)\n\t$(CC) $(CFLAGS) $(OBJECTS) -o $@ $(LDFLAGS) $(LDLIBS)')
        genFile.write(
            '\n\n.cpp.o:\n\t$(CC) $(CFLAGS) -c $< -o $@ $(LDFLAGS) $(LDLIBS)')
        genFile.write('\n\nclean_o:\n\trm -rf *o\n\trm -rf $(SRCDIR)/*o')
        genFile.write('\n\nclean:\n\trm -rf $(EXECUTABLE)\n\trm -rf $(SRCDIR)/*o')
        genFile.close()
        print(' done!')

        # Third, we generate data files
        # iterate over aptdata objects in self
        self.data_indices =[]
        for apt_data in self.Exp.values():
            filename = ''.join(['aptdata_', apt_data.name, '.csv'])
            # for later, when we write the options file
            currpath = os.path.join(self.outputDir, filename)
            print('Writing csv file {:s} ...'.format(currpath), end="")
            with open(currpath, 'w') as csvfile:
                # create a dict of states we have data to indices of states in
                # the model
                mydict = {}
                for datastate in self.listOfStates:
                    if datastate in apt_data.states:
                        mydict[datastate] = apt_data.states[datastate]
                    else:
                        mydict[datastate] = None
                self.data_indices.append(mydict)
                # for datastate in apt_data.states.keys():
                #     # Subtract 1 b/c python is 0-based indexing and we defined model states using 1-based
                #     data_indices.append(self.listOfStates[datastate]-1)
                # data_indices = np.sort(data_indices)
                # # now we can write the csvfile
                # # if data does not have initial conditions, put a blank line of
                # # commas
                # if apt_data.noInitCondition:
                #     for i in range(0, self.nStates-1):
                #         csvfile.write(',')
                #     csvfile.write('\n')
                # write the data spaced out according to the index mapping
                # dictionary we made earlier
                for t in range(0, apt_data.nTimes):
                    for i,state in enumerate(self.listOfStates):
                        if mydict[state] is not None:
                            if not np.isnan(apt_data.values[mydict[state], t]):
                                if self.logMode:
                                    csvfile.write(str(apt_data.values[mydict[state], t]))
                                else:
                                    csvfile.write(str(apt_data.rawvalues[mydict[state], t]))
                        if (i == self.nStates-1) & (t < apt_data.nTimes-1):
                            csvfile.write('\n')
                        elif (i != self.nStates-1):
                            csvfile.write(',')
                # done writing!
            print(' done!')

        # Fourth, we write the options files
        oldpath  = os.path.join(os.path.dirname(os.path.realpath(__file__)),'aptmcmc_src','template_options.hpp')
        currpath = os.path.join(self.outputDir,'options.hpp')  
        print('Writing {:s} ...'.format(currpath), end="")
        with open(oldpath) as old, open(currpath, 'w') as new:
            counter = 0
            for line in old:
                if '|' in line:
                    line = line.replace('|', self.__optlist[counter])
                    new.write(line)
                    counter += 1
                else:
                    new.write(line)
        print(' done!')

        # Fifth, write the prior portion of the UserModel.cpp
        def fprintf(filename, format, *args):
            # Not good practice, but this makes it easy to port my file generator from
            # matlab
            filename.write(format % args)
        
        currpath = os.path.join(self.outputDir,'UserModel.cpp')   
        print('Writing {:s}:'.format(currpath))
        with open(currpath,'w') as fidW:
            print('\tWriting prior definitions ...',end="")
            fprintf(fidW,'//=====GENERATED using Python Module APTModel=====\n')

            # Include definitions
            fprintf(fidW,'#include "options.hpp"\t\t\t\t/* User-specified MCMC Options */ \n#include "classes.hpp"\t\t\t\t/* Support for custom arrays */\n#include <vector>\t\t\t\t\t/* Support for vectors */\n#include <array>\t\t\t\t\t/* Support for arrays */\n#include <trng/yarn3.hpp>\t\t\t/* Support for TRNG''s rand generator */\n')
            if self.__infusions:
                fprintf(fidW,'#include "CVODE_infusion_engine.hpp"\t\t\t/* Definitions for the CVODE integrator with infusions */')
            else:
                fprintf(fidW,'#include "CVODE_engine.hpp"\t\t\t/* Definitions for the CVODE integrator */')
            
            fprintf(fidW,'\n#include <cmath>\t\t\t\t\t/* Include math libraries */\n#include <nvector/nvector_serial.h>\t/* Support for CVODE vectors, types, functions, macros */\n#include "functions.hpp"\t\t\t/* Include helper math functions, definition of type realtype */\n\n\n')
            
            # Comments and instructions for listing prior distributions in case user wants to manually modify them
            fprintf(fidW,'/* ========================================\n * ===LIST PARAMETER DISTRIBUTIONS HERE====\n * ========================================\n * List the types of distributions here along with appropriate parameters.\n* Consult documentation linked below for the possibilities.\n * Documentation:  http://numbercrunch.de/trng/trng.pdf\n*\n * Be sure to rename each one differently, eg par2, par 3, ...\n *\n * Example:\n *   #include <trng/uniform_dist.hpp>\n *    #include <trng/lognormal_dist.hpp> \n *     #include <trng/discrete_dist.hpp>\n *\n *   static trng::uniform_dist<>     par1(-10,10);\n *   static trng::lognormal_dist<>   par2(6,10);\n *     static trng::discrete_dist  par3(47);\n*/\n\n\n')
            fprintf(fidW,'#include <trng/uniform_dist.hpp>\n');
            
            # Define priors
            if self.__unif01:
                fprintf(fidW,'#include <trng/uniform01_dist.hpp>\n')
                fprintf(fidW,'static trng::uniform01_dist<> \t distr0;\n')
                genrange = range(1,self.__uniqueLB_UB.shape[0])
            else:
                genrange = range(0,self.__uniqueLB_UB.shape[0])
            for i in genrange:
                fprintf(fidW,'static trng::uniform_dist<> \t distr%i(%g,%g);\n', i, self.__uniqueLB_UB[i,0], self.__uniqueLB_UB[i,1])
            fprintf(fidW,'/* =========================================\n * ==END LISTING OF PARAMETER DISTRIBUTIONS=\n * =========================================\n */\n\n\n')
            fprintf(fidW,'\n\nstatic void set(double *ptr, unsigned parID, double value); \n\n// Function to initialize the ensemble based on your prior\nvoid sampleprior(double * par_ptr,  trng::yarn3 &gen)\n{\n')
            
            # Comments and instructions for setting parameters to their prior distributions
            fprintf(fidW,'\t/* ==============================================\n\t * ===========SET EACH PARAMETER PRIOR===========\n\t * ==============================================\n\t * Example:\n\t *   set(par_ptr, 0,     par1(gen) );       // Par 1: k1\n\t *   set(par_ptr, 1,     par2(gen) );       // Par 2: k2\n\t *   set(par_ptr, 2,     par3(gen) );       // Par 3: k3\n\t *\n\t * Or with a for loop if multiple parameters share same prior:\n\t *   for (int i = 0; i < ___nPars___; ++i )\n\t *       set(par_ptr,i,     par1(gen));\n\t *\n\t * DON\'T FORGET C++ USES 0-BASED INDEXING!!\n\t */\n')
            # Set params to their priors
            for i in range(0,self.nPar):
                fprintf(fidW,'\tset(par_ptr, %i, \t distr%i(gen) ); \t \t //Par %i, %s \n',i, self.__prior_idx[i], i, self.Parameters[i].name)
            fprintf(fidW,'\t/* =========================================\n\t * ======END SETTING PARAMETER PRIORS=======\n\t * =========================================\n\t*/\n')

            # Function for MCMC to sample parameters from their distributions during initialization
            fprintf(fidW,'\n}\n\n// Function to calculate the ln(prior) probability of a given parameter set\nvoid priorPDF(double &energy, double*par_ptr)\n{\n\tenergy = 0;\n\n')
            for i in range(0,self.nPar):
                if (self.__unif01) & (self.__prior_idx[i] == 0):
                    fprintf(fidW,'\tenergy += log(\t distr%i.pdf(par_ptr[%i])\t);\t\t\t\t\t\t\t //Par %i: %s\n',self.__prior_idx[i],i,i,self.Parameters[i].name)
                else:
                    fprintf(fidW,'\tenergy += log(\t distr%i.pdf(par_ptr[%i])* (distr%i.b()-distr%i.a())\t);\t //Par %i: %s\n',self.__prior_idx[i],i,self.__prior_idx[i],self.__prior_idx[i],i,self.Parameters[i].name)
            fprintf(fidW,'\n}\n\n\n');
            print(' done!')

            print('\tWriting objective function (energy) ...',end="")
            # Function definition
            fprintf(fidW,'// Function to calculate the energy of your parameter. Specifically, this calculates the\n// ln likelihood of your parameters\nvoid energy(double &energy, double *par, const std::array<   Array2D<double>, ___nExperiments___> data, const std::array<  std::vector<int>, ___nExperiments___> validInd)\n{\n')
            fprintf(fidW,'\t/* ==============================================\n\t * =====CALCULATE THE ENERGY OF YOUR PARAMS======\n\t * ==============================================\n\t * This step is open ended and you need to choose how you pose this problem.\n\t * Do you assume Gaussian noise in your data? If so, then integrate the model\n\t * with your parameters and calc a sum of least squares.\n\t * If you assume other noise, then you should calculate its ln likelihood function\n\t * appropriately.\n\t */')
            
            # Invoke prior function
            fprintf(fidW,'\n\t/* Calculate prior first */\n\tpriorPDF(energy,par);\n\tif (energy <= -___big_energy___)\n\t{\n\t\tenergy = -___big_energy___;     // Prevent energy fron taking on inf as a value because it might cause problems further down the road\n\t\treturn;\n\t}')
            fprintf(fidW,'\n\n\tint errflag=0;\t//Integration error flag\n')
            for dataNumber,data in enumerate(self.Exp.values()):
                fprintf(fidW,'\n\n\t// Calculate objective function for data: %s\n',data.name)
                # Define Y0
                fprintf(fidW,'\tconst std::vector<double>  %s_Y0   = { ',data.name)
                last = self.nStates-1
                    # # Define initial condition parameters in the C++ code
                    # for initvar in data.init_dict:
                    #     # fprintf(fidW,'\tdouble %s_%s = par[%i];\n',data.name,initvar,data.init_dict[initvar]-1)
                    #     fprintf(fidW,'\tdouble %s = par[%i];\n',initvar,self.listOfParameters[initvar]-1)
                for i,state in enumerate(self.listOfStates):
                    if type(data.initcond[state]) is str:
                        fprintf(fidW,' par[%i]',self.listOfParameters[data.initcond[state]])  
                    else:
                        fprintf(fidW, '%g', data.initcond[state])                      
                    if i == last:
                        fprintf(fidW,' };\n')
                    else:
                        fprintf(fidW,',')
                # else:
                #     for i,state in enumerate(self.listOfStates):    
                #         fprintf(fidW,' exp(data[%i](%i))',dataNumber,data_indices[dataNumber][state] )
                #         if i == last:
                #             fprintf(fidW,' };\n')
                #         else:
                #             fprintf(fidW,',')

                # Define tsim
                fprintf(fidW,'\tconst std::vector<double>  %s_tsim = { ',data.name)
                if data.tshift:
                    fprintf(fidW,'0,')
                    data.tshift_val = self.listOfParameters[data.tshift_name]
                last = data.nTimes-1 #+data.noInitCondition
                for i,t in enumerate(data.time):
                    fprintf(fidW,'%g', t)
                    if data.tshift:
                        fprintf(fidW,'+par[%i]',data.tshift_val-1)
                    if i == last:
                        fprintf(fidW,' };\n')
                    else:
                        fprintf(fidW,',')
                # Define infusions, if any
                if self.__infusions:
                    if data.tshift: # please make my life more difficult. infusions and a timeshift?
                        sim_t0 = 'par[%i]'%(data.tshift_val-1)
                        sim_tf = '%e + par[%i]'%(data.time[-1],data.tshift_val-1)
                    else:
                        sim_t0 = str(data.time[0])
                        sim_tf = str(data.time[-1])

                    if not np.all(np.isnan(data.infus_zoh)):
                        nrow = data.infus_zoh.shape[0]
                        ncol = data.infus_zoh.shape[1]
                        fprintf(fidW,'\n\tArray2D<realtype> %s_infus_zoh(%i, %i);\n',data.name,nrow,ncol)
                        for ii in range(nrow):
                            for jj in range(ncol):
                                fprintf(fidW,'\t%s_infus_zoh(%i,%i) = %e;\n',data.name,ii,jj,data.infus_zoh[ii,jj])
                    else:
                        if not np.all(np.isnan(data.infus_ramp)): # if ramp exists, make infus_zoh same width
                            ncol = data.infus_ramp.shape[1]
                        else:
                            ncol = 2
                        fprintf(fidW,'\n\tArray2D<realtype> %s_infus_zoh(2, %i);\n',data.name,ncol)
                        fprintf(fidW,'\t%s_infus_zoh(0,0) = %s;\n',data.name,sim_t0)
                        fprintf(fidW,'\t%s_infus_zoh(1,0) = %s;\n',data.name,sim_tf)

                    if not np.all(np.isnan(data.infus_ramp)):
                        nrow = data.infus_ramp.shape[0]
                        ncol = data.infus_ramp.shape[1]
                        fprintf(fidW,'\n\tArray2D<realtype> %s_infus_ramp(%i, %i);\n',data.name,nrow,ncol)
                        for ii in range(nrow):
                            for jj in range(ncol):
                                fprintf(fidW,'\t%s_infus_ramp(%i,%i) = %e;\n',data.name,ii,jj,data.infus_ramp[ii,jj])
                    else:
                        if not np.all(np.isnan(data.infus_zoh)): # if zoh exists, make infus_ramp same width
                            ncol = data.infus_zoh.shape[1]
                        else:
                            ncol = 2
                        fprintf(fidW,'\n\tArray2D<realtype> %s_infus_ramp(2, %i);\n',data.name,ncol)
                        fprintf(fidW,'\t%s_infus_ramp(0,0) = %s;\n',data.name,sim_t0)
                        fprintf(fidW,'\t%s_infus_ramp(1,0) = %s;\n',data.name,sim_tf)

                    if not np.all(np.isnan(data.infus_bolus)):
                        nrow = data.infus_bolus.shape[0]
                        ncol = data.infus_bolus.shape[1]
                        fprintf(fidW,'\n\tArray2D<realtype> %s_infus_bolus(%i, %i);\n',data.name,nrow,ncol)
                        for ii in range(nrow):
                            for jj in range(ncol):
                                fprintf(fidW,'\t%s_infus_bolus(%i,%i) = %e;\n',data.name,ii,jj,data.infus_bolus[ii,jj])
                    else:
                        fprintf(fidW,'\n\tArray2D<realtype> %s_infus_bolus(1, %i);\n',data.name,self.nStates+1)
                        fprintf(fidW,'\t%s_infus_bolus(0,0) = %s;\n',data.name,sim_t0)

                # Define weights matrix, if it exists
                if not np.all(data.weights==1):
                    fprintf(fidW,'\tconst std::vector<double>  %s_weights = { ',data.name)  
                    weight = data.weights.flatten()
                    weight_len = len(weight)
                    for wt_id,wtval in enumerate(weight):
                        fprintf(fidW,'%g',wtval)
                        if wt_id != weight_len-1:
                            fprintf(fidW,',')
                    fprintf(fidW,'};')

                # Define results matrix
                fprintf(fidW,'\n\tArray2D<realtype> %s_model(%s_tsim.size(), %s_Y0.size());\n',data.name,data.name,data.name)

                # Integrate
                if self.__infusions:
                    fprintf(fidW,'\terrflag = integrate(%i, par, &%s_model, %s_Y0, %s_tsim, %s_infus_ramp, %s_infus_zoh, %s_infus_bolus);',dataNumber,data.name,data.name,data.name,data.name,data.name,data.name)
                else:
                    fprintf(fidW,'\terrflag = integrate(%i, par, %s_model, %s_Y0, %s_tsim);',dataNumber,data.name,data.name,data.name)

                # Calculate SSE
                # Add 1 to the simulation calculations, %s_model(e) to maintain consistency with
                # the data and to prevent large negative values when log().
                # Also add 1 to the SSE before squaring to prevent squaring low numbers. 
                fprintf(fidW,'\n\tif (errflag)\t //some cvode integration failure encountered\n\t{\n\t\tenergy = -___big_energy___;\n\t\treturn;\n\t}\n\telse\n\t{\n\t\t/* Calculate the ln likelihood */')
                if self.logMode:
                    fprintf(fidW,'\n\t\tfor (auto e: validInd[%i])      // For loop over valid indicies only\n\t\t{\n\t\t\t%s_model(e) = log(1.0+%s_model(e));\n\t\t\t',dataNumber,data.name,data.name)
                else:
                    fprintf(fidW,'\n\t\tfor (auto e: validInd[%i])      // For loop over valid indicies only\n\t\t{\n\t\t\t',dataNumber)
                #     Check if any weights are present (any non-unity weights in the experiment).
                if np.all(data.weights==1):
                    fprintf(fidW,'energy -= pow(%s_model(e)- data[%i](e), 2)  ;   // Add sum of squares',data.name,dataNumber)
                else:
                    fprintf(fidW,'energy -= pow(%s_model(e)- data[%i](e), 2) * %s_weights[e]  ;   // Add sum of squares',data.name,dataNumber,data.name)
                fprintf(fidW,'\n\t\t}\n\t}')

            # Write benchmark code if we are not integrating.
            if self.benchmark_flag:
                fprintf(fidW,'\n\n\t// Calculate objective function for benchmark function:\n')
                fprintf(fidW,self.benchmark_energy)

            # Finish up objective function
            fprintf(fidW,'\n\t/* ==============================================\n\t * ===END CALCULATE THE ENERGY OF YOUR PARAMS====\n\t * ==============================================\n\t */\n}\n\n')
            print(' done!')

            # Write rhs for CVODE
            print('\tWriting CVODE RHS function ...',end="")
            fprintf(fidW,'/* ========================================\n * ==DEFINE PARAMETERS &  STATE MACROS=====\n * ========================================\n * Warning: These are macros to be defined for your convenience. This makes\n * your life easier later on when you define your CVODE equations. Don\'t be\n * silly and use C++ reserved names (e.g. don\'t redefine int or double). If\n * want to be cautious, use ___ as a prefix before and after your MACRO\n * definition names. \n */\n\n')
            for state, val in self.listOfStates.items():
                fprintf(fidW,'#define %s\t\t NV_Ith_S(y,%i)\n',state,val-1)
                fprintf(fidW,'#define %s_dot\t NV_Ith_S(ydot,%i)\n',state,val-1)
            fprintf(fidW,'\n//Define parameters\n#define exp_no params[0]\n')
            for i,par in enumerate(self.Parameters):
                if (':' in par.name) or (' ' in par.name) or ('-' in par.name):
                    raise NameError('Parameter name %s is invalid.'%par.name)
                else:
                    fprintf(fidW,'#define %s params[%i]\n',par.name, i+1)  # i+1 because we want params[0] to be experiment number
            if self.__infusions:
                for infusno in self.listOfInfusions.keys():
                    infus = self.listOfInfusions[infusno]
                    if (':' in infus) or (' ' in infus) or ('-' in infus):
                        raise NameError('Infusion name %s is invalid'%infus)
                    else:
                        fprintf(fidW,'#define %s u[%i]\n',infus, infusno+1)
            fprintf(fidW,'/* ========================================\n * =END DEFINE PARAMETERS &  STATE MACROS==\n * ========================================\n */\n\n')
            fprintf(fidW,'// Compute function rhs(t,y), a dy/dt slope function that is invoked by CVODE\nint rhs(realtype t, N_Vector y, N_Vector ydot, ')
            if self.__infusions:
                fprintf(fidW,'realtype* params, realtype* u)\n{\n')
            else:
                fprintf(fidW,'void *user_data)\n{\n\tdouble* params = (double* ) user_data;    // Cast null pointer into a concrete type\n\n')

            if self.nExperimentalPars > 0 or self.__dependent_const_flag:
                if self.nExperimentalPars > 0:
                    for exp_parname, mydict in self.dependent_pars:
                        fprintf(fidW,'\n\trealtype %s;',exp_parname)
                if self.__dependent_const_flag:
                    for const_parname, mydict in self.dependent_const:
                        fprintf(fidW,'\n\trealtype %s;',const_parname)
                fprintf(fidW,'\n\tswitch( (int) exp_no)\n\t{\n\t')
                for ii in range(self.nExp):
                    fprintf(fidW,'\tcase %i:\n\t\t{\n\t\t',ii)
                    Expname = list(self.Exp.items())[ii][0]
                    if self.nExperimentalPars > 0:                        
                        for exp_parname, mydict in self.dependent_pars:
                            fprintf(fidW,'\t%s = params[%i];\n\t\t',exp_parname, self.listOfParameters[mydict[Expname]]+1) # +1 because params[0] is exp_no
                    if self.__dependent_const_flag:
                        for const_parname, mydict in self.dependent_const:
                            fprintf(fidW,'\t%s = %f;\n\t\t',const_parname, mydict[Expname]) 
                    fprintf(fidW,'\tbreak;\n\t\t}\n\t')
                fprintf(fidW,'}\n\n')

            fprintf(fidW,'\t/* ==============================================\n\t * =============DEFINE ODES HERE=================\n\t * ==============================================\n\t * Based on your macro definitions earlier, you can use them to "pretty" up your ODEs\n\t */\n\n')
            # for constants in model.ODE['Constants'].helper:
            #     fprintf(fidW,'\t%s\n',constants)
            for ode in self.ODE.values():
                fprintf(fidW,'\t//ODE for state %s\n',ode.name)
                for helper in ode.helper:
                    fprintf(fidW,'\t%s\n',helper)
                fprintf(fidW,'\t%s\n\n',ode.equation)
            fprintf(fidW,'\t/* ==============================================\n\t * ==========END ODE DEFINITIONS HERE============\n\t * ==============================================\n\t */\n\n\treturn(0);\n}\n\nstatic void set(double *ptr, unsigned parID, double value)\n{\n\t*(ptr+parID) = value;\n}')
            print(' done!')

            # Write Python plotting code w/ Assimulo
        currpath = os.path.join(self.outputDir,'rhs_%s.py'%options.filename)   
        print('Writing {:s}...'.format(currpath),end='')
        with open(currpath,'w') as fidW:
            fprintf(fidW,"#APT-MCMC Generated Code to Plot Simulation Results\n\nimport numpy as np\n\n")
            fprintf(fidW,"def hill(a,b):\n\treturn a/(a+b)\n\ndef hilln(a,n,b):\n\treturn np.power(a,n)/(np.power(a,n)+np.power(b,n))\n\ndef pow(a,b):\n\treturn a**b\n\n")

            fprintf(fidW,"parnames = [")
            for ii,par in enumerate(self.Parameters):
                fprintf(fidW,"'%s'",par.name)
                if ii != self.nPar-1:
                    fprintf(fidW,",")
            fprintf(fidW,"]\n\n")

            fprintf(fidW,"def rhs(t,y,par):\n\t")
            for ii,state in enumerate(self.listOfStates.keys()):
                fprintf(fidW,"%s",state)
                if ii != self.nStates-1:
                    fprintf(fidW,",")
            fprintf(fidW," = y\n\n\t")

            for ii, par in enumerate(self.Parameters):
                fprintf(fidW,"%s",par.name)
                if ii != self.nPar -1:
                    fprintf(fidW,",")
            fprintf(fidW," = par\n\n\t")

            tmpstr = ''
            for element in self.ODE:
                for helper in self.ODE[element].helper:
                    fprintf(fidW,"%s\n\t",helper.replace("realtype ", "").replace(";",""))
                if self.ODE[element].equation is '':
                    continue
                fprintf(fidW,"%s\n\t",self.ODE[element].equation[0:-1])
                tmpstr += self.ODE[element].name + "_dot,"

            fprintf(fidW,"\n\n\treturn np.array([%s])\n\n",tmpstr[0:-1])
            
            # fprintf(fidW,"model = parmodel()\n")
            # fprintf(fidW,"y0 = FILLMEOUT\npar = FILLMEOUT\ntspan = FILLMEOUT\n")

            # fprintf(fidW,"\n\nt,y = model.sim(rhs,tspan, y0, par)")


            # # Attempt to compile the program for the user
            # # print('\n\n======Attempting to compile======')
            # # if os.system('make -C {:s}'.format(self.outputDir))==0:
            # #     print('Compiled successfully!')
            # # else:
            # #     print('Failed to compile. Double check your Python code and/or include/lib paths. Otherwise, file a bugrequest.')
            # # make_process = subprocess.Popen("make", stderr=subprocess.STDOUT)
            # # if make_process.wait() != 0:
            # #      print('Failed to compile. Double check your Python code and/or include/lib paths. Otherwise, file a bugrequest.')
        print(' done!')


class aptresults:
    def __init__(self):
        self.name=''

def openresults(filename,sizeofdouble=8,sizeofint=4):
    with open(filename,'rb') as f:
        out = aptresults()
        out.name        = filename
        nChains     = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        nSteps      = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        nEnsemble   = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        nSwaps      = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        nPars       = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        lastswap    = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        nInit       = int(np.frombuffer(f.read(sizeofdouble),dtype=np.double))
        adapt_beta_interval = np.frombuffer(f.read(sizeofdouble),dtype=np.double)

        energy_chain = np.frombuffer(f.read(nChains*nEnsemble*nSwaps*sizeofdouble),dtype=np.double)
        out.energy_chain = np.reshape(energy_chain,[nChains,nEnsemble,nSwaps],order='C')
        
        par_chain = np.frombuffer(f.read(nChains*nEnsemble*nSwaps*nPars*sizeofdouble),dtype=np.double)
        out.par_chain = np.reshape(par_chain,[nChains,nEnsemble,nSwaps,nPars],order='C')

        step_accept = np.frombuffer(f.read(nChains*nSwaps*sizeofint),dtype=np.int32)
        out.step_accept = np.reshape(step_accept,[nChains,nSwaps],order='C')

        swap_accept = np.frombuffer(f.read(nChains*nSwaps*sizeofint),dtype=np.int32)
        out.swap_accept = np.reshape(swap_accept,[nChains,nSwaps],order='C')

        beta_hist = np.frombuffer(f.read(nChains*nSwaps*sizeofdouble),dtype=np.double)
        out.beta_hist = np.reshape(beta_hist,[nChains,nSwaps],order='C')

        out.acor = np.frombuffer(f.read(nPars*sizeofdouble),dtype=np.double)


        out.nChains=nChains
        out.nSteps=nSteps
        out.nEnsemble=nEnsemble
        out.nSwaps=nSwaps
        out.nPars=nPars
        out.lastswap=lastswap
        out.nInit=nInit
        out.adapt_beta_interval=adapt_beta_interval

        out.flat_par = np.reshape(out.par_chain, [nChains, nEnsemble*nSwaps, nPars])
        out.flat_energy = np.reshape(out.energy_chain, [nChains, nEnsemble*nSwaps])

        out.best_energy = np.max(out.flat_energy[0,:])
        out.best_par = out.flat_par[0,np.argmax(out.flat_energy[0,:]),:]

        def plothist(figname,parnames, trupar=np.nan):
            import matplotlib.pyplot as plt
            import seaborn as sns
            sns.set(style='whitegrid')
            plt.figure(figsize=(24,14))
            nrow = np.max([1, np.floor(out.nPars/9)])
            ncol = np.ceil(out.nPars/nrow)
            for i in range(out.nPars):
                ax = plt.subplot(nrow,ncol, i+1)
                sns.distplot(out.flat_par[0,:,i])
                ax.get_yaxis().set_ticks([])
                if ~np.all(np.isnan(trupar)):
                    plt.plot([trupar[i],trupar[i]],ax.get_ylim())
                plt.title(parnames[i], fontsize=20)
            plt.tight_layout()
            plt.savefig(figname)

        def plotjoint(figname, parnames, ix=None, truval = None):
            import matplotlib.pyplot as plt
            import seaborn as sns
            sns.set(style='whitegrid')
            parnames = np.array(parnames)
            import corner
            f = plt.figure(figsize=(24,14))
            # ax = plt.subplot(1,1,1)
            if ix is None:
                corner.corner(out.flat_par[0,:,:], labels=parnames, show_titles=True, bins=100, truths = truval)
            else:
                corner.corner(out.flat_par[0,:,ix].T, labels=parnames[ix], show_titles=True, bins=100, truths = truval)

            plt.tight_layout()
            plt.savefig(figname)
            # plt.show()

        def plotchain(figname, parnames):
            import matplotlib.pyplot as plt
            import seaborn as sns
            sns.set(style='whitegrid')
            E = out.energy_chain[0,0,:]
            P = out.par_chain[0,0,:,:]


            nplot = P.shape[1] + 1
            plt.figure(figsize=(40,nplot))


            plt.subplot(nplot,1, 1)
            plt.plot(E)
            plt.ylabel('Energy')
            plt.tick_params(
                axis='both',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom='off',      # ticks along the bottom edge are off
                top='off',         # ticks along the top edge are off
                labelbottom='off') # labels along the bottom edge are off

            for i in range(1,nplot):
                plt.subplot(nplot,1,i)
                plt.plot(P[:,i-1])
                plt.tick_params(
                    axis='both',          # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom='off',      # ticks along the bottom edge are off
                    top='off',         # ticks along the top edge are off
                    labelbottom='off') # labels along the bottom edge are off
                plt.ylabel(parnames[i-1])

            plt.savefig(figname)
            plt.close()

        out.plothist = plothist
        out.plotjoint = plotjoint
        out.plotchain  = plotchain

        return out

