/* Helper code to define helper math functions. 
*  Makes entering the ODEs easier. 
*/

#include "functions.hpp"             /* Function definitions */
#include <math.h>

realtype hill ( realtype x, realtype k )   {   
    return ( x/(x + k) );   
}

realtype hilln ( realtype x, realtype n, realtype k )   {   
    return ( pow(x,n)/(pow(x,n) + pow(k,n)) );   
}

realtype heavy (realtype __z__)
{
	realtype __dp__;
	__dp__=1/(1+pow(10,-10*__z__));
	return(__dp__);
}