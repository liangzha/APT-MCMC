#include "classes.hpp"
#include "options.hpp"
#include <cstring>
#include <stdio.h>

/* Save all simulation data to Matlab-readable format */
void save_fwrite(const char *file, Array4D *par_chain, Array3D *energy_chain, Array2D<int> *step_accept, Array2D<int> *swap_accept, Array2D<double> *beta_hist, int lastswap, double* autocorr)
{	
	printf("Saving simulation results...");
	
	// Open binary file for reading
	FILE *fileBuffer = fopen(file, "w");
	
	// Save simulation options
	const double opt[] = {___nChains___, ___nSteps___, ___nWalkers___, ___nSwaps___, ___nPars___, (double) lastswap, ___max_init_steps___, ___adapt_beta_interval___};
	fwrite(opt, sizeof(double), 8, fileBuffer);	
	
	(*energy_chain).save(fileBuffer);
	(*par_chain).save(fileBuffer);
	(*step_accept).save(fileBuffer);
	(*swap_accept).save(fileBuffer);
	(*beta_hist).save(fileBuffer);
	fwrite(autocorr,sizeof(double),___nPars___,fileBuffer );
	
	fclose(fileBuffer);
	
	printf("saved to %s successfully.\n",file);
	

}


// Save functions for custom classes
void Array3D::save(void *fstream) 
{
	for (int i = 0; i < d1; ++i)
	{
		for (int j = 0; j < d2; ++j)
		{
			double *ptr = data[i][j];
			fwrite(ptr, sizeof(double)*d3, 1, (FILE*) fstream);
		}
	}

}

template <typename T>
void Array2D<T>::save(void *fstream)
{
	fwrite(pAr, sizeof(T)*nWd*nHt, 1, (FILE*) fstream);
}


void Array4D::save(void *fstream)
{
	for (int i = 0; i < d1; ++i)
		{
			for (int j = 0; j < d2; ++j)
			{
				for (int k = 0; k < d3; ++k)
				{
					double *ptr = data[i][j][k];
					fwrite(ptr, sizeof(double)*d4, 1, (FILE*) fstream);
				}
			}
		}
}