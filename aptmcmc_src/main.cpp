#include "options.hpp"				 /* Options for MCMC */
#include "classes.hpp"				 /* Support for custom Array classes */
#include <sundials/sundials_types.h> /* Definition of type realtype */
#include "main.hpp"					 /* Support for functions used throughout this code */
#include "acor.h"					 /* Support for autocorrelation function */
#include <omp.h>					 /* Support for parallelization */
#include <iostream>					 /* Support for printing stuff */
#include <stdio.h>					 /* Support for printing stuff */
#include <math.h>					 /* Support for Math */
#include <trng/uniform_dist.hpp> 	 /* Support for uniform distribution for use with generating z */
#include <trng/uniform01_dist.hpp>   /* Support for uniform distribution (0 to 1) for probability acceptances */


/* Preallocate simulation data in the heap */
static Array4D par_chain(___nChains___,___nWalkers___,___nSwaps___,___nPars___);	// parameter samnples
static Array3D energy_chain(___nChains___,___nWalkers___,___nSwaps___);				// energy samples
static Array2D<double> beta_hist(___nChains___,___nSwaps___);						// beta (1/T) history
static Array2D<int> step_accept(___nChains___,___nSwaps___);						// number of accepted steps, among all walkers per chain per swap
static Array2D<int> swap_accept(___nChains___,___nSwaps___);						// number of accepted swaps, among all walkers per chain per swap

/* Preallocate temporary data in the heap */
static Array3D par_curr(___nChains___,___nWalkers___,___nPars___);
static Array2D<double> energy_curr(___nChains___,___nWalkers___);
static double beta_curr[___nChains___];

/* Preallocate autocorrelation data in the heap */
static Array3D mean_par(___nChains___, ___nPars___, ___nSwaps___);					// Time series for each par. Mean across nWalkers; one set of time series for each par for each temp
// static double auto_corr[___nPars___];						// Autocorrelation results
// static double maxcorr = -1;													 		// Save max corr dor display purposes
static double step_curr[___nChains___];

int main() 
{

	/*=================================================================
	 *======================Initialization=============================
	 *=================================================================*/
    double runtime = omp_get_wtime(); 					/* Timing the run */
	
	double gb_used = 8/1000000000.0  * ___nChains___ * ___nWalkers___*___nSwaps___*___nPars___ + 8/1000000000.0  * ___nChains___ * ___nWalkers___*___nSwaps___ + 8/1000000000.0 *2 * ___nChains___*___nSwaps___ + 8/1000000000.0  *___nChains___ * ___nWalkers___*___nPars___ + 8/1000000000.0  * ___nChains___*___nWalkers___ + 8/1000000000.0  * ___nChains___ + 8/1000000000.0  *___nChains___ * ___nPars___*___nSwaps___ + 8/1000000000.0  * ___nChains___;
	
	printf("==========================================================================\n==========================================================================\n");
	printf("==========================Beginning APT-MCMC Run==========================\n==========================================================================\n");
	printf("  Run Name: %s\n", ___matFilename___);
	printf("  Chain Size: %i\n  Ensemble Size: %i\n  Steps Per Swap: %i\n  Swap Length: %i\n",___nChains___,___nWalkers___,___nSteps___,___nSwaps___);
	printf("  Initialized %3.2f gb RAM for simulation results.\n", gb_used);
	
	/* Setup string for filename */
	std::string basename(___matFilename___);
	std::string fileName = basename;
	/* Load data */
	std::array<  std::vector<int>, ___nExperiments___> validInd;				
	std::array<char*,___nExperiments___> fileNames ___ExperimentFiles___;
	std::array<   Array2D<double>, ___nExperiments___> data;
	for (int i = 0; i < ___nExperiments___; ++i)
	{
		printf("Loading data file %i: %s...\n", i+1 ,fileNames[i]);
		importData(data[i], validInd[i],fileNames[i]); 
	}
	
	/* Parallelization Initialization */
	const unsigned int nThreads = std::min(___MaxThreads___,___nChains___);
	const unsigned int maxThreads = std::min(omp_get_max_threads(), ___MaxThreads___);
	
	omp_set_num_threads( std::min(nThreads,maxThreads) ); 						/* Request ___MaxThreads___ threads for parallelized for loop*/
	/* Initializing random number stream */
	trng::yarn3 gen;									/* Initialize random number generator for use with parallelized random num generation */
	trng::uniform01_dist<> u;
	gen.seed( (long)runtime );
	
	/* Initialize Beta
     * Use same method as emcee's parallel tempering source 
	 */
	double tstep;
	// emcee method
	if (___nPars___ <= 100)
	{
		tstep = betaarray[___nPars___-1];
	}
	else
	{
		tstep = 1.0 + 2.0*pow(log(4.0), 0.5)/pow(___nPars___,0.5);
	}
	tstep = log(tstep);
	
	// justin's method:
	//tstep = (___nChains___-1)*log(tstep)/___nChains___; 

	// hacky way to get high temps:
	// tstep = tstep + 1;  
	printf("\n Starting Temperatures at T= ");
	for (int i = 0; i < ___nChains___; ++i) 
		printf("%g; ", exp(tstep*i));
	printf("\nInitializing ensemble locations via %i samples of static MCMC...\n", ___max_init_steps___);
	
	/* Initialize helper variables */
	double lowest_E_perChain[___nChains___]; 					// Helper vars to help calc lowest energy and its location among chain 0
	int lowest_E_walker=0;
	int lowest_E_swap=0;
	double step_rate[___nChains___], swap_rate[___nChains___];	// Helper variable to calculate step/swap acceptance percentages
	
	
	#pragma omp parallel firstprivate(gen) 		/* Begin parallelization */
	{
		/*=================================================================
		 *=============Monte Carlo Parameter Hot Start=====================
		 *=================================================================*/
		 
		const unsigned int thrID=omp_get_thread_num(); 		/* Get the current Thread ID */
		const unsigned int thrSize=omp_get_num_threads();      /* Get total number of threads opened by OpenMP*/
		unsigned int availableThreads = (maxThreads-thrSize)/___nChains___;
		gen.split(thrSize,thrID);	
		// If we have additional cores leftover, enable nested parallelization of Ensemble movement
		if ( availableThreads>0)
		{
			availableThreads++; // e.g. if we have 1 core left leftover per chain, we can split each chain's task into two. 
			omp_set_num_threads(availableThreads);
		}

		// Have a single core check for thread availability 
		// then make decision to nest parallelization
		#pragma omp single
		{	
			if (thrSize > 1)
				printf("\n Enabled parallelization with %i threads.\n", thrSize);
			if (availableThreads > 1)  
			{
				omp_set_nested(1);
				printf("  Enabled nested parallelism of %i sub-threads.\n",availableThreads);
			}
		}
		
		double best_energy;
		
		for (int i = thrID; i < ___nChains___; i+=thrSize) // Loop over temperatures (each thread gets its own chain)
		{
		
			/* Initial parameter hotstart */
			beta_curr[i] = exp(-tstep*i); // Set thread-specific beta (temperature)
			
			/* Initialize lowest Energy ever experienced vector */
			lowest_E_perChain[i] = -___big_energy___;
			
			for (int walkerID = 0; walkerID < ___nWalkers___; ++walkerID) // Loop over each walker at a temperature
			{
				// Best par and best_energy are the best par/energy experienced for a specific walker during this hot-start
				double *best_par = new double[___nPars___];
				best_energy = -___big_energy___;
			
				for (int j = 0; j < ___max_init_steps___; ++j) 	// Make a couple of initial guesses to start the algorithm. 
				{
					sampleprior(par_curr.getPar(i,walkerID), gen ); /* Sample from parameter priors using thread-specific random number generator */
					
					energy(energy_curr(i,walkerID), par_curr.getPar(i,walkerID), data, validInd);	/* Calculate the energy of the current parameters */
					
					// If energy is less than the best encountered energy, save it!
					if ( energy_curr(i, walkerID) > best_energy )
					{	
						best_energy = energy_curr(i, walkerID);
						for (int p = 0; p < ___nPars___ ; ++p)
						{
							best_par[p] = par_curr.getPar(i,walkerID)[p];
						}
					}
				}
				
				/* Save the best parameters within each chain into par_chain (for swap 0)
				 * and into par_curr to begin MCMC */
				
				// Save the best parameter set into par_curr and par_chain
				for (int p = 0; p < ___nPars___ ; ++p)
					par_curr.getPar(i,walkerID)[p] = best_par[p];
										
				delete [] best_par;
				
				// Save the best energy into energy chain for this walker
				energy_curr(i,walkerID) = best_energy;
				
				// If this energy beats the best ever experienced, save it to lowest_E_perChain
				if ( best_energy > lowest_E_perChain[i])
					lowest_E_perChain[i] = best_energy;
			}
		}  // end loop over chains
		
		step_curr[thrID] = ___gzStep___;

		/* Utilize one thread to display a message that hotstart completed.
		 */
		 # pragma omp single 
		{
			if (lowest_E_perChain[0] == -___big_energy___) 
			{
				// something must be wrong, stop simulation
				printf("\n\nEnergy function returned -inf during all initalization attempts. There must be an error in the energy function in UserModel.cpp!\n");
			 	exit (EXIT_FAILURE);
			}
			if (___dispInterval___ != 0)
			{	
				printf("\n=====Finished %i guesses for hotstart, Cumulative Runtime is %-8.3g min=====\n",___max_init_steps___,  (omp_get_wtime()-runtime) / 60);
				/* for (int w = 0; w < ___nWalkers___; ++w)
					for (int i = 0; i < ___nPars___; ++i)
					{
						printf("Par %i = %f\n", i+1, par_curr.getPar(0,w)[i]);
					} */
			
			}
		}		// barrier here, implicit from using omp single
		/*=================================================================
		 *========================Begin MCMC===============================
		 *=================================================================*/
	
		for (int swapID = 0; swapID < ___nSwaps___; ++swapID)
		{  	/* Begin MCMC simulation. Take nSteps before each swap attempt. Repeat until nSwaps are performed */
			
			
			for (int i = thrID; i < ___nChains___; i+=thrSize) // Loop over temperatures (each thread gets its own chain(s) to work on )
			{
				/* Save the mean of each par across all walkers in the previous chain to help out with autocorrelation calculations */
				for (int p = 0; p < ___nPars___; ++p)
				{	for (int walkerID = 0; walkerID < ___nWalkers___; ++walkerID)
						mean_par(i, p, swapID) += par_curr(i,walkerID,p);
					mean_par(i, p, swapID) /= ___nWalkers___;
				}
					
				/* ================================================================
				 * ==================Stretch Move Step ============================
				 *=================================================================
				 * Split this into two parallelizable halves to satisfy detailed balance according to Foreman-Mackey, et al 2013
				 */
				for (int nstep = 0; nstep < ___nSteps___; ++nstep)
				{
					std::vector<double> zs(___nWalkers___);					// Preallocate vector of random numbers for use with stretch moves
					g_PDF(zs,step_curr[thrID],gen); 							// Generate vector of random numbers for use with stretch moves
					std::vector<int> S(___nWalkers___);
					sample_ensemble(S,gen); 	// generate these random numbers before nested-parallel loops	
					double r[___nWalkers___];
					for (int index=0;index<___nWalkers___;++index)
					{
						r[index] = u(gen);
					}
					
					
					#pragma omp parallel  // Nested parallel to move walkers
					{			
						#pragma omp for schedule(guided) 
						for (int walkerID=0; walkerID <___nWalkers___; walkerID+=2)
						{
							// Split walkers into ODDS and EVEN (dictated by j)
							//  j dictates the walker set to update
							// !j dictates the walker set to sample from to initiate walks
							// double Xk, Xj, r, q, prop_energy, prop_par[___nPars___],zs;			// Initialize private variables for each nested thread
							double Xk, Xj, q, prop_energy, prop_par[___nPars___];			// Initialize private variables for each nested thread
							
							// Pick a walker from the !j set
							// int S = sample_ensemble(___nWalkers___/2,___nWalkers___, gen);					// Sample from the other ensemble and get its index
							//g_PDF(zs,___gzStep___,gen);
							// int S = S1[walkerID] + ___nWalkers___/2;
							// sample_ensemble(S,___nWalkers___/2, gen);
							
							// Parameter proposal generation
							for (int p = 0; p < ___nPars___; ++p)
							{
								Xj = par_curr(i,S[walkerID],p);			// Chosen walker from !j walker set
								Xk = par_curr(i,walkerID,p);	// Current walker to move
								prop_par[p] = Xj + zs[walkerID]*( Xk - Xj);				
								// prop_par[p] = Xj + zs*( Xk - Xj);				
							}
							
							// Calculate energy of proposed parameter
							energy(prop_energy, prop_par, data, validInd);
							if (prop_energy == -___big_energy___)
							{
								continue;
							}
							
							// Generate probabilities of acceptance
							q = exp( ___nPars___*log(zs[walkerID]) + beta_curr[i] * ( prop_energy - energy_curr(i,walkerID) )  ); 
							
							// printf("oldE: %4.3f and proposedE: %4.3f at T= %4.3f q = %f and r = %f for par %f %f %f. \n",energy_curr(i,walkerID), prop_energy,1/beta_curr[i],q,r, prop_par[0],prop_par[1],prop_par[2]);
							
							// Accept Proposed Parameter State
							if (q > r[walkerID] )  		// If true, update parameters
							{	
								for (int p = 0; p < ___nPars___; ++p)
									par_curr(i,walkerID,p) = prop_par[p];	// accept proposed param by saving it into par_curr
								energy_curr(i,walkerID) = prop_energy;
								#pragma omp atomic update
								++step_accept(i, swapID);	
							} 
							//printf("walkerID %i, zs = %f\n", walkerID, zs[walkerID]);
						} // End moving half of walker ensemble
						
						#pragma omp barrier
						
						#pragma omp for schedule(guided)    // SECOND HALF
						for (int walkerID=1; walkerID <___nWalkers___; walkerID+=2)
						{
							// double Xk, Xj, r, q, prop_energy, prop_par[___nPars___],zs;			// Initialize private variables for each nested thread
							double Xk, Xj, q, prop_energy, prop_par[___nPars___];			// Initialize private variables for each nested thread
							// int S = sample_ensemble(0,___nWalkers___/2, gen);					// Sample from the other ensemble and get its index
							// g_PDF(zs,___gzStep___,gen);
							// sample_ensemble(S,___nWalkers___/2, gen);
							
							// Parameter proposal generation
							for (int p = 0; p < ___nPars___; ++p)
							{
								Xj = par_curr(i,S[walkerID],p);
								Xk = par_curr(i,walkerID,p);
								// prop_par[p] = Xj + zs*( Xk - Xj);				
								prop_par[p] = Xj + zs[walkerID]*( Xk - Xj);				
							}
							
							// Energy of proposed parameter
							energy(prop_energy, prop_par, data, validInd);
							if (prop_energy == -___big_energy___)
							{
								continue;
							}
							
							// Step acceptance criteria
							q = exp( ___nPars___*log(zs[walkerID]) + beta_curr[i] * ( prop_energy - energy_curr(i,walkerID) )  ); 
							if (q > r[walkerID] )  		// If true, accept proposed param
							{	
								for (int p = 0; p < ___nPars___; ++p)
									par_curr(i,walkerID,p) = prop_par[p];
								energy_curr(i,walkerID) = prop_energy;
								#pragma omp atomic update
								++step_accept(i, swapID);
							} // End acceptance
						} // End moving other half of walker ensemble
						
					} // End nested-parallel walker strech moves 
					
					
					// Sync threads here after stretch moves to proceed onto next step or swap attempt
					#pragma omp barrier						
					
				}	// End taking nSteps
			}	// End thread/temperature specific movement (End looping over nChains)
			 
			 /* Master thread handles the following sequentially:
			  *  a Attempt swaps from highest to lowest temperature chains
			  *  b Save all temporary *_curr variables to their respective heap space
			  *  c Display results
			  *  d Adapt beta
			  *  e Partial saves
			  */			  
			#pragma omp master
			{	
				/* ================================================================
				 * =====================Swap attempt===============================
				 * ================================================================ 
				 */
				for (int chain = ___nChains___-1; chain > 0; --chain)
				{
					// Initialize premutation arrays
					int *shuffA = new int[___nWalkers___];
					int *shuffB = new int[___nWalkers___];
					for (int ii = 0; ii < ___nWalkers___; ++ii)
					{
						shuffA[ii] = ii;
						shuffB[ii] = ii;
					}
					
					// Shuffle the permutation arrays to dictate swap attempt order between walkers
					shuffle(shuffA, ___nWalkers___, gen);
					shuffle(shuffB, ___nWalkers___, gen);
					
					double dbeta = beta_curr[chain-1] - beta_curr[chain]; 
					double q, r;
					
					// Begin swap attempt for each walker between shuffA[walkerID] and shuffB[walkerID]
					for (int walkerID = 0; walkerID < ___nWalkers___; ++walkerID)
					{
						// Attempt swap between shuffA[walkerID] at curr temp and shuffB[walkerID] at lower temp
						q = dbeta * (  energy_curr(chain,shuffA[walkerID])-energy_curr(chain-1,shuffB[walkerID])  );
						r = log(u(gen));
						
						// ++swap_accept(chain, swapID); // debug only
						
						if (q > r) 	/* Accept the swap! */
						{
							/* Perform the swap */
							for (int p = 0; p < ___nPars___; ++p)
							{	
								double par_temp = par_curr(chain,shuffA[walkerID],p);
								par_curr(chain,shuffA[walkerID],p) = par_curr(chain-1, shuffB[walkerID],p);
								par_curr(chain-1, shuffB[walkerID],p) = par_temp;
							}
							double nrg_temp = energy_curr(chain,shuffA[walkerID]);
							energy_curr(chain,shuffA[walkerID]) = energy_curr(chain-1,shuffB[walkerID]);
							energy_curr(chain-1,shuffB[walkerID]) = nrg_temp;
							
							++swap_accept(chain, swapID);
						}
					}
					
					// Clear temp par from memory; must use delete whenever allocating memory with new
					delete [] shuffA;
					delete [] shuffB;
					

				} // End loop for swap attempts
				
				/* ================================================================
				 * =====================SAVE TEMP VARIABLES========================
				 * ================================================================ 
				 */
				/* Begin saving from curr to big chain variables*/
				double *currBestE = new double[___nChains___];
				for (int chainID = 0; chainID < ___nChains___; ++chainID)
				{
					// Save beta into beta_hist
					beta_hist(chainID,swapID) = beta_curr[chainID];
					
					/* Calculate swap and step acceptance rates */
					//step_rate[chainID] = calc_rate(swapID, step_accept.getChain(chainID)) / (___nSteps___ * ___nWalkers___);
					//swap_rate[chainID] = calc_rate(swapID, swap_accept.getChain(chainID)) / ___nWalkers___; 			// swap_rate[0] is empty and not used
					step_rate[chainID] = (double) step_accept(chainID,swapID) / (___nSteps___ * ___nWalkers___);
					swap_rate[chainID] =(double)  swap_accept(chainID,swapID) / (___nWalkers___);
					
					currBestE[chainID] = -___big_energy___;
					for (int walkerID = 0; walkerID < ___nWalkers___; ++walkerID)
					{
						// Save energy into energy_chain
						energy_chain(chainID,walkerID, swapID) = energy_curr(chainID, walkerID);
						
						// Find best lowest energy in each walker
						if (energy_curr(chainID, walkerID) > currBestE[chainID])
						{	
							currBestE[chainID] = energy_curr(chainID, walkerID);  // save energy of walker with lowest energy
							
							// Is the lowest walker's energy lower than the minimum energy experienced so far?
							if (currBestE[chainID] > lowest_E_perChain[chainID])
							{	
								lowest_E_perChain[chainID] = currBestE[chainID]; 		// if this energy is global min, save it
								if (chainID == 0) // save its indicies if it is chain 0				
								{	
									lowest_E_walker = walkerID;
									lowest_E_swap = swapID;
								}			
							}
						}
							
						// Save parameters into par_chain
						for (int p = 0; p < ___nPars___; ++p) { par_chain(chainID, walkerID, swapID, p) = par_curr(chainID,walkerID,p);	}	
					}
					
				}
				
				/* ================================================================
				 * =====================DISP RESULTS===============================
				 * ================================================================ 
				 */
				if ( (swapID > 0) && ( swapID % ___dispInterval___ == 0 || swapID == ___nSwaps___-1  ) ) 	// If we are on a display interval or if we're at last swap, print some info
				{	
					printf("                                                                                                              ");  // clear the status bar line for cleanliness
					printf("\n=========================================================================\n");
					printf("|        Reached swap %i of %i, Cumulative Runtime is %-7.3g min      |\n",swapID,___nSwaps___,  (omp_get_wtime()-runtime) / 60);
					for (int chainID = 0; chainID < ___nChains___; ++chainID)
						printf("|   Chain %i: T=%6.3f   Accept=%2.2g%%   Highln(E):%-8.2e   Currln(E)=%-8.2e|\n",chainID, 1/beta_curr[chainID],step_rate[chainID]*100,lowest_E_perChain[chainID],currBestE[chainID] );
					for (int chainID = 1; chainID < ___nChains___; ++chainID)
						printf("|          Swap rates %i <-> %i = %-3.2f%%                                        |\n",chainID-1, chainID, swap_rate[chainID]*100);
					// if (maxcorr > 0)
						// printf("|          Longest Autocorrelation (over all Temps): %-3.2f                |\n", maxcorr);
					// for (int chainID = 0; chainID < ___nChains___; ++chainID)
					// {	
						// printf("|  stepaccept %i = %f   \n|", chainID, calc_rate(swapID, step_accept.getChain(chainID)));
						// printf("|  swapaccept %i = %f   \n|", chainID, calc_rate(swapID, swap_accept.getChain(chainID)));
					// }
					printf("==========================================================================\n\n");
					
					printf("Best par so far:\n[");
					for (int p = 0; p < ___nPars___; ++p)
					{
					// printf("par(%i) = %4.2e \n", p+1, par_chain(0,lowest_E_walker,lowest_E_swap,p));
					printf("%4.5e ,\n", par_chain(0,lowest_E_walker,lowest_E_swap,p));
					}
					printf("];\n");
					printf("Associated Energy: %.2f\n", energy_chain(0,lowest_E_walker,lowest_E_swap));
				}
				
				delete[] currBestE;
				
				/* ================================================================
				 * =====================ADAPT BETA=================================
				 * ================================================================ 
				 */
				if (swapID > 0 && swapID <= ___adapt_last___	&&  swapID % ___adapt_beta_interval___ == 0)
				{	
					adapt_beta(swap_rate, beta_curr);
					if (___dispInterval___ != 0)
					{
						printf(" Beta adapted!\n");
						for (int temp =1; temp < ___nChains___; ++temp)
							printf("  T%i changed from %-6.3f -> %-6.3f\n", temp+1, 1/beta_hist(temp,swapID),1/beta_curr[temp]);
					}
					
				}
				
				/* ================================================================
				 * =====================ADAPT STEP SIZE============================
				 * ================================================================ 
				 */
				if (swapID > 0 && swapID <= ___adapt_last___	&&  swapID % ___adapt_step_interval___ == 0)
				{	
					double oldstep[___nChains___];
					for (int temp =0; temp < ___nChains___; ++temp)
						oldstep[temp] = step_curr[temp];
					
					adapt_step(step_rate, step_curr);
					if (___dispInterval___ != 0)
					{
						printf(" Step Size adapted!\n");
						for (int temp =0; temp < ___nChains___; ++temp)
							printf("  Chain%i Step changed from %-2.2f -> %-2.2f\n", temp, oldstep[temp],step_curr[temp]);
					}
					
				}
				
				/* ================================================================
				 * =====================PARTIAL RUN SAVING=========================
				 * ================================================================ 
				 */
				if ( swapID % (int) ___saveInterval___ == 0 && swapID < ___nSwaps___ && swapID > 0) 	// If it hit the save interval
				{
				
					/* Calc autocorrelation for temperature 1 */
					printf("\n\n");
					double acor_mean, acor_sigma, acor_tau;
					double auto_corr[___nPars___];
					for (int p = 0; p < ___nPars___; ++p)
					{	
						int acor_status = acor(&acor_mean, &acor_sigma, &acor_tau, mean_par.getPar(0, p), swapID);
						if (acor_status == 0) auto_corr[p] = acor_tau;		// Save tau if acor was successfully calculated
						else auto_corr[p] = -1.0;
					}
				
					// Attempt to remove previous partial file
					if (fileName.compare(basename) != 0)
//					printf("Removing file %s\n",fileName.c_str());
						remove(fileName.c_str());
					
					// Clear string and save as new filename
					fileName.clear();
					fileName = basename;
										
					fileName.append("_partial");
					fileName.append(std::to_string(swapID));
					fileName.append(".mcmc");
					
					// Save
					printf("Partial Save: ");
					save_fwrite(fileName.c_str(), &par_chain, &energy_chain, &step_accept, &swap_accept, &beta_hist, swapID, auto_corr);
				}


				/* Display animated status bar */
				if (___statusBar___)
				{
					double progress = (swapID % ___dispInterval___) / static_cast <double> (___dispInterval___) ; // progress, in percent (denom is cast as double to prev. integer division)
					int pos = progress * 20; // 42 is bar width
					std::cout << "Time until next display interval [";
					for (int i = 0; i < 20; ++i)
					{
						if (i < pos) std::cout << "=";
						else if (i == pos) std::cout<< ">";
						else std::cout << " ";
					}
					std::cout << "] " << int(progress* 100.0) << " %\r";
					std::cout.flush();
				}
			}	// End master thread calculation of swaps and displaying info
			
			// Make all other threads wait for master thread to finish
			#pragma omp barrier
			
		} 	// End parallelization of moving nChains
	} 	// End swaps and simulation
	
	printf("                                                                                                               \n");  // clear the status bar line for cleanliness
		
	// /* Calc autocorrelation for temperature 1 */
	// printf("\n\n");
	// double acor_mean, acor_sigma, acor_tau;
	// double auto_corr[___nPars___];
	// for (int p = 0; p < ___nPars___; ++p)
	// {	
	// 	int acor_status = acor(&acor_mean, &acor_sigma, &acor_tau, mean_par.getPar(0, p), ___nSwaps___);
	// 	if (acor_status == 0) auto_corr[p] = acor_tau;		// Save tau if acor was successfully calculated
	// 	else auto_corr[p] = -1.0;
	// 	printf("  Param %i Autocorrelation = %f\n",p+1, acor_tau);
	// }
	// printf("\n");

	// /* Calc variace via Welford method */
	// Array2D<double> M(___nChains___+1, ___nPars___);
	// Array2D<double> S(___nChains___+1, ___nPars___);
	// Array2D<double> variance(___nChains___+1, ___nPars___);
	// double PSRF[___nPars___];
	
	// for (int p = 0; p < ___nPars___; ++p)
	// {
	// 	unsigned int k_overall = 1;
	// 	double avg_within_chain_variance;
	// 	for (int chain = 0; chain < ___nChains___; ++chain)
	// 	{
	// 		unsigned int k = 1;
	// 		for (int walk = 0; walk < ___nWalkers___; ++walk)
	// 		{
	// 			for (int swap = 0; swap < ___nSwaps___; ++swap)
	// 			{
	// 				double M_old = M(chain,p);
	// 				M(chain,p) += (par_chain(chain,walk,swap,p)- M(chain,p))/k;
	// 				S(chain,p) += (par_chain(chain,walk,swap,p)- M(chain,p))*(par_chain(chain,walk,swap,p)-M_old);
	// 				k++;

	// 				M_old = M(___nChains___,p);
	// 				M(___nChains___,p) += (par_chain(chain,walk,swap,p)- M(___nChains___,p))/k_overall;
	// 				S(___nChains___,p) += (par_chain(chain,walk,swap,p)- M(___nChains___,p)) * (par_chain(chain,walk,swap,p)- M_old);
	// 				k_overall++;
	// 			}
				
	// 		}

	// 		variance(chain,p) = S(chain,p)/(k-1);	
	// 		avg_within_chain_variance += variance(chain,p);
	// 	}
	// 	variance(___nChains___,p) = S(___nChains___,p)/(k_overall-1);
	// 	avg_within_chain_variance = avg_within_chain_variance/ ___nChains___;
	// 	PSRF[p] = pow(variance(___nChains___,p)/avg_within_chain_variance, 0.5);
	// }

	/* Calc autocorrelation and PSRF*/
	printf("\n\n");
	double acor_mean, acor_sigma, acor_tau;
	double auto_corr[___nPars___];
	Array2D<double> M(___nChains___+1, ___nPars___);
	Array2D<double> S(___nChains___+1, ___nPars___);
	Array2D<double> variance(___nChains___+1, ___nPars___);
	double PSRF[___nPars___];
	for (int p = 0; p < ___nPars___; ++p)
	{	
		/* Calc autocorrelation for Temperature Chain 0 (T=0) */
		int acor_status = acor(&acor_mean, &acor_sigma, &acor_tau, mean_par.getPar(0, p), ___nSwaps___);
		if (acor_status == 0) auto_corr[p] = acor_tau;		// Save tau if acor was successfully calculated
		else auto_corr[p] = -1.0;

		/* Calculate Potential Scale Reduction Factor */
		unsigned int k_overall = 0;
		double avg_within_chain_variance=0;
		for (int chain = 0; chain < ___nChains___; ++chain)
		{
			unsigned int k = 0;
			for (int walk = 0; walk < ___nWalkers___; ++walk)
			{
				for (int swap = 0; swap < ___nSwaps___; ++swap)
				{
					k++;
					double M_old = M(chain,p);
					M(chain,p) += (par_chain(chain,walk,swap,p)- M(chain,p))/k;
					S(chain,p) += (par_chain(chain,walk,swap,p)- M(chain,p))*(par_chain(chain,walk,swap,p)-M_old);

					k_overall++;
					M_old = M(___nChains___,p);
					M(___nChains___,p) += (par_chain(chain,walk,swap,p)- M(___nChains___,p))/k_overall;
					S(___nChains___,p) += (par_chain(chain,walk,swap,p)- M(___nChains___,p)) * (par_chain(chain,walk,swap,p)- M_old);
				}
				
			}

			variance(chain,p) = S(chain,p)/(k-1);	
			avg_within_chain_variance += variance(chain,p);
		}
		variance(___nChains___,p) = S(___nChains___,p)/(k_overall-1);
		avg_within_chain_variance = avg_within_chain_variance/ ___nChains___;
		PSRF[p] = pow(variance(___nChains___,p)/avg_within_chain_variance, 0.5);

		printf("  Param %i\tAutocorrelation = %2.2f\tPSRF = %2.2f\n",p+1, acor_tau,PSRF[p]);
	}
	printf("\n");

	/* Calc variace via Welford method */
	
	
	for (int p = 0; p < ___nPars___; ++p)
	{
	
	}


		
	/* Save the results of the simulation */
	// Attempt to remove previous partial file 
	if (fileName.compare(basename) !=0)
	{
//		printf("Removing file %s\n", fileName.c_str());
		remove(fileName.c_str());
	}
	basename.append(".mcmc");
	save_fwrite(basename.c_str(), &par_chain, &energy_chain, &step_accept, &swap_accept, &beta_hist, ___nSwaps___, auto_corr);

	
	printf("MCMC Simulation finished. I took %f min.\n", (omp_get_wtime()-runtime) / 60);
	return 0;
}
