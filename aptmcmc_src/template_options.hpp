#ifndef ___OPTIONS___ // include header guard
#define ___OPTIONS___

/*=================================================================================================*
 *======================================OPTIONS BEGIN HERE=========================================*
 *=================================================================================================*/
#define ___MaxThreads___ 							|	// Maximum number of threads to open
#define ___nChains___								|	// Number of temperature chains
#define ___nSteps___								|	// Total number of steps to attempt before a swap attempt
#define ___nWalkers___								|	// Total number of steps to attempt before a swap attempt
#define ___nSwaps___								|	// Total number of swaps between chains to attempt
#define ___nPars___									|	// Total number of parameters

/* Status interval Options */
#define ___dispInterval___ 							|	// Display status updates after n swaps have been attempted
#define ___saveInterval___							|	// Save partially completed results in case of crashes
#define ___statusBar___ 							|	// Turns on or off the dynamic status bar between disp Intervals


/* General Adaption Options */
#define ___adapt_last___							|	// Last swap event that adaption of relstep or beta can occur
#define ___min_adaption_factor___					|	// Lowest multiplier to modify relstep or beta
#define ___max_adaption_factor___					|	// Max multiplier to modify relstep or beta

/* Adapt Beta Options */
#define ___optimal_swap_accept___					| 	// Set optimum swap acceptance % that program should aim for
#define ___adapt_beta_interval___					|	// How often to change temperatures among chains
#define ___adapt_beta_rate___						|	// Exponential to change beta

/* Adapt Step Size Options */	
#define ___optimal_step_accept___					|	// Set optimum step acceptance % that program should aim for
#define ___adapt_step_interval___					|	// How often to change step size among chains
#define ___adapt_step_size___						|	// Exponential to change step size
#define ___gzStep___                				|	// Starting Step size

/* Initialization Options */
#define ___max_init_steps___						|		// Max number of parameters to test before running algorithm

/* Energy Options */
#define ___big_energy___							|	// Penalty energy

/* Data Options */
#define ___nExperiments___							|	// Number of exeriments/data files
#define ___ExperimentFiles___						|	// Array of filenames
#define ___log_data___								|	// true to tell the code to take the ln of your data. false if it is already in ln space
#define ___log_min___								|	// Define a minimum value to use if log encounters a 0 

/* Save Options */
#define ___matFilename___							|

/* cvODE Settings */
#define ___CVODE_NEQ___   							|	// number of equations 
#define ___CVODE_RTOL___  							| 	// scalar relative tolerance           
#define ___CVODE_MAX_NUM_STEPS___					|	// Set max number of steps for solver to take before reaching next time

/* Optional:  instead of providing cvode_atol, you can provide a vector of absolute tolerance levels, one for each ODE/state (CVODE_NEQ total states) that you have specified
example: 3 states:
#define ___CVODE_VECTOR_ATOL___ 		{1.0e-8, 1.0e-14, 1.0e-6}  
 */
#define ___CVODE_ATOL___ 							|	// Absolute tolerance for cvode, constant value for each ODE you have
// #define ___CVODE_DispWarnings___        /* UnComment this line if you want to see CVODE step size warnings! */

/* Some useful constants, if you need it */
#define ___pi___	3.14159265358979323846264338327950288419716939937510

/*=================================================================================================*
 *======================================OPTIONS END HERE===========================================*
 *=================================================================================================*/
#endif