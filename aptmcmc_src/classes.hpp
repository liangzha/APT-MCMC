#ifndef ___classHPP___
#define ___classHPP___

// #include <iostream>

template <typename T>
class Array2D
{
public:
  // constructor
  Array2D(unsigned ht,unsigned wd)
    : nWd(wd), nHt(ht), pAr(0)
  {
    if(wd > 0 && ht > 0)
    {
	  pAr = new T[wd * ht];
	  for (int i = 0; i < wd*ht; ++i)
		pAr[i] = 0;
	}
  }
  
  Array2D()
  {
	nWd = 0;
	nHt = 0;
	pAr = 0;
  }

  // destructor
  ~Array2D()
  {
  	// std::cout<<"\n=======DELETING ARRAY2D=======\n";
  	// for (int i=0; i < nHt; ++i)
  	// {
  	// 	for (int j = 0; j < nWd; ++j)
  	// 		std::cout << pAr[i*nWd+j] <<"\t";
  	// 	std::cout<<std::endl;
  	// }
  	// std::cout << "poop!\n";
    delete[] pAr;
  }
  
class BoundsViolation { };
  // indexing (parenthesis operator)
  //  two of them (for const correctness)

  const T& operator () (unsigned x,unsigned y) const
  {  if (x >= nHt || y >= nWd) throw BoundsViolation();  
    return pAr[ x*nWd + y ];   }

  T& operator () (unsigned x,unsigned y)
  { if (x >= nHt || y >= nWd) throw BoundsViolation();  
	return pAr[ x*nWd + y ];   }

  const T& operator () (unsigned x) const
  { if (x >= nHt*nWd)  throw BoundsViolation();
    return pAr[ x ];   }

  T& operator () (unsigned x)
  { if (x >= nHt*nWd)  throw BoundsViolation();
	return pAr[ x ];   }
  
  // get dims
  unsigned GetWd() const { return nWd; }
  unsigned GetHt() const { return nHt; }
  unsigned size() const { return nHt*nWd; }
  
  unsigned GetInd(unsigned x, unsigned y) { return x*nWd +y; }
  T * getChain(unsigned ID) { return &pAr[ID* nWd];}
  T * getPar() {return pAr;}
  // void setPar(T* new_pAr)  // this is stupidly dangerous, avoid!
  // {
  // 	delete[] pAr;
  // 	pAr = new_pAr;
  // }
class CopyError {} ;

  void copy(Array2D<T> x, unsigned i,unsigned j)
  {
  	// Copies data from Array2D x (of same type), starting at x(i,j)
  	unsigned len_self = nWd * nHt;
  	unsigned xloc = i * x.GetWd() + j;
  	unsigned len_x = x.GetWd() * x.GetHt();
  	if ((len_x-xloc) < len_self)				// prevent extraction from smaller x
  		throw CopyError();
  	for (int ii=0; ii < len_self; ++ii)
  	{
  		pAr[ii] = x(xloc+ii);
  	}
  }

  // void print()
  // {
  // 	// std::cout<<"\n=======DELETING ARRAY2D=======\n";
  // 	for (int i=0; i < nHt; ++i)
  // 	{
  // 		for (int j = 0; j < nWd; ++j)
  // 			std::cout << pAr[i*nWd+j] <<"\t";
  // 		std::cout<<std::endl;
  // 	}
  // }

  // void extract_col(T* col, int colID)
  // {
  // 	for (int i = 0; i < nHt; ++i)
  // 	{
  // 		col[i] = pAr[colID + i * nWd];
  // 	}
  // }
	
  void save(void *);
  
  // Copy constructor
  Array2D(const Array2D<T>& x) 
	: nWd(x.GetWd()), nHt(x.GetHt()), pAr(0)
  {
	pAr = new T[nWd * nHt];
	for (int i = 0; i < nWd*nHt; ++i)
		pAr[i] = x(i);
  }
  
  Array2D& operator = (const Array2D<T>& x)
  {
	nWd = x.GetWd();
	nHt = x.GetHt();
	T* new_pAr = new T[nWd * nHt];
	for (int i = 0; i < nWd*nHt; ++i)
		new_pAr[i] = x(i);
	
	delete[] pAr;
	pAr = new_pAr;
  }
  
  
  // private data members
private:
  unsigned nWd;
  unsigned nHt;
  T*       pAr;

  
};

//--------------------------------------------------------------------------
class Array3D{
	public:
	// constructor
	Array3D(unsigned d1_in,unsigned d2_in,unsigned d3_in)
		: d1(d1_in), d2(d2_in), d3(d3_in), data(0)
	{
		data = new double **[d1_in];
		for (int i = 0; i < d1_in; ++i)
		{
			data[i] = new double *[d2_in];
			for (int j = 0; j < d2_in; ++j)
			{
				data[i][j] = new double [d3_in];
				for (int k = 0; k < d3_in; ++k)
					data[i][j][k] = 0;
			}
			
		}
	}


	// deconstructor
	~Array3D()
	{
		for (int i = 0; i < d1; ++i)
		{
			for (int j = 0; j < d2; ++j)
			{
				delete [] data[i][j];
			}
			delete [] data[i];
		}
		delete [] data;
	}
	
	class BoundsViolation { };
	
	// Access methods to get the (i,j,k,l) element:
	double& operator() (unsigned i, unsigned j, unsigned k)
	{
		if (i >=d1 || j>=d2 || k>= d3) throw BoundsViolation();
		return data[i][j][k];
	}
	
	double const& operator() (unsigned i, unsigned j, unsigned k) const
	{
		if (i >=d1 || j>=d2 || k>= d3) throw BoundsViolation();
		return data[i][j][k];
	}
	
	unsigned getd1() { return d1;}
	unsigned getd2() { return d2;}
	unsigned getd3() { return d3;}
	double *** ptr() { return data;}
	double ** getChain(unsigned ID) { return data[ID];}
	double * getPar(unsigned d1, unsigned d2) { return data[d1][d2];}
	
	void save(void *);
	
	private:
		unsigned d1,d2,d3;
		double ***data;
		Array3D(const Array3D&);
		Array3D& operator = (const Array3D&);
};

//--------------------------------------------------------------------------
class Array4D{
	public:
	// constructor
	Array4D(unsigned d1_in,unsigned d2_in,unsigned d3_in,unsigned d4_in)
		: d1(d1_in), d2(d2_in), d3(d3_in), d4(d4_in)
	{
		data = new double***[d1_in];
		for (int i = 0; i < d1_in; ++i)
		{			
			data[i] = new double **[d2_in];
			for (int j = 0; j < d2_in; ++j)
			{
				data[i][j] = new double *[d3_in];
				for (int k = 0; k < d3_in; ++k)
				{
					data[i][j][k] = new double[d4_in];
					for (int l = 0 ; l < d4_in; ++l)
						data[i][j][k][l] = 0;
				}
			}			
		}
	}

	// deconstructor
	~Array4D()
	{
		for (int i = 0; i < d1; ++i)
		{
			for (int j = 0; j < d2; ++j)
			{
				for (int k = 0; k < d3; ++k)
				{
					delete [] data[i][j][k];
				}
				delete [] data[i][j];
			}
			delete [] data[i];
			
		}
		delete [] data;
	}
	
	class BoundsViolation { };
	// Access methods to get the (i,j,k,l) element:
	double& operator() (unsigned d1_in,unsigned d2_in,unsigned d3_in,unsigned d4_in)
	{
		if (d1_in>=d1||d2_in>=d2||d3_in>=d3||d4_in>=d4) throw BoundsViolation();
		return data[d1_in][d2_in][d3_in][d4_in];
	}
	
	double const& operator() (unsigned d1_in,unsigned d2_in,unsigned d3_in,unsigned d4_in) const
	{
		if (d1_in>=d1||d2_in>=d2||d3_in>=d3||d4_in>=d4) throw BoundsViolation();
		return data[d1_in][d2_in][d3_in][d4_in];
	}
	
	double **** ptr() { return data;}
	unsigned getd1() { return d1;}
	unsigned getd2() { return d2;}
	unsigned getd3() { return d3;}
	unsigned getd4() { return d4;}
	double * getPar(unsigned chain, unsigned walker, unsigned swap) { return data[chain][walker][swap];}
	
	void save(void *);
	
	private:
		unsigned d1;
		unsigned d2;
		unsigned d3;
		unsigned d4;
		double ****data;
		Array4D(const Array4D&);
		Array4D& operator = (const Array4D&);
};

#endif