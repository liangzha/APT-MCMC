#ifndef ___functions_HPP___
#define ___functions_HPP___
#include <sundials/sundials_types.h> /* definition of type realtype */

realtype hill ( realtype x, realtype k );
realtype hilln(realtype x, realtype n, realtype k);
realtype heavy (realtype __z__);

#endif