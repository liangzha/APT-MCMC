#include "options.hpp"
#include "classes.hpp"				 /* Support for 2d array virtualization */
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <list>
#include <vector>
#include <algorithm> 
#include <math.h>

/* Helper function for reading data file */
static void split_line( std::string& line,  std::string delim, std::list<std::string>& values)
{
    size_t pos = 0;
	// std::cout << "Splitting the return for sequence: '" << line << "'" << std::endl;
  //   while ((pos = line.find(delim, pos)) != std::string::npos) {
  //       std::string p = line.substr(0, pos);
		// std::cout<< pos << std::endl;
		// std::cout<< "pushed " << p << " into values\n";
  //       values.push_back(p);
  //       line = line.substr(pos + 1);
  //   }
	while ((pos = line.find(delim, 0)) != std::string::npos) {
        std::string p = line.substr(0, pos);
		// std::cout<< pos << std::endl;
		// std::cout<< "pushed " << p << " into values\n";
        values.push_back(p);
        line = line.substr(pos + 1);
    }

    //if (!line.empty()) {
        values.push_back(line);
		//std::cout<< "pushed " << line << " into values\n";
    //}
}

 void importData(Array2D<double>& output,  std::vector<int> &validInd, char* csvName)
{
    std::ifstream file ( csvName); 	/* Open file buffer here */
    std::string value;					/* Temp string value to read each line */
    std::list<std::string> values;		/* Save parsed lines into a list of strings */
	int nCol=1, nRow=1; 				/* Initialize number of rows and columns to 1 */
	
	if (! file.good() )
	{
		printf("loading file error!\n");
		exit (EXIT_FAILURE);
	}
	bool one_col_data = true;
	bool one_row_data = true;

	/* Read the CSV file into a list of strings (values)*/
    while ( file.good() )  						 			/* Loop until end of file is reached */
    {
        std::getline ( file, value, ',' );  			 	/* Get string until next comma is reached */
		
		if (!file.good() && one_col_data) 			/* Special case: 1 column of data */
		{
			nCol = 1;
			split_line(value, "\n", values);
			nRow=values.size();
			break;
		}

		if (value.find('\n') != std::string::npos) {  		/* Split if a line return is encountered */
            split_line(value, "\n", values);
			++nRow;
			one_row_data = false;
        } 
        else 
        {								 			/* Else push the value into the list */
        	one_col_data = false;
            values.push_back(value);
			if (nRow==1) ++nCol;
        }
    }
    // Note, 1 row data doesn't work! it adds an extra column
    // if (one_row_data) 
    // {
    // 	nCol -= 1;
    // 	values.pop_back();
    // }

	/* Parse through the list (values) and convert non null character arrays into doubles; store result into tempdata */
	Array2D<double> data(nRow,nCol);		/* Double array to store the data sequentially, lets put it into matrix later */
//	printf("row=%i, col = %i\n", nRow, nCol); //debug
    std::list<std::string>::const_iterator it = values.begin();	 /* String iterator it needs to be created for for-loop*/
	int counter = 0 ;
    for (; it != values.end(); it++) {
        std::string tmp = *it;
		const char *cpoint = tmp.c_str();
		if (*cpoint!='\0' && *cpoint!='\r')  // convert to double if the characters are not null
		{
			double d = atof(cpoint);
			//double d = strtod(cpoint,NULL);
			if (___log_data___) 
			{
				if (d< ___log_min___)
					data(counter) = log(___log_min___);
				else
					data(counter) = log(d);
			}
			else
				data(counter) = d;
//			printf("adding %f to data[%i]\n",d,counter);
		validInd.push_back(counter);		// Add the valid index into validInd
		}
		
		++counter; 			// increment location/index
    }

    // for (int i =0; i < ___CVODE_NEQ___; i++)
    // 	validInd.pop_front(); // no need for initial conditions to be compared against data...
	
	std::cout <<"    Data with  Rows: " << nRow << ", Columns: " << nCol << ", successfully imported." << std::endl; 	
	
	// /* Parse through tempdata to put them into tsim */
	
	// // Debugging only
	// std::cout << std::endl;
	// // Use this when comparing CVODE outputs
	// for(auto e:validInd)
		// std::cout << e << std:: endl;
	
	// Print Data
	// for (int i = 0; i < nCol; i++)
	// {
	// 	std::cout<< "state " << i << std::endl;
	// 	for (int j = 0; j < nRow; j++)
	// 		std::cout<< data(j,i) << std::endl;
	// }
	// Data can be accessed as such: data(time, state) 
	
	
	// Pass data back into main. 
	output = data;
	
}
