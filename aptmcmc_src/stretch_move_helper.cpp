#include <vector>
#include <trng/yarn3.hpp>
#include <trng/uniform_dist.hpp>	 /* Support for TRNG's uniform distribution */
#include <trng/discrete_dist.hpp>	 /* Support for TRNG's discrete integer (0 to N-1) distribution */
#include <math.h>
// #include "options.hpp"
//#include <iostream>

/* Inputs
		&F, where F is a vector of randomized numbers along the [0,1] uniform distribution
		a, where a is the parameter of this PDF
*/
// void g_PDF(double &F, double a, trng::yarn3 &gen)
void g_PDF(std::vector<double> &F, double a, trng::yarn3 &gen)
{
	size_t N = F.size();
	trng::uniform_dist<> u(-log(a),log(a));
	for (int i = 0; i<N; i++) 
	{
		F[i] = exp(u(gen));
	}
}

// void sample_ensemble(int &F, int N, trng::yarn3 &gen)
void sample_ensemble(std::vector<int> &F, trng::yarn3 &gen)
{
	// trng::discrete_dist u(N);

	// // single gen
	// F = u(gen);

	// multiple gen
	size_t N = F.size();
	trng::discrete_dist u(N/2);
	
	// Loop over ODD indicies, generating EVEN numbers
	for (int i = 1; i<N; i+=2)
	{
		F[i] = 2 * u(gen);
	}
	for (int i = 0; i <N; i+=2)
	{
		F[i] = 2 * u(gen) + 1;
	}
}