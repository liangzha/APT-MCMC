/*
 * -----------------------------------------------------------------
 * $Revision: 0.1 $
 * $Date: 2014/11/19 $
 * -----------------------------------------------------------------
 * Programmer(s): Li Ang Zhang @ Chemical and Petroleum Engineering,
 *                Swanson School of Engineering, 
 *                University of Pittsburgh, Pittsburgh,PA, USA
 * -----------------------------------------------------------------
 * Description:
 * 
 * The following is an engine to solve a system of ODEs using 
 * CVODE's Backwards Differentiation Formula method and Netwon 
 * iteration with the CVDENSE dense linear solver.
 * It uses a scalar relative tolerance and a vector absolute
 * tolerance.
 * -----------------------------------------------------------------
 */
 
/* Header files with a description of contents used */
#include "options.hpp"				 /* User-specified MCMC Options */
#include <sundials/sundials_types.h> /* definition of type realtype */
#include "classes.hpp"				 /* definition of custom Array2D class */
#include <cvode/cvode.h>             /* prototypes for CVODE fcts., consts. */
#include <nvector/nvector_serial.h>  /* serial N_Vector types, fcts., macros */
#include <cvode/cvode_dense.h>       /* prototype for CVDense */
#include <sundials/sundials_dense.h> /* definitions DlsMat DENSE_ELEM */

#include <vector>                   /* Include support for std::vector */

#include "CVODE_engine.hpp"         /* Includes function definitions and macros for this engine file */
#include <stdio.h>                  /* Standard IO library */


// Function definition for the ODE that you are trying to solve
int rhs(realtype t, N_Vector y, N_Vector ydot, void *user_data);


/*
 *-------------------------------
 * Begin CVODE Integrate Engine
 *-------------------------------
 */


int integrate(unsigned int exp_no, void *par, Array2D<realtype> &model, const std::vector<double> &Y0, const std::vector<double> &tsim) 
{
	
  realtype reltol, t;
  N_Vector y;
  void *cvode_mem;
  int flag, flagr;
  
  /* Index Counters */
  size_t ii;

  y =  NULL;
  cvode_mem = NULL;

  /* Create serial vector of length ___CVODE_NEQ___ for I.C. */
  y = N_VNew_Serial(___CVODE_NEQ___);
  if (check_flag((void *)y, "N_VNew_Serial", 0)) 
    return(1);
  
  /* Load initial conditions into the state vector Y */
  for (ii = 0; ii < ___CVODE_NEQ___; ++ii)
    {
		NV_Ith_S(y,ii) = Y0[ii];
		model(0,ii) = Y0[ii];
	}
    
  /* If user specified a vector of absolute tolerances, load them up now */
  #ifdef ___CVODE_VECTOR_ATOL___
    /* First create a serial vector to hold abstol */
    N_Vector abstol = NULL;
    abstol = N_VNew_Serial(___CVODE_NEQ___); 
    if (check_flag((void *)abstol, "N_VNew_Serial", 0)) 
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      N_VDestroy_Serial(abstol);
      return(1);
    }
    /* Now load this vector of absolute tolerances */
    realtype abstol_values [___CVODE_NEQ___] = ___CVODE_VECTOR_ATOL___;
    for (ii = 0; ii < ___CVODE_NEQ___ ; ++ii)
      NV_Ith_S(abstol,ii) = abstol_values[ii]; 
  #endif



  /* Call CVodeCreate to create the solver memory and specify the 
  * Backward Differentiation Formula and the use of a Newton iteration */
  cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
  if (check_flag((void *)cvode_mem, "CVodeCreate", 0))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    return(1);
  }
  
  /* Call CVodeInit to initialize the integrator memory and specify the
  * user's right hand side function in y'=rhs(t,y), the inital time tsim[0], and
  * the initial dependent variable vector y. */
  flag = CVodeInit(cvode_mem, rhs, tsim[0], y);
  if (check_flag(&flag, "CVodeInit", 1))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return(1);
  }

  /* Call CVodeSVtolerances to specify the scalar relative tolerance
  * and vector absolute tolerances */
  reltol = ___CVODE_RTOL___;      /* Set the scalar relative tolerance */
  #ifdef ___CVODE_VECTOR_ATOL___
    flag = CVodeSVtolerances(cvode_mem, reltol, abstol);
    if (check_flag(&flag, "CVodeSVtolerances", 1)) 
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      N_VDestroy_Serial(abstol);
      CVodeFree(&cvode_mem);
      return(1);
    }
  #else 
    flag = CVodeSStolerances(cvode_mem, reltol, ___CVODE_ATOL___);
    if (check_flag(&flag, "CVodeSVtolerances", 1)) 
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      CVodeFree(&cvode_mem);
      return(1);
    }
  #endif
  
  /* Pass the parameters into the RHS function */
  realtype odepar[___nPars___];
  odepar[0] = exp_no;
  for (int ii = 0; ii < ___nPars___; ++ii)
  {
    odepar[ii+1] = ((realtype *) par)[ii];
  }


  flag = CVodeSetUserData(cvode_mem, (void*) odepar);
  if (check_flag(&flag, "CVodeSetUserdata", 1))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }
  
  #ifndef ___CVODEDispWarnings___
    flag = CVodeSetMaxHnilWarns(cvode_mem, -1);
    if (check_flag(&flag, "CVodeSetMaxHnilWarns", 1))
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      #ifdef ___CVODE_VECTOR_ATOL___
        N_VDestroy_Serial(abstol);
      #endif
      CVodeFree(&cvode_mem);
      return 1;
    }
  #endif

  /* Call CVDense to specify the CVDENSE dense linear solver */
  flag = CVDense(cvode_mem, ___CVODE_NEQ___);
  if (check_flag(&flag, "CVDense", 1))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }
    
  /* Set the maximum number of steps to be taken by the solver in an attempt to
     reach the next output time */
  flag = CVodeSetMaxNumSteps(cvode_mem, ___CVODE_MAX_NUM_STEPS___);
  if (check_flag(&flag, "CVodeSetMaxNumSteps", 1))
  {   /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }
  
  /* Set a custom error function for CVODE to prevent the countless SUNDIALS_ERROR messages that pop up */
  flag = CVodeSetErrHandlerFn(cvode_mem, myErrFcn, (void *) odepar);
  if (check_flag(&flag, "CVodeSetErrHandlerFn", 1))
  {   /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }

	size_t nTime = tsim.size();
	size_t nState = Y0.size();
  /* In a for loop, call CVode, print results, and test for error.
    Start loop at ii=1 because tsim[0] is the initial condition.
    Break out of for loop if an error is encountered */	
  for (ii =  1; ii < nTime; ++ii) // start at ii=1 due to tsim[0] = initial conditions
{
    flag = CVode(cvode_mem, tsim[ii], y, &t, CV_NORMAL);
    for (int state = 0; state < nState; ++state)
		model(ii,state) = NV_Ith_S(y,state);

    if (check_flag(&flag, "CVode", 1)) {
        N_VDestroy_Serial(y);
        #ifdef ___CVODE_VECTOR_ATOL___
			N_VDestroy_Serial(abstol);
		#endif
        CVodeFree(&cvode_mem);
        return 1; 
    }
    
} 

  /* Print some final statistics */
  //PrintFinalStats(cvode_mem);

  /* Free y and abstol vectors */
  N_VDestroy_Serial(y);
  #ifdef ___CVODE_VECTOR_ATOL___
    N_VDestroy_Serial(abstol);
  #endif

  /* Free integrator memory */
  CVodeFree(&cvode_mem);

  return(0); 
} /* End Main */


/*
 *-------------------------------
 * Private helper functions
 *-------------------------------
 */

static void PrintOutput(realtype t, realtype y1, realtype y2, realtype y3)
{
#if defined(SUNDIALS_EXTENDED_PRECISION)
  printf("At t = %0.4Le      y =%14.6Le  %14.6Le  %14.6Le\n", t, y1, y2, y3);
#elif defined(SUNDIALS_DOUBLE_PRECISION)
  printf("At t = %0.4le      y =%14.6le  %14.6le  %14.6le\n", t, y1, y2, y3);
#else
  printf("At t = %0.4e      y =%14.6e  %14.6e  %14.6e\n", t, y1, y2, y3);
#endif

  return;
}


/* 
 * Get and print some final statistics
 */

static void PrintFinalStats(void *cvode_mem)
{
  long int nst, nfe, nsetups, nje, nfeLS, nni, ncfn, netf, nge;
  int flag;

  flag = CVodeGetNumSteps(cvode_mem, &nst);
  check_flag(&flag, "CVodeGetNumSteps", 1);
  flag = CVodeGetNumRhsEvals(cvode_mem, &nfe);
  check_flag(&flag, "CVodeGetNumRhsEvals", 1);
  flag = CVodeGetNumLinSolvSetups(cvode_mem, &nsetups);
  check_flag(&flag, "CVodeGetNumLinSolvSetups", 1);
  flag = CVodeGetNumErrTestFails(cvode_mem, &netf);
  check_flag(&flag, "CVodeGetNumErrTestFails", 1);
  flag = CVodeGetNumNonlinSolvIters(cvode_mem, &nni);
  check_flag(&flag, "CVodeGetNumNonlinSolvIters", 1);
  flag = CVodeGetNumNonlinSolvConvFails(cvode_mem, &ncfn);
  check_flag(&flag, "CVodeGetNumNonlinSolvConvFails", 1);

  flag = CVDlsGetNumJacEvals(cvode_mem, &nje);
  check_flag(&flag, "CVDlsGetNumJacEvals", 1);
  flag = CVDlsGetNumRhsEvals(cvode_mem, &nfeLS);
  check_flag(&flag, "CVDlsGetNumRhsEvals", 1);

  flag = CVodeGetNumGEvals(cvode_mem, &nge);
  check_flag(&flag, "CVodeGetNumGEvals", 1);

  printf("\nFinal Statistics:\n");
  printf("nst = %-6ld nfe  = %-6ld nsetups = %-6ld nfeLS = %-6ld nje = %ld\n",
	 nst, nfe, nsetups, nfeLS, nje);
  printf("nni = %-6ld ncfn = %-6ld netf = %-6ld nge = %ld\n \n",
	 nni, ncfn, netf, nge);
}

/*
 * Check function return value...
 *   opt == 0 means SUNDIALS function allocates memory so check if
 *            returned NULL pointer
 *   opt == 1 means SUNDIALS function returns a flag so check if
 *            flag >= 0
 *   opt == 2 means function allocates memory so check if returned
 *            NULL pointer 
 */

 
static int check_flag(void *flagvalue, char *funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
	    funcname);
    return(1); }

  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      // fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
	      // funcname, *errflag);
      return(1); }}

  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
	    funcname);
    return(1); }

  return(0);
}


void myErrFcn( int error_code, const char *module, const char *function, char *msg, void *eh_data)
{ /*Do nothing when error is encountered to pretty up terminal output for MCMC.*/ }