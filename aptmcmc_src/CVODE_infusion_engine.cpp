/*
 * -----------------------------------------------------------------
 * $Revision: 0.4 $
 * $Date: 2016/3/22 $
 * -----------------------------------------------------------------
 * Programmer(s): Li Ang Zhang, Timothy Knab 
 *                @ Chemical and Petroleum Engineering,
 *                Swanson School of Engineering, 
 *                University of Pittsburgh, Pittsburgh,PA, USA
 * -----------------------------------------------------------------
 * Description:
 * 
 * The following is an engine to solve a system of ODEs using 
 * CVODE's Backwards Differentiation Formula method and Netwon 
 * iteration with the CVDENSE dense linear solver.
 * It uses a scalar relative tolerance and a vector absolute
 * tolerance.
 * -----------------------------------------------------------------
 */
 
/* Header files with a description of contents used */
#include "options.hpp"				          /* User-specified MCMC Options */
// #include <sundials/sundials_types.h>    /* definition of type realtype */
#include "classes.hpp"				          /* definition of custom Array2D class */
#include <cvode/cvode.h>                /* prototypes for CVODE fcts., consts. */
#include <nvector/nvector_serial.h>     /* serial N_Vector types, fcts., macros */
#include <cvode/cvode_dense.h>          /* prototype for CVDense */
#include <sundials/sundials_dense.h>    /* definitions DlsMat DENSE_ELEM */

#include <vector>                       /* Include support for std::vector */
#include <algorithm>                    /* Include support for sort() */
#include <cmath>                        /* abs value */

#include "CVODE_infusion_engine.hpp"    /* Includes function definitions and macros for this engine file */
#include <stdio.h>                      /* Standard IO library */
// #include <stdlib.h>                    /* For abs */

// Todo:
// [x] case where ramp_time and zoh time are not equal, but both less than next time step
//    - how update?
// [ ] Special case: infusion begins at t=0
// [ ] check ramp, zoh are same width, must contain at least t0, tend



// Function definition for the user specified ODEs
int rhs(realtype t, N_Vector y, N_Vector ydot, realtype* par, realtype* u);

// Check if the time is close enough to activate the ZOH infusion
int close_enough(realtype curr_time, realtype infus_time)
{
  if ( std::abs(curr_time - infus_time) <= ___CVODE_ATOL___ )
    return 1;
  else
    return 0;
}

// Use the point-slope equation to calculate exact value of ramp infusion
realtype ramp_u(realtype x, realtype x1, realtype x2, realtype y1, realtype y2)
{
  if (y1 == y2)
    return y1;
  else
    return y1 + (y2-y1)/(x2-x1) * (x-x1);      // add atol to hopefully trigger activation near 0
}

// Copy a row from ZOH into infusion array: u
void copy_row(realtype* u, Array2D<realtype>* zoh_segment, unsigned int rowID)
{
  for (int j = 0; j < zoh_segment->GetWd(); ++j)
  {
    u[j] = (*zoh_segment)(rowID, j);
  }
}

// Process the infusion data into a single u input. 
int infus_wrapper(realtype t, N_Vector y, N_Vector ydot, void *user_data)
{
  // Decrypt the void pointer
  realtype* par = (double*)(*((void**) user_data));
  Array2D<realtype>* zoh_segment =  static_cast< Array2D<realtype>*>(*((void**) user_data+1));
  Array2D<realtype>* ramp_segment =  static_cast< Array2D<realtype>*>(*((void**) user_data+2));
  
  // printf("|currt = %2.3e",t);
  // printf(" rampsegment: %2.1f,%2.1f\\%2.1f,%2.1f  ", (*ramp_segment)(0,0), (*ramp_segment)(0,1), (*ramp_segment)(1,0), (*ramp_segment)(1,1) );
  // printf(" zoh_segment = \n [%f, %f]\n [%f, %f]\n", (*zoh_segment)(0,0),(*zoh_segment)(0,1),(*zoh_segment)(1,0),(*zoh_segment)(1,1));
  
  realtype u[zoh_segment->GetWd()];

  if (t >= (*zoh_segment)(1,0))                     copy_row(u, zoh_segment, 1);
  else if (close_enough(t, (*zoh_segment)(1,0) ))   copy_row(u, zoh_segment, 1);
  else                                              copy_row(u, zoh_segment, 0);

  for (int i = 1; i < ramp_segment->GetWd(); ++i)
  {
    // printf(" rampcontribution = %2.1f  ", ramp_u(t, (*ramp_segment)(0,0), (*ramp_segment)(1,0), (*ramp_segment)(0,i), (*ramp_segment)(1,i) ) );
    u[i] += ramp_u(t, (*ramp_segment)(0,0), (*ramp_segment)(1,0), (*ramp_segment)(0,i), (*ramp_segment)(1,i) );
  }    

  // printf("\ty=[%2.1e, %2.1e]\t",NV_Ith_S(y,0),NV_Ith_S(y,1));
  // printf("\tu=%2.2f", u[1]);
  // printf("|\n");
  // printf(" zoh segment afta= [%2.1e]\n", (*zoh_segment)(0,1));
  return rhs(t, y, ydot, par, u);
}

/*
 *-------------------------------
 * Begin CVODE Integrate Engine
 *-------------------------------
 */ 

int integrate(unsigned int exp_no, void *par, Array2D<realtype> *model, const std::vector<double> &Y0, const std::vector<double> &tsim, Array2D<realtype> infus_ramp, Array2D<realtype> infus_zoh, Array2D<realtype> infus_bol) 
{

  // printf("Integrate: Width of infuse zoh  is %i \n",infus_zoh.GetWd());
	
  realtype reltol, t;
  N_Vector y;
  void *cvode_mem;
  int flag, flagr;
  
  /* Index Counters */
  size_t ii;

  y =  NULL;
  cvode_mem = NULL;

  size_t nTime = tsim.size();
  size_t nState = Y0.size();

  /* Perform a precheck to see if infusion arrays are ill-conditioned */
  if (tsim[0] != infus_ramp(0)) 
  {
    printf("ERROR: Ramp infusion array is ill-conditioned.\n\tEnsure first infusion time is the same as the first time of your tsim.\n");
    exit(EXIT_FAILURE);
  }
  else if (tsim[0] != infus_zoh(0)) 
  {
    printf("ERROR: Zero-Order-Hold infusion array is ill-conditioned.\n\tEnsure first infusion time is the same as the first time of your tsim.\n");
    exit(EXIT_FAILURE);
  }
  else if (tsim[nTime-1] != infus_ramp(infus_ramp.GetHt()-1,0))
  {
    printf("ERROR: Ramp infusion array is ill-conditioned.\n\tEnsure last infusion time is the same as the last time of your tsim.\n");
    exit(EXIT_FAILURE);
  }
  else if (tsim[nTime-1] != infus_zoh(infus_zoh.GetHt()-1,0)) 
  {
    printf("ERROR: Zero-Order-Hold infusion array is ill-conditioned.\n\tEnsure last infusion time is the same as the last time of your tsim.\n");
    exit(EXIT_FAILURE);
  }
  else if ( infus_bol.GetWd() != (nState + 1) )
  {
    printf("ERROR: Bolus infusion array is ill-conditioned.\n\tEnsure its width is equal to number of states + 1: [t, s_1, s_2, ... s_n]\n");
    exit(EXIT_FAILURE);
  }
  else if ( infus_zoh.GetWd() != infus_ramp.GetWd())
  {
    printf("ERROR: Bolus and ramp infusion arrays are ill-conditioned.\n\tEnsure their widths are equal.\n");
    exit(EXIT_FAILURE);
  }

  // Overhead for CVODE
  /* Create serial vector of length ___CVODE_NEQ___ for I.C. */
  y = N_VNew_Serial(___CVODE_NEQ___);
  if (check_flag((void *)y, "N_VNew_Serial", 0)) 
    return(1);
  
  /* Load initial conditions into the state vector Y */
  for (ii = 0; ii < ___CVODE_NEQ___; ++ii)
    {
		NV_Ith_S(y,ii) = Y0[ii];
		(*model)(0,ii) = Y0[ii];
	}
    
  /* If user specified a vector of absolute tolerances, load them up now */
  #ifdef ___CVODE_VECTOR_ATOL___
    /* First create a serial vector to hold abstol */
    N_Vector abstol = NULL;
    abstol = N_VNew_Serial(___CVODE_NEQ___); 
    if (check_flag((void *)abstol, "N_VNew_Serial", 0)) 
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      N_VDestroy_Serial(abstol);
      return(1);
    }
    /* Now load this vector of absolute tolerances */
    realtype abstol_values [___CVODE_NEQ___] = ___CVODE_VECTOR_ATOL___;
    for (ii = 0; ii < ___CVODE_NEQ___ ; ++ii)
      NV_Ith_S(abstol,ii) = abstol_values[ii]; 
  #endif

  /* Call CVodeCreate to create the solver memory and specify the 
  * Backward Differentiation Formula and the use of a Newton iteration */
  cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
  if (check_flag((void *)cvode_mem, "CVodeCreate", 0))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    return(1);
  }
  
  /* Call CVodeInit to initialize the integrator memory and specify the
  * user's right hand side function in y'=rhs(t,y), the inital time tsim[0], and
  * the initial dependent variable vector y. */
  flag = CVodeInit(cvode_mem, infus_wrapper, tsim[0], y);
  // flag = CVodeInit(cvode_mem, rhs2, tsim[0], y);
  if (check_flag(&flag, "CVodeInit", 1))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return(1);
  }

  /* Call CVodeSVtolerances to specify the scalar relative tolerance
  * and vector absolute tolerances */
  reltol = ___CVODE_RTOL___;      /* Set the scalar relative tolerance */
  #ifdef ___CVODE_VECTOR_ATOL___
    flag = CVodeSVtolerances(cvode_mem, reltol, abstol);
    if (check_flag(&flag, "CVodeSVtolerances", 1)) 
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      N_VDestroy_Serial(abstol);
      CVodeFree(&cvode_mem);
      return(1);
    }
  #else 
    flag = CVodeSStolerances(cvode_mem, reltol, ___CVODE_ATOL___);
    if (check_flag(&flag, "CVodeSVtolerances", 1)) 
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      CVodeFree(&cvode_mem);
      return(1);
    }
  #endif

  /* Pass the parameters and infusions into the RHS function via void pointer user_data*/

  // Initialize some variables for infusion handling
  unsigned int n_zoh = infus_zoh.GetHt();    // keep track of number of time inputs to zoh infusions
  unsigned int n_ramp = infus_ramp.GetHt();  // keep track of number of time inputs to zoh infusions
  unsigned int zoh_counter=0,ramp_counter=0; // value to keep track of what time we are on in zoh infusions
  Array2D<realtype> zoh_segment(2, infus_zoh.GetWd());  // segment to store 2 rows of zoh at a time
  Array2D<realtype> ramp_segment(2, infus_ramp.GetWd());// segment to store 2 rows of ramp at a time
    
  bool set_stopflag = false;                 // flag to tell cvode to set a stop time
  void* user_data[3];                        // void** to hold { par, infus_zoh, infus_ramp} 

  if (n_zoh > 1)      // infusion array must be larger than 1 row
  {
    zoh_segment.copy(infus_zoh, zoh_counter, 0);
    set_stopflag = true;  
    for (int i = 1; i < n_zoh; ++i)         // check for time monotonicity 
    {
      if (infus_zoh(i,0) <= infus_zoh(i-1,0))
      {
        printf("ERROR: Bolus infusion array is ill-conditioned.\n\tTime column must be monotonically increasing.\n");
        exit(EXIT_FAILURE);
      }
    }
  }
  else
    zoh_segment(1,0) = tsim[nTime-1];
  
  
  if (n_ramp > 1)   // infusion array must be larger than 1 row
  {
    ramp_segment.copy(infus_ramp, ramp_counter, 0);
    set_stopflag = true;  
    for (int i = 1; i < n_ramp; ++i)        // check for time monotonicity 
    {
      if (infus_ramp(i,0) <= infus_ramp(i-1,0))
      {
        printf("ERROR: Ramp infusion array is ill-conditioned.\n\tTime column must be monotonically increasing.\n");
        exit(EXIT_FAILURE);
      }
    }
  }
  else
    ramp_segment(1,0) = tsim[nTime-1];

  realtype odepar[___nPars___];
  odepar[0] = exp_no;
  for (int ii = 0; ii < ___nPars___; ++ii)
  {
    odepar[ii+1] = ((realtype *) par)[ii];
  }

  user_data[0] = (void*) odepar;
  user_data[1] = (void*) &zoh_segment;
  user_data[2] = (void*) &ramp_segment;

  flag = CVodeSetUserData(cvode_mem, user_data);  
  if (check_flag(&flag, "CVodeSetUserdata", 1))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }

  std::vector<double> tsim_checked;                     // new list of tsims, t0 is saved
  std::vector<bool> tsim_save;                          // lets cvode know if we save results or not to model
  std::vector<double>::iterator it;                     // helper iterator for ramp/zoh infusions
  std::vector<double>::iterator curr_bolus;             // helper iterator for bolus infusions
  std::vector<double> infus_time;                       // store the times of infusions
  std::vector<double> bolus_time;                       // store the times of bolus infusions

  int zoh_i = 1, ramp_i = 1, bol_i = 0;

  // Process bolus infusion array
  if (tsim[0] == infus_bol(0))                          // ignore the first bolus if first time is 0 
  {
    //printf("WARNING: Bolus infusion array contains tstart, turning off boluses...\n");
    bol_i = 1;
  }

  for ( ; bol_i < infus_bol.GetHt(); ++bol_i)
  {
    bolus_time.push_back(infus_bol(bol_i,0));
    // printf("added bolus: %2.1f\n", infus_bol(bol_i,0));
  }
  bol_i = tsim[0] == infus_bol(0);                      // ignore the first bolus if first time is 0

  // Initialize a new list of tsim in case multiple infusions occur before next tsim step
  for (ii =  1; ii < nTime; ++ii)                       // loop over user-specified integration times
  {
      // If an infusion is detected, perform a precheck of integration times
    if (set_stopflag)
    {
      std::vector<double> infus_time_epoch;
      // Define the current epoch
      double tlast = tsim[ii-1];
      double tcurr = tsim[ii];

      // Check if any ZOH/ramp/bol infusions occurs within this epoch
      // If so, add all infusion times within this epoch to infus_time_epoch
      for ( ; zoh_i < n_zoh-1; ++zoh_i)
      {
        if ((infus_zoh(zoh_i,0) > tlast) && (infus_zoh(zoh_i,0) <= tcurr) )     infus_time_epoch.push_back(infus_zoh(zoh_i,0));
        else if (infus_zoh(zoh_i,0) > tcurr )                                   break;
      }
      for ( ; ramp_i < n_ramp-1; ++ramp_i)
      {
        if ((infus_ramp(ramp_i,0) > tlast) && (infus_ramp(ramp_i,0) <= tcurr))   infus_time_epoch.push_back(infus_ramp(ramp_i,0));
        else if (infus_ramp(ramp_i,0) > tcurr )                                  break;
      }
      for ( ; bol_i < infus_bol.GetHt(); ++bol_i)
      {
        if ((infus_bol(bol_i,0) > tlast) && (infus_bol(bol_i,0) <= tcurr))      infus_time_epoch.push_back(infus_bol(bol_i,0));
        else if (infus_bol(bol_i,0) > tcurr )                                   break;
      }

      std::sort(infus_time_epoch.begin(), infus_time_epoch.end());            // sort the times in infus_time_epoch
      it = std::unique(infus_time_epoch.begin(), infus_time_epoch.end());     // keep unique elements in this vector
      infus_time_epoch.resize( std::distance(infus_time_epoch.begin(), it) ); // reshape infus_time_epoch because we may have removed some nonunique elements
      // printf("!! in epoch %2.1e to %2.1e, infusions are : ",tlast, tcurr); // debug print line

      // for (it = infus_time_epoch.begin(); it < infus_time_epoch.end(); ++it) printf(" %2.1e ",*it);   //debug print line

      // If infusions are detected between an integration time epoch, insert those infusion times into the tsim.
      if (infus_time_epoch.size() > 0)
      {
        // printf("Adding times!\n"); //debug
        for (it = infus_time_epoch.begin(); it < infus_time_epoch.end(); ++it) // Loop through each element in infus_timem_epoch (except for last one)
        {                                                                       // Append avg of curr el + next one to tsim_checked 
          if (*it != tcurr)
          {
            tsim_checked.push_back( *it);                                          
            tsim_save.push_back(false);  
          }
          infus_time.push_back(*it);
        }
      }
      else if (infus_time_epoch.size() == 1)
        infus_time.push_back(infus_time_epoch[0]);
    }

    tsim_checked.push_back(tsim[ii]);     // Append the next tsim
    tsim_save.push_back(true);
  }
  infus_time.push_back(tsim[nTime-1]+___CVODE_ATOL___);                  // Add an impossible infusion time to disable infusions at last time step

  // printf("Finished Precheck! Integrating at times:\n\t["); // debug print line
  // for (it = tsim_checked.begin(); it != tsim_checked.end(); ++it)   // debug print line
  // {                                                                 // debug print line
  //     printf("%2.1f  ",*it);                                        // debug print line
  // }                                                                 // debug print line
  // printf("]\n\n");                                                  // debug print line
  // printf("Infusion at times:\n\t[");                                // debug print line
  // for (it = infus_time.begin(); it != infus_time.end(); ++it)       // debug print line
  // {                                                                 // debug print line
  //   printf("%2.1f  ",*it);                                          // debug print line
  // }                                                                 // debug print line
  // printf("]\n\n");                                                  // debug print line
  

  #ifndef ___CVODEDispWarnings___
    flag = CVodeSetMaxHnilWarns(cvode_mem, -1);
    if (check_flag(&flag, "CVodeSetMaxHnilWarns", 1))
    { /* Free dynamic memory */
      N_VDestroy_Serial(y);
      #ifdef ___CVODE_VECTOR_ATOL___
        N_VDestroy_Serial(abstol);
      #endif
      CVodeFree(&cvode_mem);
      return 1;
    }
  #endif

  /* Call CVDense to specify the CVDENSE dense linear solver */
  flag = CVDense(cvode_mem, ___CVODE_NEQ___);
  if (check_flag(&flag, "CVDense", 1))
  { /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }
    
  /* Set the maximum number of steps to be taken by the solver in an attempt to
     reach the next output time */
  flag = CVodeSetMaxNumSteps(cvode_mem, ___CVODE_MAX_NUM_STEPS___);
  if (check_flag(&flag, "CVodeSetMaxNumSteps", 1))
  {   /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }
  
  /* Set a custom error function for CVODE to prevent the countless SUNDIALS_ERROR messages that pop up */
  flag = CVodeSetErrHandlerFn(cvode_mem, myErrFcn, par);
  if (check_flag(&flag, "CVodeSetErrHandlerFn", 1))
  {   /* Free dynamic memory */
    N_VDestroy_Serial(y);
    #ifdef ___CVODE_VECTOR_ATOL___
      N_VDestroy_Serial(abstol);
    #endif
    CVodeFree(&cvode_mem);
    return 1;
  }

  it =infus_time.begin();          // it is pointer to next infusion time
  curr_bolus = bolus_time.begin(); // bol_counter 
  if (set_stopflag)
  {
    CVodeSetStopTime(cvode_mem, *it); 
    // printf("Setting first stop time at %f\n",*it);   // debug print line
  }
  unsigned int save_ind = 1;       // counter index for saving results

  /* In a for loop, call CVode, print results, and test for error.
  Start loop at ii=1 because tsim[0] is the initial condition.
  Break out of for loop if an error is encountered */ 
  for (ii = 0; ii < tsim_checked.size(); ++ii) // start at 1 due to tsim[0] = initial conditions
  {	
    // printf("::Integrating to time %2.1f...\n", tsim_checked[ii]); // debug print line
    
    flag = CVode(cvode_mem, tsim_checked[ii], y, &t, CV_NORMAL);    
    if (check_flag(&flag, "CVode", 1)) 
    {
      N_VDestroy_Serial(y);
      #ifdef ___CVODE_VECTOR_ATOL___
        N_VDestroy_Serial(abstol);
      #endif
      CVodeFree(&cvode_mem);
      return 1; 
    }

    // Add bolus if any
    if ((curr_bolus < bolus_time.end()) && (tsim_checked[ii] == *curr_bolus))
    {
      // printf(" Attempting to inject bolus...["); // debug print line
      for (int state = 0; state < nState; ++state)
      {  
        NV_Ith_S(y,state) += infus_bol( std::distance(bolus_time.begin(),curr_bolus), state+1 );
        // printf("%2.1f\t",infus_bol( std::distance(bolus_time.begin(),curr_bolus),state+1) );   // debug print line
      }
      flag = CVodeReInit(cvode_mem, tsim_checked[ii], y);
      if (check_flag(&flag, "CVodeReInit", 1))
      { /* Free dynamic memory if CVODE failed */
        N_VDestroy_Serial(y);
        #ifdef ___CVODE_VECTOR_ATOL___
          N_VDestroy_Serial(abstol);
        #endif
        CVodeFree(&cvode_mem);
        return(1);
      }
      curr_bolus++;
      // printf("]    SUCCESS!\n");  // debug print line
    }

    // Append state to results if it was requested
    if (tsim_save[ii])
    {
      for (int state = 0; state < nState; ++state)
		    (*model)(save_ind,state) = NV_Ith_S(y,state);
      save_ind++;
    }    

    // Increment to the next infusion time if any
    if (set_stopflag & (tsim_checked[ii] >= *it))
    {
      if (infus_zoh(zoh_counter+1,0) == *it)  // if zoh is next infusion
      {
        zoh_counter++;
        // zoh_segment.setPar(infus_zoh.getPar() + zoh_counter * infus_zoh.GetWd());
        zoh_segment.copy(infus_zoh, zoh_counter, 0);
        // printf(" zoh is now: %2.1f, %2.1f \\ %2.1f, %2.1f\n", zoh_segment(0,0), zoh_segment(0,1), zoh_segment(1,0), zoh_segment(1,1)); // debug print line
        user_data[1] = (void*) &zoh_segment;
      }
      if (infus_ramp(ramp_counter+1,0) == *it)// if ramp is next infusion
      {
        ramp_counter++;
        // ramp_segment.setPar(infus_ramp.getPar() + infus_ramp.GetWd());
        ramp_segment.copy(infus_ramp, ramp_counter, 0);
        user_data[2] = (void*) &ramp_segment;
      }

      // Update user data accordingly
      flag = CVodeSetUserData(cvode_mem, user_data);
      if (check_flag(&flag, "CVodeSetUserdata", 1))
      { /* Free dynamic memory */
        N_VDestroy_Serial(y);
        #ifdef ___CVODE_VECTOR_ATOL___
          N_VDestroy_Serial(abstol);
        #endif
        CVodeFree(&cvode_mem);
        return 1;
      }

      ++it;
      CVodeSetStopTime(cvode_mem, *it); 
      
      // printf("\t incrementing infus time to %2.1f, size is %i,\n",*it, infus_time.size());   // debug print line
      
    }
  } 
  // printf("I finished!\n"); // debug print line

  /* Print some final statistics */
  // PrintFinalStats(cvode_mem);

  /* Free y and abstol vectors */
  N_VDestroy_Serial(y);
  #ifdef ___CVODE_VECTOR_ATOL___
    N_VDestroy_Serial(abstol);
  #endif

  /* Free integrator memory */
  CVodeFree(&cvode_mem);

  // printf("I cleared my mem! saveind = %i, modelheight = %i\n",save_ind, model->GetHt());
  // printf("====results====\nt\tCa\tCb\n");
  // for (int i = 0; i < model->GetHt(); ++i)
  // {
  //  printf("%2.1f\t",tsim[i]);
  //  for (int j = 0;  j < model->GetWd(); ++j)
  //    printf("%2.1f \t",(*model)(i,j));
  //  printf("\n");
  // }
  
  return(0); 
} /* End Main */


/*
 *-------------------------------
 * Private helper functions
 *-------------------------------
 */

// static void PrintOutput(realtype t, realtype y1, realtype y2, realtype y3)
// {
// #if defined(SUNDIALS_EXTENDED_PRECISION)
//   printf("At t = %0.4Le      y =%14.6Le  %14.6Le  %14.6Le\n", t, y1, y2, y3);
// #elif defined(SUNDIALS_DOUBLE_PRECISION)
//   printf("At t = %0.4le      y =%14.6le  %14.6le  %14.6le\n", t, y1, y2, y3);
// #else
//   printf("At t = %0.4e      y =%14.6e  %14.6e  %14.6e\n", t, y1, y2, y3);
// #endif

//   return;
// }


/* 
 * Get and print some final statistics
 */

// static void PrintFinalStats(void *cvode_mem)
// {
//   long int nst, nfe, nsetups, nje, nfeLS, nni, ncfn, netf, nge;
//   int flag;

//   flag = CVodeGetNumSteps(cvode_mem, &nst);
//   check_flag(&flag, "CVodeGetNumSteps", 1);
//   flag = CVodeGetNumRhsEvals(cvode_mem, &nfe);
//   check_flag(&flag, "CVodeGetNumRhsEvals", 1);
//   flag = CVodeGetNumLinSolvSetups(cvode_mem, &nsetups);
//   check_flag(&flag, "CVodeGetNumLinSolvSetups", 1);
//   flag = CVodeGetNumErrTestFails(cvode_mem, &netf);
//   check_flag(&flag, "CVodeGetNumErrTestFails", 1);
//   flag = CVodeGetNumNonlinSolvIters(cvode_mem, &nni);
//   check_flag(&flag, "CVodeGetNumNonlinSolvIters", 1);
//   flag = CVodeGetNumNonlinSolvConvFails(cvode_mem, &ncfn);
//   check_flag(&flag, "CVodeGetNumNonlinSolvConvFails", 1);

//   flag = CVDlsGetNumJacEvals(cvode_mem, &nje);
//   check_flag(&flag, "CVDlsGetNumJacEvals", 1);
//   flag = CVDlsGetNumRhsEvals(cvode_mem, &nfeLS);
//   check_flag(&flag, "CVDlsGetNumRhsEvals", 1);

//   flag = CVodeGetNumGEvals(cvode_mem, &nge);
//   check_flag(&flag, "CVodeGetNumGEvals", 1);

//   printf("\nFinal Statistics:\n");
//   printf("nst = %-6ld nfe  = %-6ld nsetups = %-6ld nfeLS = %-6ld nje = %ld\n",
// 	 nst, nfe, nsetups, nfeLS, nje);
//   printf("nni = %-6ld ncfn = %-6ld netf = %-6ld nge = %ld\n \n",
// 	 nni, ncfn, netf, nge);
// }

/*
 * Check function return value...
 *   opt == 0 means SUNDIALS function allocates memory so check if
 *            returned NULL pointer
 *   opt == 1 means SUNDIALS function returns a flag so check if
 *            flag >= 0
 *   opt == 2 means function allocates memory so check if returned
 *            NULL pointer 
 */

 
static int check_flag(void *flagvalue, char *funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    // fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
	    // funcname);
    return(1); }

  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      // fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
	      // funcname, *errflag);
      return(1); }}

  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    // fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
	   //  funcname);
    return(1); }

  return(0);
}


void myErrFcn( int error_code, const char *module, const char *function, char *msg, void *eh_data)
{ /*Do nothing when error is encountered to pretty up terminal output for MCMC.*/ }