#include "options.hpp"
#include <trng/yarn3.hpp>
#include <trng/uniform01_dist.hpp>
#include <math.h>
#include <algorithm>

/* Helper functions for swaps */


void shuffle(int * shuffle_ptr, int len, trng::yarn3 &gen)
{
// Shuffles the array pointed by ptr_start and shufles it randomly.
int ind, tmp;

trng::uniform01_dist<> u;

for (int ind2 = len-1; ind2 > 0; --ind2)
	{
		ind = static_cast<int> ( u(gen) * len ); 	// generate a random index to swap i with
		// Swap ind with ind2 from the array pointed by ptr_start into shuffle_ptr
		tmp = shuffle_ptr[ind];
		shuffle_ptr[ind] = shuffle_ptr[ind2];
		shuffle_ptr[ind2] = tmp;
	}


}

double calc_rate (int ID, int * ptr )
{
	double result = 0;
	for (int i = 0; i < ID; ++i)
	{
		result += ptr[i];
	}

	result = result/ID;
	return result;
}

void adapt_beta( double* swaprate, double* beta_curr)
{
	double adaptfactor;
	for (int chainID = 1; chainID < ___nChains___; ++chainID)
	{
		adaptfactor = pow(swaprate[chainID]/___optimal_swap_accept___, ___adapt_beta_rate___);
		
		// Adjust adapt factor so that it is bounded by a max/min set by the options.hpp entries
		adaptfactor = std::min(std::max(adaptfactor, ___min_adaption_factor___), ___max_adaption_factor___ );
		
		// Adjust adapt factor so that it does not get too close to its cooler neighbor
		adaptfactor = std::max(adaptfactor, 2*beta_curr[chainID]/(beta_curr[chainID]+beta_curr[chainID-1]) );
		
		for (int chain2 = chainID; chain2 < ___nChains___; ++chain2)
			beta_curr[chain2] /= adaptfactor;
	}

}

void adapt_step(double * steprate, double* step_curr)
{
	double adaptfactor;
	for (int chainID = 0; chainID < ___nChains___; ++chainID)
	{
		adaptfactor = pow(steprate[chainID]/___optimal_step_accept___, ___adapt_step_size___);
		
		// Adjust adapt factor so that it is bounded by a max/min set by the options.hpp entries
		adaptfactor = std::min(std::max(adaptfactor, ___min_adaption_factor___), ___max_adaption_factor___ );
		
		if (!(adaptfactor*step_curr[chainID] <= 1.0))
			step_curr[chainID] = adaptfactor*step_curr[chainID];
		
	}

}